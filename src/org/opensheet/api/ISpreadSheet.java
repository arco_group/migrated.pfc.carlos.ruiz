/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.api;

import java.util.List;
import org.opensheet.api.exceptions.CellNoFoundException;

/**
 * Es la interfaz que permite operar con hojas de c&aacute;lculo de un documento.
 * A trav&eacute;s de esta interfaz se permite insertar y extraer valores de
 * celdas de la hoja.
 *
 * @author cruiz
 */
public interface ISpreadSheet {
    /**
     * Devuelve el nombre de la hoja de c&aacute;laculo.
     * @return String con el nombre de la hoja de c&aacute;laculo.
     */
    public String getName();

    /**
     * Cambia el nombre actual de la hoja de c&aacute;laculo por el nombre pasado
     * como par&aacute;metro.
     * @param spreadSheetNewName String que es el nuevo nombre que se le quiere
     * dar a la hoja de c&aacute;lculo.
     * @return true si se consigue cambiar el nombre, false en caso contrario.
     */
    public boolean rename(String spreadSheetNewName);    

    /**
     * Asigna un valor num&eacute;rico una celda.
     * @param position CellPosition que representa la celda a la que se le desea
     * asignar un valor.
     * @param value double con el valor a asignar en la celda.
     * @throws CellNoFoundException
     */
    public void setCellValue(CellPosition position, double value) throws CellNoFoundException;

    /**
     * Asigna un valor de texto una celda.
     * @param position CellPosition que representa la celda a la que se le desea
     * asignar un valor.
     * @param value String con el valor a asignar en la celda.
     * @throws CellNoFoundException
     */
    public void setCellValue(CellPosition position, String value) throws CellNoFoundException;

    /**
     * Asigna un valor num&eacute;rico a todas la celdas de un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @param value double con el valor a asignar en la celda.
     * @throws CellNoFoundException
     */
    public void setRangeValue(CellPosition firstCellPosition, CellPosition lastCellPosition, double value) throws CellNoFoundException;

    /**
     * Asigna un valor de texto a todas la celdas de un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @param value String con el valor a asignar en la celda.
     * @throws CellNoFoundException
     */
    public void setRangeValue(CellPosition firstCellPosition, CellPosition lastCellPosition, String value) throws CellNoFoundException;

    /**
     * Recupera el valor num&eacute;rico de una celda.
     * @param position CellPosition que representa la celda a la que se le desea
     * extraer su valor.
     * @return double con el valor num&eacute;rico de la celda.
     * @throws CellNoFoundException
     */
    public double getCellValue(CellPosition position) throws CellNoFoundException;

    /**
     * Recupera el valor de texto de una celda.
     * @param position CellPosition que representa la celda a la que se le desea
     * extraer su valor.
     * @return String con el valor de texto de la celda.
     * @throws CellNoFoundException
     */
    public String getCellText(CellPosition position) throws CellNoFoundException;

    /**
     * Recupera una lista de valores num&eacute;ricos a partir de cada una de
     * las celdas que forman un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @return Una lista de doucble con cada uno de los valores num&eacute;ricos
     * de las celdas que forman el rango.
     * @throws CellNoFoundException
     */
    public List<Double> getRangeValues(CellPosition firstCellPosition, CellPosition lastCellPosition) throws CellNoFoundException;

    /**
     * Recupera una lista de valores de texto a partir de cada una de las celdas
     * que forman un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @return Una lista de String con cada uno de los valores de texto de las
     * celdas que forman el rango.
     * @throws CellNoFoundException
     */
    public List<String> getRangeTexts(CellPosition firstCellPosition, CellPosition lastCellPosition) throws CellNoFoundException;

    /**
     * Recupera el valor de una celda, ya sea de texo o num&eacute;rico.
     * @param position CellPosition que representa la celda a la que se le desea
     * extraer su valor.
     * @return Object con el valor de la celda, que puede ser de tipo Double o
     * String.
     * @throws CellNoFoundException
     */
    public Object getCellContent(CellPosition position) throws CellNoFoundException;

    /**
     * Recupera una lista de valores a partir de cada una de las celdas
     * que forman un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @return Una lista de Objects con cada uno de los valores de las
     * celdas que forman el rango. Estos valores pueden ser de tipo String o
     * Double.
     * @throws CellNoFoundException
     */
    public List<Object> getRangeContent(CellPosition firstCellPosition, CellPosition lastCellPosition) throws CellNoFoundException;
}
