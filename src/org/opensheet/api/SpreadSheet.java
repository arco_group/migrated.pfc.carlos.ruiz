/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.api;

import com.sun.star.container.XNamed;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.sheet.XSpreadsheet;
import com.sun.star.table.CellContentType;
import com.sun.star.table.XCell;
import com.sun.star.table.XCellRange;
import com.sun.star.text.XText;
import com.sun.star.text.XTextCursor;
import com.sun.star.uno.UnoRuntime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opensheet.api.exceptions.CellNoFoundException;

/**
 * Es la clase que implementa la interfaz ISpreadSheet que permite operar
 * con hojas de c&aacute;lculo de un documento.
 *
 * @author cruiz
 */
public class SpreadSheet implements ISpreadSheet
{

    XSpreadsheet xSpreadsheet;
    String name;

    /**
     * Es el constructor de la clase que tiene una visibilidad <i>protected</i>
     * para que s&oacute;lo pueda ser usado por las clases de su paquete. Este
     * tipo de objetos deben ser creados a trav&eacute;s del uso de la clase
     * IOpenSheetDocument.
     * @param xSpreadsheet Interfaz remota de OpenOffice.org/Uno.
     * @param name String con el nombre de la hoja de c&aacute;lculo.
     */
    SpreadSheet(XSpreadsheet xSpreadsheet, String name)
    {
        this.xSpreadsheet = xSpreadsheet;
        this.name = name;
    }

    /**
     * Devuelve el nombre de la hoja de c&aacute;laculo.
     * @return String con el nombre de la hoja de c&aacute;laculo.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Cambia el nombre actual de la hoja de c&aacute;laculo por el nombre pasado
     * como par&aacute;metro.
     * @param spreadSheetNewName String que es el nuevo nombre que se le quiere
     * dar a la hoja de c&aacute;lculo.
     * @return true si se consigue cambiar el nombre, false en caso contrario.
     */
    public boolean rename(String spreadSheetNewName)
    {

        if (spreadSheetNewName == null || spreadSheetNewName.isEmpty())
        {
            return false;
        }

        XNamed xNamed = (XNamed) UnoRuntime.queryInterface(XNamed.class, xSpreadsheet);
        if (xNamed == null)
        {
            return false;
        }

        xNamed.setName(spreadSheetNewName);
        name = spreadSheetNewName;

        return true;
    }

    /**
     * Asigna un valor num&eacute;rico una celda.
     * @param position CellPosition que representa la celda a la que se le desea
     * asignar un valor.
     * @param value double con el valor a asignar en la celda.
     * @throws CellNoFoundException
     */
    public void setCellValue(CellPosition cell, double value) throws CellNoFoundException
    {
        try
        {
            XCell xCell = xSpreadsheet.getCellByPosition(cell.getPosX(), cell.getPosY());
            xCell.setValue(value);
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }
    }

    /**
     * Asigna un valor de texto una celda.
     * @param position CellPosition que representa la celda a la que se le desea
     * asignar un valor.
     * @param value String con el valor a asignar en la celda.
     * @throws CellNoFoundException
     */
    public void setCellValue(CellPosition cell, String value) throws CellNoFoundException
    {
        try
        {
            XCell xCell = xSpreadsheet.getCellByPosition(cell.getPosX(), cell.getPosY());
            XText xCellText = (XText) UnoRuntime.queryInterface(XText.class, xCell);
            XTextCursor xTextCursor = xCellText.createTextCursor();
            xCellText.insertString(xTextCursor, value, false);
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }
    }

    /**
     * Asigna un valor num&eacute;rico a todas la celdas de un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @param value double con el valor a asignar en la celda.
     * @throws CellNoFoundException
     */
    public void setRangeValue(CellPosition firstCellPosition, CellPosition lastCellPosition, double value) throws CellNoFoundException
    {
        try
        {
            XCellRange xCellRange = xSpreadsheet.getCellRangeByPosition(firstCellPosition.getPosX(), firstCellPosition.getPosY(), lastCellPosition.getPosX(), lastCellPosition.getPosY());

            int xLen = getAbsoluteValue(firstCellPosition.getPosX() - lastCellPosition.getPosX());
            int yLen = getAbsoluteValue(firstCellPosition.getPosY() - lastCellPosition.getPosY());

            for (int i = 0; i <= xLen; i++)
            {
                for (int j = 0; j <= yLen; j++)
                {
                    XCell xCell = xCellRange.getCellByPosition(i, j);
                    xCell.setValue(value);
                }
            }
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }
    }

    private int getAbsoluteValue(int x)
    {

        return x < 0 ? -x : x;
    }

    /**
     * Asigna un valor de texto a todas la celdas de un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @param value String con el valor a asignar en la celda.
     * @throws CellNoFoundException
     */
    public void setRangeValue(CellPosition firstCellPosition, CellPosition lastCellPosition, String value) throws CellNoFoundException
    {
        try
        {
            XCellRange xCellRange = xSpreadsheet.getCellRangeByPosition(firstCellPosition.getPosX(), firstCellPosition.getPosY(), lastCellPosition.getPosX(), lastCellPosition.getPosY());

            int xLen = getAbsoluteValue(firstCellPosition.getPosX() - lastCellPosition.getPosX());
            int yLen = getAbsoluteValue(firstCellPosition.getPosY() - lastCellPosition.getPosY());

            for (int i = 0; i <= xLen; i++)
            {
                for (int j = 0; j <= yLen; j++)
                {
                    XCell xCell = xCellRange.getCellByPosition(i, j);
                    XText xCellText = (XText) UnoRuntime.queryInterface(XText.class, xCell);
                    XTextCursor xTextCursor = xCellText.createTextCursor();
                    xCellText.insertString(xTextCursor, value, false);
                }
            }
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }
    }

    /**
     * Recupera el valor num&eacute;rico de una celda.
     * @param position CellPosition que representa la celda a la que se le desea
     * extraer su valor.
     * @return double con el valor num&eacute;rico de la celda.
     * @throws CellNoFoundException
     */
    public double getCellValue(CellPosition cell) throws CellNoFoundException
    {
        try
        {
            XCell xCell = xSpreadsheet.getCellByPosition(cell.getPosX(), cell.getPosY());
            return xCell.getValue();
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }
    }

    /**
     * Recupera el valor de texto de una celda.
     * @param position CellPosition que representa la celda a la que se le desea
     * extraer su valor.
     * @return String con el valor de texto de la celda.
     * @throws CellNoFoundException
     */
    public String getCellText(CellPosition cell) throws CellNoFoundException
    {
        try
        {
            XCell xCell = xSpreadsheet.getCellByPosition(cell.getPosX(), cell.getPosY());
            XText xCellText = (XText) UnoRuntime.queryInterface(XText.class, xCell);

            return xCellText.getString();
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }
    }

    /**
     * Recupera una lista de valores num&eacute;ricos a partir de cada una de
     * las celdas que forman un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @return Una lista de doucble con cada uno de los valores num&eacute;ricos
     * de las celdas que forman el rango.
     * @throws CellNoFoundException
     */
    public List<Double> getRangeValues(CellPosition firstCellPosition, CellPosition lastCellPosition) throws CellNoFoundException
    {
        int xLen = getAbsoluteValue(firstCellPosition.getPosX() - lastCellPosition.getPosX());
        int yLen = getAbsoluteValue(firstCellPosition.getPosY() - lastCellPosition.getPosY());        
        ArrayList values = new ArrayList();

        try
        {
            XCellRange xCellRange = xSpreadsheet.getCellRangeByPosition(firstCellPosition.getPosX(), firstCellPosition.getPosY(), lastCellPosition.getPosX(), lastCellPosition.getPosY());

            for (int i = 0; i <= xLen; i++)
            {
                for (int j = 0; j <= yLen; j++)
                {
                    XCell xCell = xCellRange.getCellByPosition(i, j);
                    values.add(xCell.getValue());
                }
            }
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }

        return values;
    }

    /**
     * Recupera una lista de valores de texto a partir de cada una de las celdas
     * que forman un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @return Una lista de String con cada uno de los valores de texto de las
     * celdas que forman el rango.
     * @throws CellNoFoundException
     */
    public List<String> getRangeTexts(CellPosition firstCellPosition, CellPosition lastCellPosition) throws CellNoFoundException
    {
        int xLen = getAbsoluteValue(firstCellPosition.getPosX() - lastCellPosition.getPosX());
        int yLen = getAbsoluteValue(firstCellPosition.getPosY() - lastCellPosition.getPosY());
        ArrayList<String> values = new ArrayList();

        try
        {
            XCellRange xCellRange = xSpreadsheet.getCellRangeByPosition(firstCellPosition.getPosX(), firstCellPosition.getPosY(), lastCellPosition.getPosX(), lastCellPosition.getPosY());

            for (int i = 0; i <= xLen; i++)
            {
                for (int j = 0; j <= yLen; j++)
                {
                    XCell xCell = xCellRange.getCellByPosition(i, j);
                    XText xCellText = (XText) UnoRuntime.queryInterface(XText.class, xCell);
                    values.add(xCellText.getString());
                }
            }
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }

        return values;
    }

    /**
     * Recupera el valor de una celda, ya sea de texo o num&eacute;rico.
     * @param position CellPosition que representa la celda a la que se le desea
     * extraer su valor.
     * @return Object con el valor de la celda, que puede ser de tipo Double o
     * String.
     * @throws CellNoFoundException
     */
    public Object getCellContent(CellPosition cell) throws CellNoFoundException
    {
        Object result = null;
        try
        {
            XCell xCell = xSpreadsheet.getCellByPosition(cell.getPosX(), cell.getPosY());
            
            result = getCellContent(xCell);
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }

        return result;
    }

    private Object getCellContent(XCell xCell)
    {
        Object result = null;

        if (xCell.getType() == CellContentType.TEXT || xCell.getType() == CellContentType.FORMULA)
        {
            XText xCellText = (XText) UnoRuntime.queryInterface(XText.class, xCell);
            result = xCellText.getString();
        }
        else if (xCell.getType() == CellContentType.VALUE)
        {
            result = xCell.getValue();
        }        

        return result;
    }
    

    /**
     * Recupera una lista de valores a partir de cada una de las celdas
     * que forman un rango.
     * @param firstCellPosition CellPosition que representa la celda de arriba a
     * la izquierda del rango.
     * @param lastCellPosition CellPosition que representa la celda de abajo a
     * la derecha del rango.
     * @return Una lista de Objects con cada uno de los valores de las
     * celdas que forman el rango. Estos valores pueden ser de tipo String o
     * Double.
     * @throws CellNoFoundException
     */
    public List<Object> getRangeContent(CellPosition firstCellPosition, CellPosition lastCellPosition) throws CellNoFoundException
    {
        int xLen = getAbsoluteValue(firstCellPosition.getPosX() - lastCellPosition.getPosX());
        int yLen = getAbsoluteValue(firstCellPosition.getPosY() - lastCellPosition.getPosY());
        ArrayList values = new ArrayList();

        try
        {
            XCellRange xCellRange = xSpreadsheet.getCellRangeByPosition(firstCellPosition.getPosX(), firstCellPosition.getPosY(), lastCellPosition.getPosX(), lastCellPosition.getPosY());

            for (int i = 0; i <= xLen; i++)
            {
                for (int j = 0; j <= yLen; j++)
                {                    
                    XCell xCell = xCellRange.getCellByPosition(i, j);
                    values.add(getCellContent(xCell));
                }
            }
        }
        catch (IndexOutOfBoundsException ex)
        {
            throw new CellNoFoundException(ex);
        }

        return values;
    }
}
