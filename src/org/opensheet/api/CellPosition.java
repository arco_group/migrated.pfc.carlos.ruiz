/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from 
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.api;

import org.opensheet.api.exceptions.CellNameException;

/**
 * Representa la posici&oacute;n de una celda.
 * Contiene las dos coordenadas de la posición X e Y, accesibles desde sus
 * m&eacute;todos de acceso.
 * Un objeto CellPosition puede ser creado a partir del valor entero de su
 * posición X e Y, o bien a través de una cadena que identifique la celda.
 *
 * @author cruiz
 */
public class CellPosition {
    private int posX;
    private int posY;

    /**
     * Constructor que permite crear un objeto, que representa una
     * posici&oacute;n de celda, a partir de las posiciones X e Y. Las posiciones
     * comienza por 0.
     * @param posX Indica el n&uacute;mero de columna. La primera columna es la 0.
     * @param posY Indica el n&uacute;mero de fila. La primera fila es la 0.
     */
    public CellPosition(int posX, int posY)
    {
        this.posX = posX;
        this.posY = posY;
    }

     /**
     * Constructor que crea un objeto que representa una
     * posici&oacute;n de celda en la posici&oacute;n (0,0).     
     */
    public CellPosition()
    {
        this.posX = 0;
        this.posY = 0;
    }

    /**
     * Constructor que permite crear un objeto, que representa una
     * posici&oacute;n de celda, a partir una cadena con el identificador de la
     * posición. La cadena empieza por una o varias letras seguidas de uno o más
     * d&iacute;gitos, donde las letras hacen referencia a las columnas y los
     *  d&iacute;gitos a la fila. Un ejemplo "A1" representa la posici&oacute;n
     * X = 0 e Y = 0.     
     * @param cellName Es la cadena con el identificador de la celda. Donde
     * primero se nombra la columna a través de una o m&aacute;s letras, y
     * despum&eacute;s se hace referencia a la fila a través de uno o m&aacute;s
     * d&iacute;gitos.
     * @throws CellNameException Se lanza cuando el nombre no es un nombre de
     * celda v&aacute;lido.
     */
    CellPosition(String cellName) throws CellNameException
    {
        if (cellName == null || cellName.isEmpty())
        {
            throw new CellNameException("Error: empty or null cell name");
        }

        if (!cellName.matches("[a-zA-Z]+[0-9]+"))
        {
            throw new CellNameException("'"+cellName+"' cell name is invalid");
        }

        cellName = cellName.toLowerCase();
        this.posX = getPosXFromString(cellName.split("[0-9]+")[0]) - 1;
        this.posY = new Integer(cellName.split("[a-z]+")[1]) - 1;
    }

    /**
     * Devuelve el valor de la posici&oacute;n X o la columna
     * del objeto CellPosition.
     * @return El entero que representa la columna o posici&oacute;n X. El valor
     * 0 representa la primera columna.
     */
    public int getPosX()
    {
        return posX;
    }

    /**
     * Reasigna la posici&oacute;n Y o la columna de un
     * objeto CellPosition.
     * @param posX Es el entero que representa la posici&oacute;n X o la columna
     * que se quiere asignar al objeto. El 0 representa la primera columna.
     */
    public void setPosX(int posX)
    {
        this.posX = posX;
    }
    
    /**
     * Devuelve el valor de la posici&oacute;n Y o la fila del
     * objeto CellPosition.
     * @return El entero que representa la fila o posici&oacute;n Y. El valor 0
     * representa la primera fila.
     */
    public int getPosY()
    {
        return posY;
    }
    
    /**
     * Reasigna la posici&oacute;n Y o la fila de un
     * objeto CellPosition.
     * @param posY Es el entero que representa la posici&oacute;n Y o la fila
     * que se quiere asignar al objeto. El 0 representa la primera fila.
     */
    public void setPosY(int posY)
    {
        this.posY = posY;
    }

    /**
     * Indica si dos objetos CellPosition son iguales. Siendo
     * dos objetos iguales cuando sus posiciones X e Y son iguales.
     * @param obj Es el objeto CellPosition con el que se quiere comparar.
     * @return true si ambos objetos son iguales (sus posiciones coindicen), false
     * en caso contrario.
     */
    @Override
    public boolean equals (Object obj)
    {
        CellPosition cell = (CellPosition)obj;

        if ( (cell.posX == this.posX) && (cell.posY == this.posY) )
            return true;
        else
            return false;
    }

    private int getPosXFromString(String posXName)
    {
        int pos = 0;
        
        int j = 0;
        for (int i = posXName.length() - 1; i >= 0; i--)
        {            
            pos += getLetterValue(posXName.charAt(i),j);
            j++;
        }

        return pos;
    }

    private int getLetterValue(char letter, int pos)
    {
        int posValue = 1;
        int letterBaseValue = 0;

        posValue = (int) Math.pow(26, pos);
       
        switch(letter)
        {
            case 'a':
                letterBaseValue = 1;
                break;
            case 'b':
                letterBaseValue = 2;
                break;
            case 'c':
                letterBaseValue = 3;
                break;
            case 'd':
                letterBaseValue = 4;
                break;
            case 'e':
                letterBaseValue = 5;
                break;
            case 'f':
                letterBaseValue = 6;
                break;
            case 'g':
                letterBaseValue = 7;
                break;
            case 'h':
                letterBaseValue = 8;
                break;
            case 'i':
                letterBaseValue = 9;
                break;
            case 'j':
                letterBaseValue = 10;
                break;
            case 'k':
                letterBaseValue = 11;
                break;
            case 'l':
                letterBaseValue = 12;
                break;
            case 'm':
                letterBaseValue = 13;
                break;
            case 'n':
                letterBaseValue = 14;
                break;
            case 'o':
                letterBaseValue = 15;
                break;
            case 'p':
                letterBaseValue = 16;
                break;
            case 'q':
                letterBaseValue = 17;
                break;
            case 'r':
                letterBaseValue = 18;
                break;
            case 's':
                letterBaseValue = 19;
                break;
            case 't':
                letterBaseValue = 20;
                break;
            case 'v':
                letterBaseValue = 21;
                break;
            case 'w':
                letterBaseValue = 22;
                break;
            case 'x':
                letterBaseValue = 23;
                break;
            case 'y':
                letterBaseValue = 24;
                break;
            case 'z':
                letterBaseValue = 25;
                break;            
        }
        posValue = posValue * letterBaseValue;
        
        return posValue;
    }    
}
