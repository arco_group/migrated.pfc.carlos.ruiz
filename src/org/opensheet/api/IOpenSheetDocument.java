/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.api;

import java.util.List;
import org.opensheet.api.exceptions.OverwriteSpreadSheetException;
import org.opensheet.api.exceptions.SpreadSheetNoFoundException;
import org.opensheet.api.exceptions.StoreOpenSheetDocumentException;
import org.opensheet.api.exceptions.UnsupportedOpenSheetDocumentException;

/**
 * Es la interfaz que permite operar con documentos de hojas de c&aacute;lculo.
 * A trav&eacute;s de esta interfaz se permite recuperar la hojas de c&aacute;lculo,
 * que contiene el documento para poder trabajar con ellas.
 * 
 * @author cruiz
 */
public interface IOpenSheetDocument {
    /**
     * Cierra un documento de hojas de cálculo liberando todos los
     * recursos abiertos por el proceso de OpenOffice.org.
     * Este m&eacute; debe ser llamado siempre cuando se termine de trabajar con 
     * el documento.
     */
    public void close();

    /**
     * Guarda el documento abierto, cambiando la ruta, el nombre o el formato.
     * El cambio de formato es directo, basta con especificar nombre que se
     * desee dar con la extensión del nuevo formato.
     * S&oacute;lo se permiten los formatos: xls, ods, ots, xlt y xlsx.
     * <i>Nota: la capacidad de conversión viene dada por el uso de OpenOffice.org,
     * por lo tanto las limitaciones que la suite presente, tambi&eacute;n se
     * dar&aacute;n aqu&iacute;</i>.
     * @param documentPath String con el nombre y la ruta con el que se quiere
     * guardar el documento.
     * @throws StoreOpenSheetDocumentException
     * @throws UnsupportedOpenSheetDocumentException
     */
    public void saveAs(String documentPath) throws StoreOpenSheetDocumentException, UnsupportedOpenSheetDocumentException;

    /**
     * Guarda los cambios realizados en el documento.
     * @throws StoreOpenSheetDocumentException
     */
    public void save() throws StoreOpenSheetDocumentException;

    /**
     * Exporta el documento a un fichero PDF.
     * @param documenPath String que contiene la ruta y el nombre del fichero
     * con el que se va a guardar el documento PDF generado.
     * @throws StoreOpenSheetDocumentException
     * @throws UnsupportedOpenSheetDocumentException
     */
    public void exportToPDF(String documenPath) throws StoreOpenSheetDocumentException, UnsupportedOpenSheetDocumentException;

    /**
     * Devuelve un array de String con los nombre de cada una
     * de las hojas de c&aacute;lculo que contiene el documento.
     * @return Devuelve un array de String con los nombres de las hojas de
     * c&aacute;lculo que contiene el documento.
     */
    public String[] getSpreadSheetsNames();

    /**
     * Devuelve un objeto para trabajar con la hoja de c&aacute;lculo situada en
     * la posici&oacute;n pasada como par&aacute;metro.
     * @param pos int que representa la posici&oacute;n de la hoja de
     * c&aacute;lculo que se quiere recuperar. La primera posici&oacute;n se
     * corresponde con el valor 0.
     * @return Devuelve un objeto ISpreadSheet.
     * @throws SpreadSheetNoFoundException
     */
    public ISpreadSheet getSpreadSheet(int pos) throws SpreadSheetNoFoundException;

    /**
     * Devuelve un objeto para trabajar con la hoja de c&aacute;lculo cuyo
     * nombre es el de la cadena pasada como par&aacute;metro.
     * @param spreadSheetName String con el nombre de la hoja de c&aacute;lculo
     * que se quiere recuperar.
     * @return Devuelve un objeto ISpreadSheet.
     * @throws SpreadSheetNoFoundException
     */
    public ISpreadSheet getSpreadSheet(String spreadSheetName) throws SpreadSheetNoFoundException;

    /**
     * Crea una nueva hoja de c&aacute;lculo dentro del documento con un nombre
     * por defecto.
     * @return ISpreadSheet que representa al objeto recien creado.
     * @throws OverwriteSpreadSheetException
     */
    public ISpreadSheet addSpreadSheet() throws OverwriteSpreadSheetException;

    /**
     * Crea una nueva hoja de c&aacute;lculo dentro del documento, asign&aacute;ndole
     * como nombre el pasado como par&aacute;metro.
     * @param spreadSheetName String que contiene el nombre que se quiere
     * asignar a la nueva hoja a crear.
     * @return ISpreadSheet que representa al objeto recien creado.
     * @throws OverwriteSpreadSheetException
     */
    public ISpreadSheet addSpreadSheet(String spreadSheetName) throws OverwriteSpreadSheetException;

    /**
     * Elimina del documento la hoja de c&aacute;lculo situada
     * en la posici&oacute;n pasada como par&aacute;metro. La posici&oacute;n de
     * la primera hoja es 0.
     * @param pos int que contiene la posici&oacute;n de la hoja que se desea
     * eliminar del documento. La posici&oacute;n de la primera hoja es 0.
     * @throws SpreadSheetNoFoundException
     */
    public void deleteSpreadSheet(int pos) throws SpreadSheetNoFoundException;

    /**
     * Elimina del documento la hoja de c&aacute;lculo cuyo
     * nombre es el pasado como par&aacute;metro. 
     * @param spreadSheetName String que contiene el nombre de la hoja que se
     * desea eliminar del documento.
     * @throws SpreadSheetNoFoundException
     */
    public void deleteSpreadSheet(String spreadSheetName) throws SpreadSheetNoFoundException;

    /**
     * Indica si en el documento existe alguna hoja de
     * c&aacute;lculo con el nombre pasado como par&aacute;metro.
     * @param name String con el nombre de una hoja de c&aacute;lculo.
     * @return true si el documento tiene una hoja con ese nombre, y false en
     * caso contrario.
     */
    public boolean containsSpreadSheet(String name);
}
