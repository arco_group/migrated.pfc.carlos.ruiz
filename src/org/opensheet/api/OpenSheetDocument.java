/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.api;

import com.sun.star.container.NoSuchElementException;
import org.opensheet.api.exceptions.UnsupportedOpenSheetDocumentException;
import org.opensheet.api.exceptions.StoreOpenSheetDocumentException;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XModel;
import com.sun.star.frame.XStorable;
import com.sun.star.io.IOException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XComponent;
import com.sun.star.sheet.XSpreadsheet;
import com.sun.star.sheet.XSpreadsheetDocument;
import com.sun.star.sheet.XSpreadsheets;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.util.CloseVetoException;
import com.sun.star.util.XCloseable;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opensheet.api.exceptions.OverwriteSpreadSheetException;
import org.opensheet.api.exceptions.SpreadSheetNoFoundException;

/**
 * Es la clase que implementa la interfaz IOpenSheetDocument que permite operar
 * con documentos de hojas de c&aacute;lculo.
 *
 * @author cruiz
 */
public class OpenSheetDocument implements IOpenSheetDocument
{

    private XComponent xComponent;
    private XSpreadsheetDocument xSpreadsheetDocument;
    private String pathURL;

    /**
     * Es el constructor de la clase que tiene una visibilidad <i>protected</i>
     * para que s&oacute;lo pueda ser usado por las clases de su paquete. Este
     * tipo de objetos deben ser creados a trav&eacute;s de la clase
     * OpenSheetManager.     
     * @param xComponentLoader Interfaz remota de OpenOffice.org/Uno.
     * @param documentPath String con la ruta y el nombre del documento de hojas
     * de c&aacute;lculo.
     * @param newDocument boolean que indica si el objeto ya existía, es decir,
     * hay que realizar una operaci&oacute;n de abrir documento o es un nuevo
     * documento y hay que crearlo.
     * @throws UnsupportedOpenSheetDocumentException
     */
    OpenSheetDocument(XComponentLoader xComponentLoader, String documentPath, boolean newDocument) throws UnsupportedOpenSheetDocumentException
    {
        String docExtension = getExtension(documentPath);

        if (!isValidExtension(docExtension))
        {
            throw new UnsupportedOpenSheetDocumentException("'" + docExtension + "' document file extension is not supported.");
        }

        PropertyValue[] loadProps = new PropertyValue[1];
        loadProps[0] = new PropertyValue();
        loadProps[0].Name = "Hidden";
        loadProps[0].Value = new Boolean(true);

        String url = "private:factory/scalc";

        if (!newDocument)
        {
            url = getFileURLFromPath(documentPath);
        }

        try
        {
            xComponent = xComponentLoader.loadComponentFromURL(url, "_blank", 0, loadProps);
            xSpreadsheetDocument = (XSpreadsheetDocument) UnoRuntime.queryInterface(XSpreadsheetDocument.class, xComponent);
            saveDocument(documentPath, false);
        }
        catch (IOException ex)
        {
            Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IllegalArgumentException ex)
        {
            Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (Exception ex)
        {
            Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Cierra un documento de hojas de cálculo liberando todos los
     * recursos abiertos por el proceso de OpenOffice.org.
     * Este m&eacute; debe ser llamado siempre cuando se termine de trabajar con
     * el documento.
     */
    public void close()
    {
        XModel xModel = (XModel) UnoRuntime.queryInterface(XModel.class, xComponent);
        XCloseable xCloseable = (XCloseable) UnoRuntime.queryInterface(XCloseable.class, xModel);
        try
        {
            xCloseable.close(false);
        }
        catch (CloseVetoException ex)
        {
            Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private PropertyValue[] getStoreProperties(String pathURL)
    {
        String documentExt = getExtension(this.pathURL);
        String saveExt = getExtension(pathURL);
        PropertyValue[] storeProps = null;
        if (documentExt.equals(saveExt))
        {
            //TODO: Revisar
            XPropertySet props = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xSpreadsheetDocument);
            storeProps = new PropertyValue[1];
            storeProps[0] = new PropertyValue();
            storeProps[0].Name = "FilterName";
            try
            {
                storeProps[0].Value = props.getPropertyValue("FilterName");
            }
            catch (UnknownPropertyException ex)
            {
                Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (WrappedTargetException ex)
            {
                Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            if (saveExt.equalsIgnoreCase("XLS"))
            { //M.Excel 97
                storeProps = new PropertyValue[1];
                storeProps[0] = new PropertyValue();
                storeProps[0].Name = "FilterName";
                storeProps[0].Value = "MS Excel 97";
            }
            else if (saveExt.equalsIgnoreCase("XLT"))
            { //M.Excel 97
                storeProps = new PropertyValue[1];
                storeProps[0] = new PropertyValue();
                storeProps[0].Name = "FilterName";
                storeProps[0].Value = "MS Excel 97 Vorlage/Template";
            }
            else if (saveExt.equalsIgnoreCase("ODS"))
            { //OpenOffice.org 2.x
                storeProps = new PropertyValue[1];
                storeProps[0] = new PropertyValue();
                storeProps[0].Name = "FilterName";
                storeProps[0].Value = "calc8";
            }
            else if (saveExt.equalsIgnoreCase("OTS"))
            { //OpenOffice.org 2.x
                storeProps = new PropertyValue[1];
                storeProps[0] = new PropertyValue();
                storeProps[0].Name = "FilterName";
                storeProps[0].Value = "calc8_template";
            }
            else if (saveExt.equalsIgnoreCase("PDF"))
            { //PDF
                storeProps = new PropertyValue[1];
                storeProps[0] = new PropertyValue();
                storeProps[0].Name = "FilterName";
                storeProps[0].Value = "calc_pdf_Export";
            }
            else if (saveExt.equalsIgnoreCase("XLSX"))
            { //PDF
                storeProps = new PropertyValue[1];
                storeProps[0] = new PropertyValue();
                storeProps[0].Name = "FilterName";
                storeProps[0].Value = "Calc MS Excel 2007 XML";
            }
        }
        return storeProps;
    }

    private String getExtension(String pathURL)
    {
        if (pathURL == null)
        {
            return "";
        }
        int index = pathURL.lastIndexOf('.');
        return pathURL.substring(index + 1);
    }

    /**
     * Guarda el documento abierto, cambiando la ruta, el nombre o el formato.
     * El cambio de formato es directo, basta con especificar nombre que se
     * desee dar con la extensión del nuevo formato.
     * S&oacute;lo se permiten los formatos: xls, ods, ots, xlt y xlsx.
     * <i>Nota: la capacidad de conversión viene dada por el uso de OpenOffice.org,
     * por lo tanto las limitaciones que la suite presente, tambi&eacute;n se
     * dar&aacute;n aqu&iacute;</i>.
     * @param documentPath String con el nombre y la ruta con el que se quiere
     * guardar el documento.
     * @throws StoreOpenSheetDocumentException
     * @throws UnsupportedOpenSheetDocumentException
     */
    public void saveAs(String documentPath) throws StoreOpenSheetDocumentException, UnsupportedOpenSheetDocumentException
    {

        String docExtension = getExtension(documentPath);
        if (!isValidExtension(docExtension))
        {
            throw new UnsupportedOpenSheetDocumentException("'" + docExtension + "' document file extension is not supported.");
        }

        saveDocument(documentPath, false);

    }

    private String getFileURLFromPath(String documentPath)
    {
        File file = new File(documentPath);
        String path = "file:///" + file.getAbsolutePath();
        
        if (System.getProperty( "os.name" ).startsWith( "Windows" ))
        {
            path = path.replace("\\", "/");
        }

        return path;
    }

    private void saveDocument(String documentPath, boolean storeToURL) throws StoreOpenSheetDocumentException
    {
        try
        {
            String path = getFileURLFromPath(documentPath);
            PropertyValue[] storeProps = getStoreProperties(path);
            XStorable xStorable = (XStorable) UnoRuntime.queryInterface(XStorable.class, xComponent);

            if (storeToURL)
            {
                xStorable.storeToURL(path, storeProps);
            }
            else
            {
                xStorable.storeAsURL(path, storeProps);
                pathURL = path;
            }
        }
        catch (IOException ex)
        {
            throw new StoreOpenSheetDocumentException(ex);
        }
    }

    private boolean isValidExtension(String ext)
    {
        boolean validExtension = false;

        if (ext.equalsIgnoreCase("XLS") || ext.equalsIgnoreCase("XLT")
                || ext.equalsIgnoreCase("ODS") || ext.equalsIgnoreCase("OTS")
                || ext.equalsIgnoreCase("XLSX"))
        {
            validExtension = true;
        }

        return validExtension;
    }

    /**
     * Exporta el documento a un fichero PDF.
     * @param documenPath String que contiene la ruta y el nombre del fichero
     * con el que se va a guardar el documento PDF generado.
     * @throws StoreOpenSheetDocumentException
     * @throws UnsupportedOpenSheetDocumentException
     */
    public void exportToPDF(String documenPath) throws StoreOpenSheetDocumentException, UnsupportedOpenSheetDocumentException
    {
        String ext = getExtension(documenPath);
        if (ext.equalsIgnoreCase("PDF"))
        {
            saveDocument(documenPath, true);
        }
        else
        {
            throw new UnsupportedOpenSheetDocumentException("'" + ext + "' extension document file is not supported with exportToPDF method");
        }
    }

    /**
     * Devuelve un array de String con los nombre de cada una
     * de las hojas de c&aacute;lculo que contiene el documento.
     * @return Devuelve un array de String con los nombres de las hojas de
     * c&aacute;lculo que contiene el documento.
     */
    public String[] getSpreadSheetsNames()
    {
        XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();

        return xSpreadsheets.getElementNames();
    }

    /**
     * Devuelve un objeto para trabajar con la hoja de c&aacute;lculo situada en
     * la posici&oacute;n pasada como par&aacute;metro.
     * @param pos int que representa la posici&oacute;n de la hoja de
     * c&aacute;lculo que se quiere recuperar. La primera posici&oacute;n se
     * corresponde con el valor 0.
     * @return Devuelve un objeto ISpreadSheet.
     * @throws SpreadSheetNoFoundException
     */
    public ISpreadSheet getSpreadSheet(int pos) throws SpreadSheetNoFoundException
    {

        if (pos < 0)
        {
            throw new SpreadSheetNoFoundException("'" + pos + "' position is a negative number. The position must be greater than or equal to 0.");
        }

        XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();
        String[] spreadSheetNames = xSpreadsheets.getElementNames();

        if (pos >= spreadSheetNames.length)
        {
            throw new SpreadSheetNoFoundException("'" + pos + "' position overruns the spreadsheets size (valid range from 0 to " + (spreadSheetNames.length - 1) + ")");
        }

        return getSpreadSheet(spreadSheetNames[pos]);
    }

    /**
     * Devuelve un objeto para trabajar con la hoja de c&aacute;lculo cuyo
     * nombre es el de la cadena pasada como par&aacute;metro.
     * @param spreadSheetName String con el nombre de la hoja de c&aacute;lculo
     * que se quiere recuperar.
     * @return Devuelve un objeto ISpreadSheet.
     * @throws SpreadSheetNoFoundException
     */
    public ISpreadSheet getSpreadSheet(String spreadSheetName) throws SpreadSheetNoFoundException
    {
        ISpreadSheet spreadSheet = null;

        if (spreadSheetName == null)
        {
            throw new SpreadSheetNoFoundException("Invalid spreadSheet name, it is null.");
        }

        try
        {
            XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();

            Object sheet = xSpreadsheets.getByName(spreadSheetName);
            XSpreadsheet xSpreadsheet = (XSpreadsheet) UnoRuntime.queryInterface(XSpreadsheet.class, sheet);
            if (xSpreadsheet != null)
            {
                spreadSheet = new SpreadSheet(xSpreadsheet, spreadSheetName);
            }
        }
        catch (NoSuchElementException ex)
        {
            throw new SpreadSheetNoFoundException(ex);
        }
        catch (WrappedTargetException ex)
        {
            Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
        }

        return spreadSheet;
    }

    /**
     * Crea una nueva hoja de c&aacute;lculo dentro del documento con un nombre
     * por defecto.
     * @return ISpreadSheet que representa al objeto recien creado.
     * @throws OverwriteSpreadSheetException
     */
    public ISpreadSheet addSpreadSheet() throws OverwriteSpreadSheetException
    {

        XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();
        String[] names = xSpreadsheets.getElementNames();
        int suffix = names.length;
        for (int i = 0; i < names.length; i++)
        {
            while (names[i].equals("Sheet" + suffix))
            {
                suffix++;
            }
        }
        String sheetName = "Sheet" + suffix;

        return addSpreadSheet(sheetName);
    }

    /**
     * Crea una nueva hoja de c&aacute;lculo dentro del documento, asign&aacute;ndole
     * como nombre el pasado como par&aacute;metro.
     * @param spreadSheetName String que contiene el nombre que se quiere
     * asignar a la nueva hoja a crear.
     * @return ISpreadSheet que representa al objeto recien creado.
     * @throws OverwriteSpreadSheetException
     */
    public ISpreadSheet addSpreadSheet(String spreadSheetName) throws OverwriteSpreadSheetException
    {
        ISpreadSheet spreadSheet = null;

        if (spreadSheetName == null || spreadSheetName.isEmpty())
        {
            return null;
        }

        if (containsSpreadSheet(spreadSheetName)) 
        {
            throw new OverwriteSpreadSheetException("'"+spreadSheetName+"' spreadsheet already exists in the document");
        }

        try
        {
            XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();
            
            xSpreadsheets.insertNewByName(spreadSheetName, (short) (xSpreadsheets.getElementNames().length));
            spreadSheet = getSpreadSheet(spreadSheetName);
        }
        catch (SpreadSheetNoFoundException ex)
        {
            Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
        }

        return spreadSheet;
    }

    /**
     * Elimina del documento la hoja de c&aacute;lculo situada
     * en la posici&oacute;n pasada como par&aacute;metro. La posici&oacute;n de
     * la primera hoja es 0.
     * @param pos int que contiene la posici&oacute;n de la hoja que se desea
     * eliminar del documento. La posici&oacute;n de la primera hoja es 0.
     * @throws SpreadSheetNoFoundException
     */
    public void deleteSpreadSheet(int pos) throws SpreadSheetNoFoundException
    {
        if (pos < 0)
        {
            throw new SpreadSheetNoFoundException("'" + pos + "' position is a negative number. The position must be greater than or equal to 0.");
        }

        XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();
        String[] spreadSheetNames = xSpreadsheets.getElementNames();

        if (pos >= spreadSheetNames.length)
        {
            throw new SpreadSheetNoFoundException("'" + pos + "' position overruns the spreadsheets size (valid range from 0 to " + (spreadSheetNames.length - 1) + ")");
        }

        deleteSpreadSheet(spreadSheetNames[pos]);

    }

    /**
     * Elimina del documento la hoja de c&aacute;lculo cuyo
     * nombre es el pasado como par&aacute;metro.
     * @param spreadSheetName String que contiene el nombre de la hoja que se
     * desea eliminar del documento.
     * @throws SpreadSheetNoFoundException
     */
    public void deleteSpreadSheet(String spreadSheetName) throws SpreadSheetNoFoundException
    {
        if (spreadSheetName == null)
        {
            throw new SpreadSheetNoFoundException("Invalid spreadSheet name, it is null.");
        }

        try
        {
            XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();
            xSpreadsheets.removeByName(spreadSheetName);
        }
        catch (NoSuchElementException ex)
        {
            throw new SpreadSheetNoFoundException(ex);
        }
        catch (WrappedTargetException ex)
        {
            Logger.getLogger(OpenSheetDocument.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Guarda los cambios realizados en el documento.
     * @throws StoreOpenSheetDocumentException
     */
    public void save() throws StoreOpenSheetDocumentException
    {        
        try
        {
            XStorable xStorable = (XStorable) UnoRuntime.queryInterface(XStorable.class, xComponent);
            xStorable.store();
        }
        catch (IOException ex)
        {
            throw new StoreOpenSheetDocumentException(ex);
        }
    }

    /**
     * Indica si en el documento existe alguna hoja de
     * c&aacute;lculo con el nombre pasado como par&aacute;metro.
     * @param name String con el nombre de una hoja de c&aacute;lculo.
     * @return true si el documento tiene una hoja con ese nombre, y false en
     * caso contrario.
     */
    public boolean containsSpreadSheet(String name)
    {
        String[] spreadSheetsNames = getSpreadSheetsNames();
        for (String spreadSheetName : spreadSheetsNames)
        {
            if (spreadSheetName.equals(name))
                return true;
        }
        
        return false;
    }    
}
