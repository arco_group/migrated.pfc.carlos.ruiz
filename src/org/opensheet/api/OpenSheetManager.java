/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.api;

import org.opensheet.api.exceptions.OverwriteOpenSheetDocumentException;
import com.sun.star.beans.PropertyValue;
import com.sun.star.comp.helper.BootstrapException;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XDesktop;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opensheet.api.exceptions.LoadOpenSheetDocumentException;
import org.opensheet.api.exceptions.UnsupportedOpenSheetDocumentException;
import org.opensheet.api.unoconnection.Bootstrap;

/**
 * Clase que establece un conexi&oacute;n con el OpenOffice.org, arrancando un
 * nuevo proceso si es necesario, de manera transparente y permite crear y abrir
 * documentos de hojas de c&aacute;lculo devolviendo la interfaz
 * IOpenSheetDocument para trabajar con ellos.
 * @author cruiz
 */
public class OpenSheetManager
{

    private XComponentContext xComponentContext;
    private XDesktop xDesktop;
    private XMultiComponentFactory xServiceManger;

    /**
     * Constructor que además de crear un objeto OpenSheetManager, realiza la
     * conexión autom&aacute;tica con OpenOffice.org.
     * @throws BootstrapException
     */
    public OpenSheetManager() throws BootstrapException
    {
        xComponentContext = Bootstrap.bootstrap();
    }

    /**
     * Crea un nuevo documento de hojas de c&aacute;lculo y devuelve un objeto
     * IOpenSheetDocument para permitir operar con &eacute;l. Si ya existe
     * un documento con ese mismo nombre en la ruta indicada se devolver&aacute;
     * una excepci&oacute;n.
     * @param documentPath String con la ruta al documento a generar
     * @return Devuelve un objeto IOpenSheetDocument que permite trabajar con el
     * documento creado.
     * @throws OverwriteOpenSheetDocumentException
     * @throws UnsupportedOpenSheetDocumentException
     */
    public IOpenSheetDocument createDocument(String documentPath) throws OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException
    {
        if (new File(documentPath).exists())
        {
            throw new OverwriteOpenSheetDocumentException("'" + documentPath + "' document could not be created because it already exists");
        }        

        IOpenSheetDocument oSheetDocument = loadOpenSheetDocument(documentPath, true);
        return oSheetDocument;
    }

    /**
     * Abre un nuevo documento de hojas de c&aacute;lculo y devuelve un objeto
     * IOpenSheetDocument para permitir operar con &eacute;l.
     * @param documentPath String con la ruta al documento de hojas de
     * c&aacute;lculo que se desea abrir.
     * @return Devuelve un objeto IOpenSheetDocument que permite trabajar con el
     * documento abierto.
     * @throws UnsupportedOpenSheetDocumentException
     * @throws LoadOpenSheetDocumentException
     */
    public IOpenSheetDocument openDocument(String documentPath) throws UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {
        if (new File(documentPath).exists() == false)
        {
            throw new LoadOpenSheetDocumentException("'" + documentPath + "' document could not be open because it doesn't exist");
        }

        IOpenSheetDocument oSheetDocument = loadOpenSheetDocument(documentPath, false);
        return oSheetDocument;
    }

    private IOpenSheetDocument loadOpenSheetDocument(String documentPath, boolean newDocument) throws UnsupportedOpenSheetDocumentException
    {
        xServiceManger = xComponentContext.getServiceManager();
        Object desktop;

        IOpenSheetDocument oSheetDocument = null;

        try
        {
            desktop = xServiceManger.createInstanceWithContext("com.sun.star.frame.Desktop", xComponentContext);
            xDesktop = (XDesktop) UnoRuntime.queryInterface(XDesktop.class, desktop);
            XComponentLoader xComponentLoader = (XComponentLoader) UnoRuntime.queryInterface(XComponentLoader.class, desktop);
            oSheetDocument = new OpenSheetDocument(xComponentLoader, documentPath, newDocument);
        }
        catch (com.sun.star.uno.Exception ex)
        {
            Logger.getLogger(OpenSheetManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return oSheetDocument;
    }

    /**
     * Libera los recursos remotos de OpenOffice.org utilizados y cierra la
     * conexi&oacute;n abierta.
     * Este m&eacute;todo debe ser llamado cuando ya no se vaya a trabajar con
     * los objetos de OpenSheetAPI.
     */
    public void terminate() {

        if (null != xDesktop)
        {
            xDesktop.terminate();
            xDesktop = null;
            xServiceManger = null;
            xComponentContext = null;
        }
                
        System.gc();
    }
   
}
