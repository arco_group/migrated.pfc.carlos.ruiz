/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.command;

import com.sun.star.comp.helper.BootstrapException;
import java.io.File;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opensheet.api.OpenSheetManager;
import org.opensheet.api.exceptions.LoadOpenSheetDocumentException;
import org.opensheet.api.exceptions.OverwriteOpenSheetDocumentException;
import org.opensheet.api.exceptions.UnsupportedOpenSheetDocumentException;
import org.opensheet.command.exceptions.LoadOpenSheetDataFileException;
import org.opensheet.command.exceptions.OpenSheetCommandInvalidParamsException;
import org.opensheet.command.exceptions.OpenSheetPropertyFormatException;
import org.opensheet.command.exceptions.OpenSheetRunScriptException;

/**
 * Clase principal de OpenSheet Command que permite ejecutar scripts escritos en
 * Groovy e inyectarles las propiedades le&iacute;das del fichero de datos.
 * Para ello hace uso de la clase ScriptExecuter.
 *
 * El comando espera como argumentos:
 * -script=<i>script_Groovy</i>
 * -data=<i>fichero_datos</i>
 * @author cruiz
 */
public class OpenSheetCommand
{

    public static String HELP_OPTION = "-help";
    public static String SCRIPT_FILE_OPTION = "-script=";
    public static String DATA_FILE_OPTION = "-data=";
    private String scriptFile;
    private String dataFile;
    private boolean showHelp;

    /**
     * Constructor que devuelve un objeto OpenSheetCommand al que deben ser
     * asignados los par&aacute;metros necesarios antes de realizar la
     * ejecuci&oacute;n de un script a trav&eacute;s del m&eacute;odo <i>run</i>.
     */
    public OpenSheetCommand()
    {
        scriptFile = null;
        showHelp = false;
    }

    /**
     * Asigna al comando los par&aacute;metros necesarios para realizar una
     * ejecuci&oacute;n de un script. <br>
     * El comando espera como argumentos: <br>
     * -script=<i>script_Groovy</i> <br>
     * -data=<i>fichero_datos</i> <br>
     * <br>
     * Tambi&eacute;n se le puede pasar el argumento -help para que muestre por
     * pantalla la ayuda de uso del comando.
     * @param args
     * @throws OpenSheetCommandInvalidParamsException
     */
    public void setParams(String[] args) throws OpenSheetCommandInvalidParamsException
    {
        switch (args.length)
        {
            case 0:
                showHelp = true;
                break;
            case 1:
                if (!args[0].equals(HELP_OPTION))
                {
                    throw new OpenSheetCommandInvalidParamsException("Call the method with a parameter is only allowed to '-help'");
                }
                showHelp = true;
                break;
            case 2:
                for (int i = 0; i < args.length; i++)
                {
                    if (args[i].startsWith(SCRIPT_FILE_OPTION))
                    {
                        scriptFile = args[i].substring(SCRIPT_FILE_OPTION.length());
                    }
                    else if (args[i].startsWith(DATA_FILE_OPTION))
                    {
                        dataFile = args[i].substring(DATA_FILE_OPTION.length());
                    }
                    else
                    {
                        throw new OpenSheetCommandInvalidParamsException("Invalid argument:" + "'" + args[i] + "'");
                    }
                }

                File data = new File(dataFile);
                File script = new File(scriptFile);
                String msgError = "";

                if (!script.exists() || !script.canRead())
                {
                    msgError += "'" + scriptFile + "' script file does not exists or does not have permissions to read\n";
                }

                if (!data.exists() || !data.canRead())
                {
                    msgError += "'" + dataFile + "' data file does not exists or does not have permissions to read";
                }

                if (!msgError.isEmpty())
                {
                    throw new OpenSheetCommandInvalidParamsException(msgError);
                }
                break;
            default:
                throw new OpenSheetCommandInvalidParamsException("Call the method with more than two parameters is not allowed");
        }
    }

    /**
     * Ejecuta el script asignado por par&aacute;metros inyect&aacute;ndole las
     * propiedades le&iacute;das del fichero de datos. Al finalizar la
     * ejecuci&oacute;n devuelve un mapa con las variables del script y sus
     * valores.
     * @return Devuelve un mapa con las variables del script y sus
     * valores.
     * @throws OpenSheetRunScriptException
     * @throws OpenSheetCommandInvalidParamsException
     */
    public Map run() throws OpenSheetRunScriptException, OpenSheetCommandInvalidParamsException
    {
        if (showHelp)
        {
            showHelpInfo();
            return null;
        }

        if (!checkParams())
        {
            throw new OpenSheetCommandInvalidParamsException("Params weren't be assigned using 'setParams' methods or they are invalid");
        }

        OpenSheetManager openSheetManager = null;                        
        try
        {
            openSheetManager = createOpenSheetManager();
            ScriptExecuter executer = createScriptExecuter(openSheetManager);
            executer.run(new File(scriptFile));
            return executer.getBinding().getVariables();
        }
        catch (BootstrapException ex)
        {
            throw new OpenSheetRunScriptException(ex);
        }
        catch (LoadOpenSheetDataFileException ex)
        {
            throw new OpenSheetRunScriptException(ex);
        }
        catch (OpenSheetPropertyFormatException ex)
        {
            throw new OpenSheetRunScriptException(ex);
        }
        catch (OverwriteOpenSheetDocumentException ex)
        {
            throw new OpenSheetRunScriptException(ex);
        }
        catch (UnsupportedOpenSheetDocumentException ex)
        {
            throw new OpenSheetRunScriptException(ex);
        }
        catch (LoadOpenSheetDocumentException ex)
        {
            throw new OpenSheetRunScriptException(ex);
        }
        finally
        {
            if (openSheetManager != null)
            {
                openSheetManager.terminate();
            }
        }        
    }

    private void showHelpInfo()
    {
        System.out.println("\nUsage: java -jar OpenSheetCommand.jar -data<file> -script<file>\n");
    }

    /**
     * Devuelve el fichero que tiene asignado como script para ejecutar el
     * comando.
     * @return File con el script de Groovy asignado para ser ejecutado por el
     * comando.
     */
    public File getScriptFile()
    {
        return new File(scriptFile);
    }

    private OpenSheetManager createOpenSheetManager() throws BootstrapException
    {
        return new OpenSheetManager();
    }

    private ScriptExecuter createScriptExecuter(OpenSheetManager manager) throws LoadOpenSheetDataFileException, OpenSheetPropertyFormatException, OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {
        return new ScriptExecuter(manager, dataFile);
    }

    private boolean checkParams()
    {
        if (scriptFile == null && dataFile == null)
        {
            return false;
        }
        return true;
    }

    /**
     * Devuelve el fichero que tiene asignado como ficheros de datos para
     * inyectar los valores de sus propiedades al ejecutar el script con el
     * comando.
     * @return String con la ruta al fichero de datos que tiene asignado el
     * comando.
     */
    public String getDataFile()
    {
        return dataFile;
    }
}
