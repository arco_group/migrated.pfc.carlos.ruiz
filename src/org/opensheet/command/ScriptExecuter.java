/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.command;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.codehaus.groovy.control.CompilationFailedException;
import org.opensheet.api.IOpenSheetDocument;
import org.opensheet.api.OpenSheetManager;
import org.opensheet.api.exceptions.LoadOpenSheetDocumentException;
import org.opensheet.api.exceptions.OverwriteOpenSheetDocumentException;
import org.opensheet.api.exceptions.UnsupportedOpenSheetDocumentException;
import org.opensheet.command.exceptions.LoadOpenSheetDataFileException;
import org.opensheet.command.exceptions.OpenSheetRunScriptException;
import org.opensheet.command.exceptions.OpenSheetPropertyFormatException;

/**
 * Clase que permite ejecutar scripts escritos en Groovy e inyectarles las
 * propiedades le&iacute;das del fichero de datos.
 * Existen propiedades especiales que el comando inyecta como objetos, el resto
 * se inyectan como cadena a las variables que tienen el mismo nombre que la
 * clave de la propiedad.
 *
 * @author cruiz
 */
public class ScriptExecuter
{

    public static String NUMBER_PROPERTY_NAME = "@number;";
    public static String LIST_PROPERTY_NAME = "@list;";
    public static String LIST_PROPERTY_SEPARATOR = ",";
    public static String OPENSHEET_MANAGER_PROPERTY_NAME = "@OpenSheetManager;";
    public static String OPENSHEET_DOCUMENT_PROPERTY_NAME = "@OpenSheetDocument;";
    public static String OPENSHEET_DOCUMENT_PROPERTY_SEPARATOR = ",";
    public static String OPENSHEET_DOCUMENT_PROPERTY_CREATE_PARAM = "create";
    public static String OPENSHEET_DOCUMENT_PROPERTY_OPEN_PARAM = "open";

    private Binding binding;
    private OpenSheetManager openSheetManager;
    GroovyShell groovyShell;

    /**
     * Constructor que crea un ScriptExecuter.
     * @param openSheetManager OpenSheetManager usado para crear los objetos de
     * OpenSheetAPI que hay que inyectar al script a ejecutar si se usan los
     * valores especiales en las propiedades del fichero de datos.
     * @param dataFile String con la ruta al fichero de datos del que se van a leer
     * las propiedades para crear los objetos para inyectar a las variables del
     * script que se vaya a ejecutar.
     * @throws OpenSheetPropertyFormatException
     * @throws OverwriteOpenSheetDocumentException
     * @throws UnsupportedOpenSheetDocumentException
     * @throws LoadOpenSheetDocumentException
     * @throws LoadOpenSheetDataFileException
     */
    public ScriptExecuter(OpenSheetManager openSheetManager, String dataFile) throws OpenSheetPropertyFormatException, 
            OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException,
            LoadOpenSheetDataFileException
    {
        try
        {
            Properties properties = new Properties();
            properties.load(new FileInputStream(dataFile));
            InitScriptExecuter(openSheetManager, properties);
        }
        catch (IOException ex)
        {
            throw new LoadOpenSheetDataFileException(ex);
        }
    }

    /**
     *  * Constructor que crea un ScriptExecuter.
     * @param openSheetManager openSheetManager OpenSheetManager usado para
     * crear los objetos de OpenSheetAPI que hay que inyectar al script a
     * ejecutar si se usan los valores especiales en las propiedades del fichero
     * de datos.
     * @param properties Properties que contiene las propiedades para crear los
     * objetos para inyectar a las variables del script que se vaya a ejecutar.
     * @throws OpenSheetPropertyFormatException
     * @throws OverwriteOpenSheetDocumentException
     * @throws UnsupportedOpenSheetDocumentException
     * @throws LoadOpenSheetDocumentException
     */
    public ScriptExecuter(OpenSheetManager openSheetManager, Properties properties) throws OpenSheetPropertyFormatException,
            OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {
                InitScriptExecuter(openSheetManager, properties);
    }

    private void InitScriptExecuter(OpenSheetManager openSheetManager, Properties properties) throws OpenSheetPropertyFormatException, 
            OverwriteOpenSheetDocumentException,  UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {
        this.openSheetManager = openSheetManager;
        groovyShell = new GroovyShell();

        Map variablesFromDataFile = getVariables(properties);
        binding = new Binding(variablesFromDataFile);
    }

    private Map getVariables(Properties properties) throws OpenSheetPropertyFormatException,
            OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {
        Map variables = new HashMap();

        for (String key : properties.stringPropertyNames())
        {
            variables.put(key, getParsedValue(properties.getProperty(key)));
        }

        return variables;
    }

    private Object getParsedValue(String value) throws OpenSheetPropertyFormatException,
            OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {
        if (value.startsWith(NUMBER_PROPERTY_NAME))
        {
            return getDoubleFromParsedValue(value);
        }

        if (value.startsWith(LIST_PROPERTY_NAME))
        {
            return getListFromParsedValue(value);
        }

        if (value.startsWith(OPENSHEET_MANAGER_PROPERTY_NAME))
        {
            return openSheetManager;
        }

        if (value.startsWith(OPENSHEET_DOCUMENT_PROPERTY_NAME))
        {
            return getOpenSheetDocumentFromParsedValue(value);
        }

        return value;
    }

    private Double getDoubleFromParsedValue(String value) throws OpenSheetPropertyFormatException
    {
        try
        {
            Double newValue = Double.parseDouble(value.substring(NUMBER_PROPERTY_NAME.length()));
            return newValue;
        }
        catch (NumberFormatException ex)
        {
            throw new OpenSheetPropertyFormatException("'" + value.substring(NUMBER_PROPERTY_NAME.length())
                    + "' assigned value is invalid to " + NUMBER_PROPERTY_NAME);
        }
    }

    private List getListFromParsedValue(String value)
    {
        List parsedList = new ArrayList();

        // Las comas contenidas en una subcadena "//," son ignoradas a la hora de realizar la division
        String[] listValues = value.substring(LIST_PROPERTY_NAME.length()).split("(?<!(//))" + LIST_PROPERTY_SEPARATOR);

        for (String listValue : listValues)
        {
            listValue = listValue.replaceAll("//" + LIST_PROPERTY_SEPARATOR, LIST_PROPERTY_SEPARATOR);

            parsedList.add(listValue);
        }

        return parsedList;
    }

    private IOpenSheetDocument getOpenSheetDocumentFromParsedValue(String value) throws OpenSheetPropertyFormatException,
            OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {

        String[] params = value.substring(OPENSHEET_DOCUMENT_PROPERTY_NAME.length()).split(OPENSHEET_DOCUMENT_PROPERTY_SEPARATOR);

        if (params.length != 2)
        {
            throw new OpenSheetPropertyFormatException("The number of params to use '"+OPENSHEET_DOCUMENT_PROPERTY_NAME
                    + "' must be 2 (action, document path)");
        }

        IOpenSheetDocument document = null;

        if (params[0].equals(OPENSHEET_DOCUMENT_PROPERTY_CREATE_PARAM))
        {
            document = openSheetManager.createDocument(params[1]);
        }
        else if (params[0].equals(OPENSHEET_DOCUMENT_PROPERTY_OPEN_PARAM))
        {
            document = openSheetManager.openDocument(params[1]);
        }
        else
        {
             throw new OpenSheetPropertyFormatException("'" + value.substring(OPENSHEET_DOCUMENT_PROPERTY_NAME.length())
                + "' assigned params are invalid to " + OPENSHEET_DOCUMENT_PROPERTY_NAME);
        }

        return document;
    }

    /**
     * Devuelve un objeto Binding con cada una de las variables del script y su
     * valor. Antes de la ejecuci&oacute;n contiene las variables con el valor a
     * inyectar; y despu&eacute;s contiene el valor de todas las variables con
     * el &uacute;ltimo valor asignado durante la ejecuci&oacute;n.
     * @return
     */
    public Binding getBinding()
    {
        return binding;
    }

    void setGroovyShell(GroovyShell groovyShell)
    {
        this.groovyShell = groovyShell;
    }

    /**
     * Ejecute un script de Groovy inyect&aacute;ndole los objetos a las variables
     * indicadas por el fichero de datos o las propiedades pasadas al constructor
     * al crear el objeto ScriptExecuter.
     * @param script File que representa al script de Groovy que se desea ejecutar.
     * @throws OpenSheetRunScriptException
     */
    public void run(File script) throws OpenSheetRunScriptException
    {
        try
        {
            Script groovyScript = groovyShell.parse(script);
            groovyScript.setBinding(binding);
            groovyScript.run();
        }
        catch (CompilationFailedException ex)
        {                        
            throw new OpenSheetRunScriptException(ex);
        }
        catch (IOException ex)
        {
            throw new OpenSheetRunScriptException(ex);
        }
    }
}
