/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.command;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.opensheet.command.exceptions.OpenSheetCommandInvalidParamsException;
import org.opensheet.command.exceptions.OpenSheetRunScriptException;

/**
 * Clase ejecutable que crea un objeto OpenSheetCommand al que le pasa los
 * argumentos recibidos y lo ejecuta.
 * @author cruiz
 */
public class Main {
    public static void main(String[] args)
    {
        try
        {
            OpenSheetCommand command = new OpenSheetCommand();
            command.setParams(args);
            command.run();
        }
        catch (OpenSheetRunScriptException ex)
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(-1);
        }
        catch (OpenSheetCommandInvalidParamsException ex)
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(-2);
        }

        System.exit(0);
    }
}
