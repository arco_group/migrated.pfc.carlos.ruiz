/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.scripts;
import org.opensheet.api.*;
import org.opensheet.api.exceptions.*;

def deleteSheetsByNameTest()
{
    String doc = "testOpenSheet.ods";
    new File("testOpenSheet.ods").delete();
    OpenSheetManager manager = new OpenSheetManager();
    try
    {
        IOpenSheetDocument oSheetDoc = manager.createDocument(doc);
        assert null != oSheetDoc.addSpreadSheet("element1");
        assert null != oSheetDoc.addSpreadSheet("1");
        assert null != oSheetDoc.addSpreadSheet("hoja1 nueva.1");

        assert oSheetDoc.containsSpreadSheet("element1") == true;
        assert oSheetDoc.containsSpreadSheet("1") == true;
        assert oSheetDoc.containsSpreadSheet("hoja1 nueva.1") == true;

        deleteString = "element1;1;hoja1 nueva.1"
        OpenSheetUtils.deleteSheets(deleteString, oSheetDoc, false);

        assert oSheetDoc.containsSpreadSheet("element1") == false;
        assert oSheetDoc.containsSpreadSheet("1") == false;
        assert oSheetDoc.containsSpreadSheet("hoja1 nueva.1") == false;
        
        oSheetDoc.close();
        new File("testOpenSheet.ods").delete();
    }
    finally {
        manager.terminate();
    }
}

def deleteSheetsByPos()
{
    String doc = "testOpenSheet.ods";
    new File("testOpenSheet.ods").delete();
    OpenSheetManager manager = new OpenSheetManager();
    try
    {
        IOpenSheetDocument oSheetDoc = manager.createDocument(doc);
        assert null != oSheetDoc.addSpreadSheet("element1");
        
        assert oSheetDoc.containsSpreadSheet("element1") == true;
        assert oSheetDoc.getSpreadSheetsNames().length == 4;

        deleteString = "2;1;0" //another posibility is "0;0;0"
        OpenSheetUtils.deleteSheets(deleteString, oSheetDoc, true);

        assert oSheetDoc.containsSpreadSheet("element1") == true;
        assert oSheetDoc.getSpreadSheetsNames().length == 1;

        oSheetDoc.close();
        new File("testOpenSheet.ods").delete();
    }
    finally {
        manager.terminate();
    }
}

def insertNumericValuesToCellsWithSheetNameTest()
{
    String doc = "testOpenSheet.ods";
    new File("testOpenSheet.ods").delete();
    OpenSheetManager manager = new OpenSheetManager();
    try
    {
        IOpenSheetDocument oSheetDoc = manager.createDocument(doc);
        ISpreadSheet sheet1 = oSheetDoc.addSpreadSheet("element1");
        ISpreadSheet sheet2 = oSheetDoc.addSpreadSheet("1");
        ISpreadSheet sheet3 = oSheetDoc.addSpreadSheet("hoja1 nueva.1");

        insertString = "element1:a1=2.34,b3=153411.3456;1:cg23=7;hoja1 nueva.1:c2834=2799856"
        OpenSheetUtils.insertValuesToCells(insertString, oSheetDoc, false, true);

        assert sheet1.getCellValue(new CellPosition("a1")) == 2.34;
        assert sheet1.getCellValue(new CellPosition("b3")) == 153411.3456;

        assert sheet2.getCellValue(new CellPosition("cg23")) == 7;

        assert sheet3.getCellValue(new CellPosition("c2834")) == 2799856;    
        oSheetDoc.close();
        new File("testOpenSheet.ods").delete();
    }
    finally {
        manager.terminate();
    }
}

def insertTextValuesToCellsWithSheetPosTest()
{
    String doc = "testOpenSheet.ods";
    new File("testOpenSheet.ods").delete();
    OpenSheetManager manager = new OpenSheetManager();
    try
    {
        IOpenSheetDocument oSheetDoc = manager.createDocument(doc);
        insertString = "0:a1=2.34,b3=prueba;1:cg23=hola de nuevo;2:c2834=prueba 14";
        OpenSheetUtils.insertValuesToCells(insertString, oSheetDoc, true, false);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(0);
        assert spreadSheet.getCellText(new CellPosition("a1")) == "2.34";
        assert spreadSheet.getCellText(new CellPosition("b3")) == "prueba";

        spreadSheet = oSheetDoc.getSpreadSheet(1);
        assert spreadSheet.getCellText(new CellPosition("cg23")) == "hola de nuevo";

        spreadSheet = oSheetDoc.getSpreadSheet(2);
        assert spreadSheet.getCellText(new CellPosition("c2834")) == "prueba 14";     
        oSheetDoc.close();
        new File("testOpenSheet.ods").delete();
    }
    finally {
        manager.terminate();
    }
}

def extractTextValuesWithSheetPosTest()
{
    String doc = "testOpenSheet.ods";
    new File(doc).delete();
    OpenSheetManager manager = new OpenSheetManager();
    try
    {
        IOpenSheetDocument oSheetDoc = manager.createDocument(doc);
        insertString = "0:a1=2.34,b3=prueba;1:cg23=hola de nuevo;2:c2834=prueba 14"
        OpenSheetUtils.insertValuesToCells(insertString, oSheetDoc, true, false);

        extractString = "0:a1,b3;1:cg23;2:c2834";

        Map properties = OpenSheetUtils.extractValuesFromCells(extractString, oSheetDoc, true);

        assert properties.get("0.a1") == "2.34";
        assert properties.get("0.b3") == "prueba";
        assert properties.get("1.cg23") == "hola de nuevo";
        assert properties.get("2.c2834") == "prueba 14";

        oSheetDoc.close();
        new File(doc).delete();
    }
    finally {
        manager.terminate();
    }
}

def extractNumericValuesWithSheetNamesTest()
{
    String doc = "testOpenSheet.ods";
    new File(doc).delete();
    OpenSheetManager manager = new OpenSheetManager();
    try
    {
        IOpenSheetDocument oSheetDoc = manager.createDocument(doc);
        ISpreadSheet sheet1 = oSheetDoc.addSpreadSheet("element1");
        ISpreadSheet sheet2 = oSheetDoc.addSpreadSheet("1");
        ISpreadSheet sheet3 = oSheetDoc.addSpreadSheet("hoja1 nueva.1");
        
        insertString = "element1:a1=2.34,b3=153411.3456;1:cg23=7;hoja1 nueva.1:c2834=2799856";
        OpenSheetUtils.insertValuesToCells(insertString, oSheetDoc, false, true);

        extractString = "element1:a1,b3;1:cg23;hoja1 nueva.1:c2834";
        
        Map properties = OpenSheetUtils.extractValuesFromCells(extractString, oSheetDoc, false);

        assert properties.get("element1.a1") == "2.34";
        assert properties.get("element1.b3") == "153411.3456";
        assert properties.get("1.cg23") == "7.0";
        assert properties.get("hoja1 nueva.1.c2834") == "2799856.0";

        oSheetDoc.close();
        new File(doc).delete();                
    }
    finally {
        manager.terminate();
    }
}


// Tests getNamesList
assert OpenSheetUtils.getNamesList("prueba;element1;with blank spaces;value33;lastElement") == ["prueba","element1","with blank spaces","value33","lastElement"]

// Tests splitSheetsWithCellLine
assert OpenSheetUtils.splitSheetsWithCellLine("Sheet1:a1=2.34,b3=prueba;1:cg23=hola de nuevo;hoja1 nueva.1:c2834=prueba 14") == ["Sheet1":"a1=2.34,b3=prueba","1":"cg23=hola de nuevo","hoja1 nueva.1":"c2834=prueba 14"]
assert OpenSheetUtils.splitSheetsWithCellLine("hola") == [:]
assert OpenSheetUtils.splitSheetsWithCellLine("hola:") == [:]
assert OpenSheetUtils.splitSheetsWithCellLine("hola:34") == ["hola":"34"]
assert OpenSheetUtils.splitSheetsWithCellLine("hola:34;prueba :otra mas") == ["hola":"34","prueba ":"otra mas"]
assert OpenSheetUtils.splitSheetsWithCellLine("hola:34prueba :otra mas") == [:]

// Tests splitCellsFromCellLine
assert OpenSheetUtils.splitCellsFromCellLine("a1=2.34") == ["a1":"2.34"]
assert OpenSheetUtils.splitCellsFromCellLine("a1=2.34,b3=prueba 14") == ["a1":"2.34","b3":"prueba 14"]
assert OpenSheetUtils.splitCellsFromCellLine("a1") == [:]
assert OpenSheetUtils.splitCellsFromCellLine("a1=") == [:]
assert OpenSheetUtils.splitCellsFromCellLine("1=2") == [:]
assert OpenSheetUtils.splitCellsFromCellLine("a1=2 b3=4") == [:]

// Tests getSheetsAndCells
assert OpenSheetUtils.getSheetsAndCells("Sheet1:a1=2.34,b3=prueba;1:cg23=hola de nuevo;hoja1 nueva.1:c2834=prueba 14") \
== ["Sheet1":["a1":"2.34","b3":"prueba"],"1":["cg23":"hola de nuevo"],"hoja1 nueva.1":["c2834":"prueba 14"]];
assert OpenSheetUtils.getSheetsAndCells("sheet1") == [:];
assert OpenSheetUtils.getSheetsAndCells("sheet1:") == [:];
assert OpenSheetUtils.getSheetsAndCells("sheet1:a1") == [:];
assert OpenSheetUtils.getSheetsAndCells("sheet1:a1=") == [:];
assert OpenSheetUtils.getSheetsAndCells("sheet1:a1=;") == [:];
assert OpenSheetUtils.getSheetsAndCells("sheet1:a1=b1") == ["sheet1":["a1":"b1"]];
assert OpenSheetUtils.getSheetsAndCells("sheet1:a1=b1;") == [:];
assert OpenSheetUtils.getSheetsAndCells("sheet1:1b=b1") == [:];

deleteSheetsByNameTest();
deleteSheetsByPos();
insertTextValuesToCellsWithSheetPosTest();
insertNumericValuesToCellsWithSheetNameTest();
extractTextValuesWithSheetPosTest();
extractNumericValuesWithSheetNamesTest();

println("All Test OK***")
