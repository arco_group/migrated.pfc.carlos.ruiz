/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.scripts
import org.opensheet.api.*
import org.opensheet.api.exceptions.*
import org.opensheet.scripts.OpenSheetUtils;

//Keys from Properties File
openSheetDocument;
logFileName;
deleteSheets;
deleteSheetsByPosition;
addSheets;
insertCellsValues;
insertCellsValuesBySheetPosition;
insertCellsNumericValues;
insertCellsNumericValuesBySheetPosition;
extractCellsValues;
extractCellsValuesBySheetPosition;
extractCellsValuesFile;
saveDocumentTo;

class OpenSheetUtils
{
    static getNamesList(string)
    {
        string.split(";");
    }

    static splitSheetsWithCellLine(string)
    {
        Map sheets = [:];

        if (!string.matches("([^;:]+:[^;:]+)(;[^;:]+:[^;:]+)*"))
            return sheets;

        def sheetsLines = string.split(";");
        for (line in sheetsLines)
        {
             def separateSheetAndCells = line.split(":");
             sheets.put(separateSheetAndCells[0],separateSheetAndCells[1]);
        }

        sheets;
    }

    static splitCellsFromCellLine(string)
    {
        Map cells = [:];

        if (!string.matches("([a-zA-Z]+[0-9]+=[^=,]+)(,[a-zA-Z]+[0-9]+=[^=,]+)*"))
            return cells;

        def separateCells = string.split(",");

        for (cellLine in separateCells)
        {
            def separateCellAndValue = cellLine.split("=");
            cells.put(separateCellAndValue[0],separateCellAndValue[1]);
        }
        cells;
    }

    static getSheetsAndCells(string)
    {
        Map sheetAndCells = [:];

        Map sheetNamesMap = splitSheetsWithCellLine(string);

        def keys = sheetNamesMap.keySet();

        for (key in keys)
        {
            Map cells = splitCellsFromCellLine(sheetNamesMap.get(key));
            if (cells != [:])
            {
                sheetAndCells.put(key, cells);
            }
        }

        sheetAndCells;
    }

    static deleteSheets(deleteString, IOpenSheetDocument document, boolean sheetPosition)
    {
        def sheets = getNamesList(deleteString);
        for (sheet in sheets)
        {
            if (sheetPosition)
                sheet = sheet.toInteger();

            document.deleteSpreadSheet(sheet);
        }
    }

    static addSheets(addString, IOpenSheetDocument document)
    {
        def sheets = getNamesList(addString);
        for (sheet in sheets)
        {
            document.addSpreadSheet(sheet);
        }
    }

    static insertValuesToCells(insertString, IOpenSheetDocument document, boolean sheetPosition, boolean numericValue)
    {
        Map sheetsMap = getSheetsAndCells(insertString);

        def sheetsNames = sheetsMap.keySet();

        for (sheetName in sheetsNames)
        {
            def sheetId;

            if (sheetPosition)
                sheetId = sheetName.toInteger();
            else
                sheetId = sheetName;

            ISpreadSheet spreadSheet = document.getSpreadSheet(sheetId);

            def cellsMap = sheetsMap.get(sheetName);
            def cellsNames = cellsMap.keySet();

            for (cellName in cellsNames)
            {
                def cellValue = cellsMap.get(cellName)

                if (numericValue)
                    cellValue = cellValue.toDouble();

                spreadSheet.setCellValue(new CellPosition(cellName), cellValue);
            }
        }
    }

    static extractValuesFromCells(extractString, IOpenSheetDocument document, boolean sheetPosition)
    {

        Map properties = [:];

        Map sheetsMap = splitSheetsWithCellLine(extractString);

        def sheetsNames = sheetsMap.keySet();

        for (sheetName in sheetsNames)
        {
            def sheetId;

            if (sheetPosition)
                sheetId = sheetName.toInteger();
            else
                sheetId = sheetName;

            ISpreadSheet spreadSheet = document.getSpreadSheet(sheetId);

            def cellsString = sheetsMap.get(sheetName);
            def cellsNames = cellsString.split(",");

            for (cellName in cellsNames)
            {
                properties.put(sheetName+"."+cellName, spreadSheet.getCellContent(new CellPosition(cellName))+"");
            }
        }
        properties;
    }
}

def writeExtractCellValuesFile(fileName, properties)
{
    def file = new File(fileName);
    properties.each(){
        key,value->string = key+" = "+value+"\n";
        file.append(string);
    }
}

def writeToLog(logFile, message)
{
    logFile.append("["+new Date().format('yyyy-MM-dd HH:mm:ss')+"]\t");
    logFile.append(message+"\n");
}


def runScript(){

    def version = "0.0.0.1"
    def productName = "OpenSheet Groovy Script "

    if (logFileName == "")
        logFileName = "opensheet.log";

    if (extractCellsValuesFile == "")
        extractCellsValuesFile = "opensheet.out"

    def logFile = new File(logFileName);

    writeToLog(logFile, "** Running "+productName+" (version: "+version+") **")


    /*
    * 1. Add All SpreadSheets
    */
    if (addSheets != "")
    {
        writeToLog(logFile, "Adding sheets using 'addSheets' property: "+ addSheets);
        OpenSheetUtils.addSheets(addSheets, openSheetDocument);
        writeToLog(logFile, "Task completed");
    }

    /*
    * 2. Delete Sheets Block (To permit delete all existing sheets in document (when it was opened), the first operation is addSheets).
    */
    //2.1 Delete sheets of 'deleteSheets' property
    if (deleteSheets != "")
    {
        writeToLog(logFile, "Deleting sheets using 'deleteSheets' property: "+ deleteSheets);
        OpenSheetUtils.deleteSheets(deleteSheets, openSheetDocument, false);        
        writeToLog(logFile, "Task completed");
    }

    //2.2 Delete sheets of 'deleteSheetsByPosition' list
    if (deleteSheetsByPosition != "")
    {
        writeToLog(logFile, "Deleting sheets using 'deleteSheetsByPosition' property: "+ deleteSheetsByPosition);
        OpenSheetUtils.deleteSheets(deleteSheetsByPosition, openSheetDocument, true);
        writeToLog(logFile, "Task completed");
    }    


    /*
    * 3. Set values to cells
    */
    //3.1 Set values as string to cells with sheet names
    if (insertCellsValues != "")
    {
        writeToLog(logFile, "Inserting cell values using 'insertCellsValues' property: "+ insertCellsValues);
        OpenSheetUtils.insertValuesToCells(insertCellsValues, openSheetDocument, false, false);
        writeToLog(logFile, "Task completed");
    }

    //3.2 Set numeric values to cells with sheet names
    if (insertCellsNumericValues != "")
    {
        writeToLog(logFile, "Inserting cell values using 'insertCellsNumericValues' property: "+ insertCellsNumericValues);
        OpenSheetUtils.insertValuesToCells(insertCellsNumericValues, openSheetDocument, false, true);
        writeToLog(logFile, "Task completed");
    }

    //3.3 Set values as string to cells with sheet position
    if (insertCellsValuesBySheetPosition != "")
    {
        writeToLog(logFile, "Inserting cell values using 'insertCellsValuesBySheetPosition' property: "+ insertCellsValuesBySheetPosition);
        OpenSheetUtils.insertValuesToCells(insertCellsValuesBySheetPosition, openSheetDocument, true, false);
        writeToLog(logFile, "Task completed");
    }

    //3.2 Set numeric values to cells with sheet position
    if (insertCellsNumericValuesBySheetPosition != "")
    {
        writeToLog(logFile, "Inserting cell values using 'insertCellsValues' property: "+ insertCellsNumericValuesBySheetPosition);
        OpenSheetUtils.insertValuesToCells(insertCellsNumericValuesBySheetPosition, openSheetDocument, true, true);
        writeToLog(logFile, "Task completed");
    }

    /*
    * 4. Extract cells values
    */
    //4.1 Extract values from cells with sheet names
    def properties = null;
    if (extractCellsValues != "")
    {
        writeToLog(logFile, "Extracting values from cells using 'extractCellsValues' property: "+ extractCellsValues);
        properties = OpenSheetUtils.extractValuesFromCells(extractCellsValues, openSheetDocument, false);
        writeToLog(logFile, "Task completed");

        if (properties != null)
        {
            writeToLog(logFile, "Writing '"+ extractCellsValuesFile + "' file with previous extracted cell values");
            writeExtractCellValuesFile(extractCellsValuesFile, properties);
            writeToLog(logFile, "Task completed");
        }
    }
    

    //4.2 Extract values from cells with sheet position
    if (extractCellsValuesBySheetPosition != "")
    {
        writeToLog(logFile, "Extracting values from cells using 'extractCellsValuesBySheetPosition' property: "+ extractCellsValuesBySheetPosition);
        properties = OpenSheetUtils.extractValuesFromCells(extractCellsValuesBySheetPosition, openSheetDocument, true);
        writeToLog(logFile, "Task completed");

        if (properties != null)
        {
            writeToLog(logFile, "Writing '"+ extractCellsValuesFile + "' file with previous extracted cell values");
            writeExtractCellValuesFile(extractCellsValuesFile, properties);
            writeToLog(logFile, "Task completed");
        }
    }


    /*
    * 5. save document
    */
    // 5.1 Save Document
    if (saveDocumentTo == "")
    {
        writeToLog(logFile, "Saving document changes");
        openSheetDocument.save();
        writeToLog(logFile, "Task completed");
    }
    else // 5.2 Export Document
    {
        writeToLog(logFile, "Exporting document to '"+saveDocumentTo+"' file");

        if (saveDocumentTo.matches(".+\\.pdf"))
        {
            openSheetDocument.exportToPDF(saveDocumentTo)
        }
        else
        {
            openSheetDocument.saveAs(saveDocumentTo);
        }
        
        writeToLog(logFile, "Task completed");
    }

    openSheetDocument.close();
}

try {
    runScript();
}
catch (Exception ex) {
    writeToLog(new File(logFileName), "ERROR: " + ex.getMessage());
    throw ex;
}


