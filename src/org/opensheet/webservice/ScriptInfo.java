/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice;

/**
 * Clase que contiene toda la informaci&oacute;n necesaria para el uso y
 * ejecuci&oacute;n de un script de Groovy.
 *
 * @author cruiz
 */
public class ScriptInfo{
    private String path;
    private String name;
    private String description;
    private String[] outputVariables;
    private String temporalDirectoryVariable;

    /**
     * Devuelve la descripci&oacute;n general de uso del script.
     * @return String con la descripci&oacute;n de uso.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Permite asignar la descripci&oacute;n general de uso del script.
     * @param description String con la descripci&oacute;n general de uso.
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Permite recuperar el nombre del script.
     * @return String con el nombre del script.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Permite establecer el nombre del script.
     * @param name String con el nombre del script.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Devuelve un array con los nombre de aquellas variables del script que
     * contienen las rutas de los ficheros de salida que son generados por el
     * script.
     * @return String[] con los nombres de las variables del script con las rutas
     * de los ficheros generados por el script.
     */
    public String[] getOutputVariables()
    {
        return outputVariables;
    }

    /**
     * Establece un array con los nombre de las variables que contiene las rutas
     * de los ficheros de salida generados por el script.
     * @param outputVariables String[] con los nombres de variables con las rutas
     * de los ficheros de salida.
     */
    public void setOutputVariables(String[] outputVariables)
    {
        this.outputVariables = outputVariables;
    }

    /**
     * Devuelve la ruta del script.
     * @return String con la ruta del script.
     */
    public String getPath()
    {
        return path;
    }

    /**
     * Permite establecer la ruta del script.
     * @param path String con la ruta del script.
     */
    public void setPath(String path)
    {
        this.path = path;
    }

    /**
     * Devuelve el nombre de la variable donde el script espera tener la ruta
     * con el directorio temporal que debe usar.
     * @return String con el nombre de la variable que el script espera que
     * contenga la ruta temporal.
     */
    public String getTemporalDirectoryVariable()
    {
        return temporalDirectoryVariable;
    }

    /**
     * Asigna el nombre de la variable donde el script espera tener la ruta
     * con el directorio temporal que debe usar.
     * @param temporalDirectoryVar String con el nombre de la variable que el
     * script espera que contenga la ruta temporal.
     */
    public void setTemporalDirectoryVariable(String temporalDirectoryVar)
    {
        this.temporalDirectoryVariable = temporalDirectoryVar;
    }
    
}
