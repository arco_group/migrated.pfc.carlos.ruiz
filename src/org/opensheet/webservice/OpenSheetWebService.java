/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice;

import org.opensheet.webservice.exceptions.OpenSheetOperationInvalidParamsException;
import org.opensheet.webservice.utils.ScriptInfoUtils;
import com.sun.star.comp.helper.BootstrapException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.opensheet.api.OpenSheetManager;
import org.opensheet.webservice.exceptions.OpenSheetWebServiceException;
import org.opensheet.webservice.utils.OperationUtils;

/**
 * Clase principal de OpenSheet Web Service que contiene las operaciones 
 * publicadas para ser usadas desde un cliente del servicio web.
 *
 * @author cruiz
 */
@WebService()
public class OpenSheetWebService {

    public final static String SCRIPT_DIRECTORY_KEYWORD = "OPENSHEET_SCRIPTS_DIRECTORY";
    public final static String CACHE_DIRECTORY_KEYWORD = "OPENSHEET_CACHE_DIRECTORY";
    
    private Map<String, ScriptInfo> scriptsInfo;
    private List<ScriptInfo> scriptsInfoList;
    private String cacheDirPath;
    private OpenSheetManager openSheetManager;
           

    /**
     * Operaci&oacute;n del servicio web que permite ejecutar un script seleccionado
     * de los disponibles (listados a trav&eacute;s de la operaci&oacute;n listScripts)
     * a trav&eacute;s de su nombre e inyectar las propiedades pasadas como una
     * lista de ScriptVariable, devolviendo la lista de documentos generados.
     *
     * @param spreadSheet Document que representa el documento base con el debe
     * trabajar el script, si no es necesario ninguno entonces este debe ser
     * vac&iacute;o, es decir, no debe tener ni nombre ni contenido pero nunca
     * debe ser un valor nulo.
     * @param variables List de ScriptVariable con las variables y los valores a
     * inyectar. Los ficheros o documentos deben referenciarse a trav&eacute;s
     * del nombre y no de la ruta, salvo algun caso especial, para que el script
     * pueda usarlos en su directorio temporal.
     * @scriptName String con el identificador del script que se desea utilizar.
     * Este debe ser el de alguno de los objetos ScriptInfo recibidos a trav&eacute;s
     * de la operaci&oacute;n listScripts del servicio web.
     * @return List de objetos Document de los ficheros generados por el script.
     * @throws OpenSheetWebServiceException
     * @throws OpenSheetOperationInvalidParamsException
     *
     */
    @WebMethod(operationName = "executeScript")
    public List<Document> executeScript(Document spreadSheet, List<ScriptVariable> variables, String scriptName) throws OpenSheetWebServiceException, OpenSheetOperationInvalidParamsException
    {        
        
        ScriptInfo scriptInfo = scriptsInfo.get(scriptName);

        if (scriptInfo == null)
        {
                throw new OpenSheetWebServiceException("'"+scriptName+"' script doesn't exist");
        }        

        ExecuteScriptOperation operation = new ExecuteScriptOperation(openSheetManager, new File(cacheDirPath), scriptInfo,
                spreadSheet, OperationUtils.getVariablesMap(variables));

        return operation.execute();
    }

    @PostConstruct
    private void loadInfo() throws NamingException, BootstrapException
    {
        try
        {
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");            
            String scriptDirectory = (String)envCtx.lookup(SCRIPT_DIRECTORY_KEYWORD);
            cacheDirPath = (String)envCtx.lookup(CACHE_DIRECTORY_KEYWORD);

            scriptsInfo = (Map<String, ScriptInfo>) ScriptInfoUtils.getScriptsInfo(ScriptInfoUtils.getScriptsAndPropertiesFiles(new File(scriptDirectory)));
            scriptsInfoList = new ArrayList<ScriptInfo>(scriptsInfo.values());

            openSheetManager = new OpenSheetManager();
        }
        catch (IOException ex)
        {
            Logger.getLogger(OpenSheetWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @PreDestroy
    private void terminate()
    {
        if (openSheetManager != null)
        {
            openSheetManager.terminate();
        }
    }

    /**
     * Operaci&oacute;n del servicio web que devuelve una lista de ScriptInfo con
     * la informaci&oacute;n de todos los scripts disponibles para usar en la
     * operaci&oacute;n executeScript.
     * @return List de ScriptInfo de todos los scripts disponibles.
     */
    @WebMethod(operationName = "listScripts")
    public List<ScriptInfo> listScripts()
    {                
        return scriptsInfoList;
    }              
}
