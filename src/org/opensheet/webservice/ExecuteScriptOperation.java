/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice;

import groovy.lang.Binding;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.opensheet.api.OpenSheetManager;
import org.opensheet.api.exceptions.LoadOpenSheetDocumentException;
import org.opensheet.api.exceptions.OverwriteOpenSheetDocumentException;
import org.opensheet.api.exceptions.UnsupportedOpenSheetDocumentException;
import org.opensheet.command.ScriptExecuter;
import org.opensheet.command.exceptions.OpenSheetPropertyFormatException;
import org.opensheet.command.exceptions.OpenSheetRunScriptException;
import org.opensheet.webservice.exceptions.OpenSheetOperationInvalidParamsException;
import org.opensheet.webservice.exceptions.OpenSheetWebServiceException;
import org.opensheet.webservice.utils.OperationUtils;

/**
 * Es la clase que representa una ejecuci&oacute;n de la operaci&oacute;n
 * executeScript y por tanto contiene toda la informaci&oacute;n necesaria para
 * llevar a cabo la ejecuci&oacute;n de un script usando como base la clase
 * ScriptExecuter.
 * 
 * @author cruiz
 */
public class ExecuteScriptOperation {

    private OpenSheetManager openSheetManager;
    private File workingDirectory;
    private ScriptInfo scriptInfo;
    private Document inputDocument;
    private File temporalDirectory;
    private Map<String,String> variablesMap;

    /**
     * Constructor que crea un objeto que representa la operaci&oacute;n
     * executeScript con todo lo necesario para ejecutar un script de Groovy.
     * @param openSheetManager OpenSheetManager necesario para crear los objetos
     * de OpenSheetApi que se necesiten inyectar al script.
     * @param workingDirectory File con el directorio de trabajo donde se va a
     * crear el directorio temporal para esta ejecuci&oacute;n.
     * @param scriptInfo ScriptInfo del script de Groovy que se quiere ejecutar.
     * @param inputDocument Document que representa el documento de entrada,
     * puede ser un documento vac&iacute;o pero no puede ser null.
     * @param variablesMap Map con las clave/valor de lo que se desea inyectar
     * al script.
     * @throws OpenSheetOperationInvalidParamsException
     */
    public ExecuteScriptOperation(OpenSheetManager openSheetManager, File workingDirectory, 
            ScriptInfo scriptInfo, Document inputDocument, Map<String, String> variablesMap)
                throws OpenSheetOperationInvalidParamsException
    {
        
        if (openSheetManager == null)
        {
            throw new OpenSheetOperationInvalidParamsException("OpenSheetManager parameter is null.");
        }

        if (workingDirectory == null)
        {
            throw new OpenSheetOperationInvalidParamsException("WorkingDirectory parameter is null.");
        }

        if (!workingDirectory.isDirectory())
        {
            throw new OpenSheetOperationInvalidParamsException("WorkingDirectory doesn't exist or isn't a directory.");
        }

        if (scriptInfo == null)
        {
            throw new OpenSheetOperationInvalidParamsException("ScriptInfo parameter is null.");
        }

        if (inputDocument == null)
        {
            throw new OpenSheetOperationInvalidParamsException("Document parameter is null.");
        }

        if (variablesMap == null)
        {
            throw new OpenSheetOperationInvalidParamsException("Variables Map parameter is null.");
        }        

        this.openSheetManager = openSheetManager;
        this.workingDirectory = workingDirectory;
        this.scriptInfo = scriptInfo;
        this.inputDocument = inputDocument;
        this.variablesMap = variablesMap;
    }

    /**
     * Devuelve el directorio temporal creado para esta ejecuci&oacute;n
     * @return File con el directorio temporal.
     */
    public File getTemporalDirectory()
    {
        return temporalDirectory;
    }

    void setTemporalDirectory(File temporalDirectory)
    {
        this.temporalDirectory = temporalDirectory;
    }
    
    Map<String, String> getVariablesMap()
    {
        return variablesMap;
    }        
    
    void saveDocumentInTemporalDirectory() throws IOException
    {        
        OperationUtils.saveDocument(inputDocument, temporalDirectory);
    }

    void createTemporalDirectory() throws IOException
    {
        this.temporalDirectory = OperationUtils.createTemporalDir(workingDirectory);
    }

    ScriptExecuter createScriptExecuter() throws OpenSheetPropertyFormatException, OverwriteOpenSheetDocumentException,
            UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {
        Properties properties = new Properties();
        properties.putAll(variablesMap);
        return new ScriptExecuter(openSheetManager, properties);
    }

    void parseVariablesMap(){                
        //1. parse @document with temporal Directory
        variablesMap = OperationUtils.addTemporalPathToDocumentKeyword(temporalDirectory, variablesMap);

        //2. Add temp Directory variable with path
        variablesMap.put(scriptInfo.getTemporalDirectoryVariable(), temporalDirectory.getAbsolutePath());
    }

    List<Document> getDocumentsFromOutputs(Binding binding) throws IOException
    {
        List<Document> outputDocuments = new ArrayList<Document>();

        for (String var : scriptInfo.getOutputVariables())
        {
            File document = new File((String) binding.getVariable(var));
            outputDocuments.add(OperationUtils.loadDocument(document));
        }
        return outputDocuments;        
    }
    
    File getScriptFile()
    {
        return new File(scriptInfo.getPath());
    }

    void deleteTemporalDirectory() throws IOException
    {
        OperationUtils.deleteDirectory(temporalDirectory);
    }

    /**
     * Ejecuta la operaci&oacute;n y devuelve una lista con los documentos
     * de salida generados y especificados en el ScriptInfo.
     * @return List de los documentos de salida generados y especificados en el
     * ScriptInfo.
     * @throws OpenSheetWebServiceException
     */
    public List<Document> execute() throws OpenSheetWebServiceException
    {
        List<Document> outputDocuments = null;
        try
        {            
            createTemporalDirectory();

            if (!inputDocument.isEmpty())
            {
                saveDocumentInTemporalDirectory();
            }
            parseVariablesMap();
            ScriptExecuter scriptExecuter = createScriptExecuter();
            scriptExecuter.run(getScriptFile());
            outputDocuments = getDocumentsFromOutputs(scriptExecuter.getBinding());
            deleteTemporalDirectory();
        }
        catch (OpenSheetRunScriptException ex)
        {            
            throw new OpenSheetWebServiceException(ex);            
        }
        catch (OpenSheetPropertyFormatException ex)
        {
            throw new OpenSheetWebServiceException(ex);
        }
        catch (OverwriteOpenSheetDocumentException ex)
        {
            throw new OpenSheetWebServiceException(ex);
        }
        catch (UnsupportedOpenSheetDocumentException ex)
        {
            throw new OpenSheetWebServiceException(ex);
        }
        catch (LoadOpenSheetDocumentException ex)
        {
            throw new OpenSheetWebServiceException(ex);
        }
        catch (IOException ex)
        {
            throw new OpenSheetWebServiceException(ex);
        }

        return outputDocuments;
    }

}
