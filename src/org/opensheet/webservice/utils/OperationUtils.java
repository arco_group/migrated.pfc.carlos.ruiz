/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.opensheet.command.ScriptExecuter;
import org.opensheet.webservice.Document;
import org.opensheet.webservice.ScriptVariable;

/**
 * Clase con m&eacute;todos de utilidades para las operaciones de OpenSheet Web
 * Service.
 * 
 * @author cruiz
 */
public class OperationUtils {

    /**
     * Crea un directorio temporal con un nombre &uacute;nico dentro un directorio
     * dado.
     * @param parentDir File con el directorio dentro del cual se quiere crear
     * el directorio temporal.
     * @return File con el directorio temporal creado.
     * @throws IOException
     */
    public static File createTemporalDir(File parentDir) throws IOException
    {
        int randomSuffix = (int) (Math.random() * 1000);
        String dirName = "ws"+System.currentTimeMillis()+randomSuffix;

        File tempDir = new File(parentDir, dirName);
        
        if (!tempDir.mkdir())
            throw new IOException("'"+tempDir.getAbsolutePath()+"' temporal directory couldn't be created.");

        return tempDir;
    }

    /**
     * Guarda dentro del directorio indicado el fichero que representa el objeto
     * Document pasado como argumento.
     * @param document Document que contiene el fichero que se desea almacenar.
     * @param directory File con el directorio donde se desea guardar el fichero
     * del objeto Document.
     * @throws IOException
     */
    public static void saveDocument(Document document, File directory) throws IOException
    {
        FileOutputStream fos = null;
        try
        {
            File documentFile = new File(directory, document.getName());
            fos = new FileOutputStream(documentFile);
            fos.write(document.getContent());            
        }        
        finally
        {
            fos.close();
        }
    }

    /**
     * Carga un fichero y crea un objeto Document que lo representa.
     * @param documentFile File con la ruta al fichero que se desea cargar como
     * un objeto Document. A este objeto se asigna el contenido binario del fichero
     * para que se quede cargado en memoria.
     * @return Document que contiene el nombre y los datos del fichero indicado
     * como argumento.
     * @throws IOException
     */
    public static Document loadDocument(File documentFile) throws IOException
    {
        FileInputStream fin = null;
        Document document = null;

        try
        {
            fin = new FileInputStream(documentFile);
            byte[] fileContent = new byte[(int) documentFile.length()];
            fin.read(fileContent);
            document = new Document(documentFile.getName(), fileContent);            
        }        
        finally
        {
            fin.close();
        }

        return document;
    }

    /**
     * Parsea los valores de las claves pasadas en un mapa asign&aacute;ndoles
     * la ruta del directorio pasado como argumento cuando estos son los valores
     * especiales usados para crear o abrir OpenSheetDocument.
     * @param tempDir File con el directorio temporal que se va a usar para
     * parsear el nombre de los ficheros a crear o abrir a través de las variables
     * especiales del ScriptExecuter. De esta manera se abrir&aacute;n o
     * crear&aacute;n con el nombre original pero en el directorio temporal indicado.
     * @param variables Map con las claves y los valores de las propiedades a
     * parsear. S&oacute;lo se parsear&aacute;n aquellos valores de claves que
     * usen las variables especiales para crear o abrir documentos con ScriptExecuter.
     * @return Map con las variables parseadas.
     */
    public static Map<String, String> addTemporalPathToDocumentKeyword(File tempDir, Map<String, String> variables)
    {
        Map<String, String> parsedVariables = new HashMap<String, String>();

        String createPrefix = ScriptExecuter.OPENSHEET_DOCUMENT_PROPERTY_NAME + ScriptExecuter.OPENSHEET_DOCUMENT_PROPERTY_CREATE_PARAM
                    + ScriptExecuter.OPENSHEET_DOCUMENT_PROPERTY_SEPARATOR;
        String openPrefix = ScriptExecuter.OPENSHEET_DOCUMENT_PROPERTY_NAME + ScriptExecuter.OPENSHEET_DOCUMENT_PROPERTY_OPEN_PARAM
                    + ScriptExecuter.OPENSHEET_DOCUMENT_PROPERTY_SEPARATOR;

        for (String key: variables.keySet())
        {
            String value = variables.get(key);                        

            if (value.startsWith(createPrefix))
            {
                String documentName = value.substring(createPrefix.length());
                value = createPrefix + tempDir.getAbsolutePath() + File.separator + documentName;
            }
            else if (value.startsWith(openPrefix)){
                String documentName = value.substring(openPrefix.length());
                value = openPrefix + tempDir.getAbsolutePath() + File.separator + documentName;
            }
            parsedVariables.put(key, value);            
        }

        return parsedVariables;
    }

    /**
     * Borra un directorio y todo su contenido.
     * @param directory File que contiene el directorio que se desea borrar.
     * @throws IOException
     */
    public static void deleteDirectory(File directory) throws IOException
    {
        File[] files = directory.listFiles();

        for (File file : files)
        {
            if (file.isDirectory())
            {
                deleteDirectory(file);
            }
            else
            {
                if (!file.delete())
                    throw new IOException("'"+ file.getName() +"' file couldn't be deleted");
            }
        }
        
        if (!directory.delete())
            throw new IOException("'"+ directory.getName() +"' directory couldn't be deleted");
    }

    /**
     * Transforma una lista de ScriptVariable en un Map de cadenas.
     * @param variables List de objetos ScriptVariable que se quiere transformar
     * en un Map de clave/valor.
     * @return Devuelve un Map de clave/valor equivalente a la lista pasada.
     */
    public static Map<String, String> getVariablesMap(List<ScriptVariable> variables)
    {
        Map<String, String> variablesMap = new HashMap<String, String>();

        if (variables == null)
            return null;
        
        for (ScriptVariable variable : variables)
        {
            variablesMap.put(variable.getName(), variable.getValue());
        }

        return variablesMap;
    }
}
