/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.webservice.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.opensheet.webservice.ScriptInfo;

/**
 * Clase con m&eacute;todos de utilidades para trabajar con objetos ScriptInfo
 * de OpenSheet Web Service.
 * 
 * @author cruiz
 */
public class ScriptInfoUtils
{

    private static final String SCRIPT_EXT = ".groovy";
    private static final String SCRIPT_INFO_EXT = ".properties";
    public static final String SCRIPT_DESCRIPTION_PROP = "description";
    public static final String SCRIPT_OUTPUT_VARIABLES_PROP = "outputVariables";
    public static final String SCRIPT_OUTPUT_VARIABLES_SEPARATOR = ";";
    public static final String SCRIPT_TEMPORAL_DIR_VARIABLE_PROP = "temporalDirectoryVariable";

    /**
     * Devuelve un mapa con cada uno de los ficheros de un directorio que
     * son un script de Groovy con extensi&oacute;n .groovy, que tambi&eacute;n
     * tienen un fichero con el mismo nombre pero con extensi&oacute;n .properties.
     * El valor clave del Map es el fichero de script, y el valor el fichero de
     * datos.
     * @param scriptsDirectory File con el directorio de donde se quiere obtener
     * un Map con los scripts que tienen a su vez un fichero de datos dentro de
     * ese directorio.     
     * @return Map con los fichero de script de Groovy como claves y los ficheros
     * de datos como valores.
     */
    public static Map<File, File> getScriptsAndPropertiesFiles(File scriptsDirectory)
    {
        Map<File, File> scripts = new HashMap<File, File>();
        File[] dirFiles = scriptsDirectory.listFiles();

        List<File> listFiles = getScriptAndPropertyFiles(dirFiles);

        for (File file : listFiles)
        {
            if (file.getName().endsWith(SCRIPT_EXT))
            {
                String propName = file.getName().substring(0, file.getName().length()
                        - SCRIPT_EXT.length()).concat(SCRIPT_INFO_EXT);
                boolean found = false;
                for (int i = 0; (i < listFiles.size()) && !found; i++)
                {
                    if (listFiles.get(i).getName().equals(propName))
                    {
                        scripts.put(file, listFiles.get(i));
                        found = true;
                    }
                }
            }
        }

        return scripts;
    }

    private static List<File> getScriptAndPropertyFiles(File[] fileList)
    {
        ArrayList<File> files = new ArrayList();

        if (fileList == null)
        {
            return files;
        }

        for (File file : fileList)
        {
            if (!file.isDirectory()
                    && (file.getName().contains(SCRIPT_EXT) || file.getName().contains(SCRIPT_INFO_EXT)))
            {
                files.add(file);
            }
        }

        return files;
    }

    /**
     * A partir de un fichero script y las propiedades del script crea un objeto
     * ScriptInfo con esa informaci&oacute;n.
     * @param script File que representa el fichero de Groovy.
     * @param ScriptProperties Properties con las propiedades con informaci&oacute;n
     * para usar el fichero de Groovy.
     * @return ScriptInfo con toda la informaci&oacute;n extra&iacute;da de las
     * propiedades y del fichero script.
     */
    public static ScriptInfo getScriptInfo(File script, Properties ScriptProperties)
    {
        ScriptInfo scriptInfo = new ScriptInfo();

        scriptInfo.setName(script.getName());
        scriptInfo.setPath(script.getPath());

        scriptInfo.setDescription(ScriptProperties.getProperty(SCRIPT_DESCRIPTION_PROP));
        scriptInfo.setTemporalDirectoryVariable(ScriptProperties.getProperty(SCRIPT_TEMPORAL_DIR_VARIABLE_PROP));

        if (ScriptProperties.containsKey(SCRIPT_OUTPUT_VARIABLES_PROP))
        {
            scriptInfo.setOutputVariables(ScriptProperties.getProperty(SCRIPT_OUTPUT_VARIABLES_PROP).split(SCRIPT_OUTPUT_VARIABLES_SEPARATOR));
        }

        return scriptInfo;
    }

    /**
     * Crea un Map usando como clave el nombre del script y como valor un objeto
     * ScriptInfo generado.
     * @param scriptsMap Map que tiene como clave los ficheros de script y como
     * valor el fichero de propiedades asociado.
     * @return Map que tiene como clave el nombre del script y como valor un
     * objeto ScriptInfo con toda la informaci&oacute;n de uso del script.
     * @throws IOException
     */
    public static Map<String, ScriptInfo> getScriptsInfo(Map<File, File> scriptsMap) throws IOException
    {
        Map<String, ScriptInfo> scripts = new HashMap<String, ScriptInfo>();

        for (File file : scriptsMap.keySet())
        {
            Properties properties = new Properties();
            properties.load(new FileInputStream(scriptsMap.get(file)));

            scripts.put(file.getName(), getScriptInfo(file, properties));
        }

        return scripts;
    }
}
