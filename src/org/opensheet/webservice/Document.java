/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice;

/**
 * Clase que representa un documento o fichero, almacenando el nombre
 * del fichero y su contenido binario.
 *
 * @author cruiz
 */
public class Document {
    private String name;
    private byte[] content;

    public Document()
    {        
    }

    /**
     * Constructor que ya asigna el nombre y los datos del documento o fichero.
     * @param name String con el nombre del documento o fichero.
     * @param data byte[] es el contenido binario del documento o fichero.
     */
    public Document(String name, byte[] data)
    {
        this.name = name;
        this.content = data;
    }

    /**
     * Devuelve el contenido binario del documento o fichero.
     * @return byte[] con el contenido binario.
     */
    public byte[] getContent()
    {
        return content;
    }

    /**
     * Devuelve el nombre del documento o fichero.
     * @return String con el nombre del documento o fichero.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Asigna el contenido binario del documento o fichero.
     * @param data byte[] con el contenido binario del documento o fichero.
     */
    public void setContent(byte[] data)
    {
        this.content = data;
    }

    /**
     * Asigna el nombre al documento o fichero.
     * @param name String con el nombre del documento o fichero.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Indica si el documento o fichero es un documento vac&iacute;o. Un
     * documento o fichero es vac&iacute;o cuando su nombre y su contenido son
     * nulos.
     * @return true si el documento es vac&iacute;o, false en caso contrario.
     */
    public boolean isEmpty()
    {
        if ( (name == null) && (content == null) )
            return true;
        else
            return false;
    }
}
