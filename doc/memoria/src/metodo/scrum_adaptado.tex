% -*- coding: utf-8 -*-
\subsection{Adaptación de Scrum al marco de trabajo}
En esta sección se va a explicar como se ha adaptado la metodología Scrum para
poder usarse en la gestión de un proyecto fin de carrera.

Para la definición del proceso de Scrum se ha partido de las recomendaciones y
experiencias transmitidas por Henrik Kniberg en su
libro~\cite{scrum_desde_trincheras}, y una adaptación posterior debido a las
características propias del entorno donde se debían aplicar. Las
características a tener en cuenta en el proyecto son:
\begin{itemize}
  \item Normalmente al aplicar Scrum se habla de un equipo de desarrollo de
  varias personas y en este caso sólo existen dos personas para abordar el
  proyecto: el alumno que debe realizar el proyecto fin de carrera, y el
  director que tiene la labor de orientar durante el desarrollo del mismo.
  \item En este caso concreto, el alumno no se puede dedicar a jornada
  completa al desarrollo del proyecto. El motivo es que el alumno trabaja a
  tiempo completo en otros proyectos, lo que hace que el número de horas
  diarias a dedicar sean muy limitadas, aumentando la dificultad de la gestión
  y planificación del proyecto.
\end{itemize}

\subsubsection{Roles asignados}
Cuando se habla de Scrum se habla de equipos de desarrollo y en el caso de un
proyecto fin de carrera sólo hay un miembro de equipo, el alumno que lo va a
realizar, y por otro lado está el director de proyecto cuyo objetivo es
orientar durante el desarrollo del proyecto. Siguiendo el funcionamiento de
Scrum es necesario que los principales roles del proceso sean asignados, pero
en este caso teniendo en cuenta el número tan reducido de personas con las que
se cuenta para que la asignación de los roles encajen lo mejor posible. Los
roles se van a asignar de la siguiente forma:
\begin{itemize}
  \item El dueño del producto será el director del proyecto pues tiene la
  capacidad de identificar las prioridades que deben cumplirse para
  lograr tener un producto con la calidad suficiente para que sea aceptado por
  el cliente. En este caso el cliente es el tribunal que debe evaluar el
  proyecto, y el producto es el proyecto fin de carrera que siga la normativa
  académica establecida.
  \item El rol {\it Scrum master} será llevado a cabo por el alumno pues uno
  de los objetivos del proyecto fin de carrera debe ser demostrar las aptitudes
  para la planificación y gestión de proyectos.
  \item El último rol por asignar es el de miembro de equipo que debe ser
  llevado a cabo por el alumno, donde tendrá el papel de desarrollador.
\end{itemize}

Por tanto el director de proyecto asumirá la responsabilidad de establecer la
prioridad de las tareas siempre en comunicación con el {\it Scrum master} y será
quién evalúe las entregas o demostraciones realizadas al finalizar cada sprint.

El alumno a parte de llevar el trabajo de desarrollo deberá gestionar el
proyecto, realizando la estimación de las historias. Además la planificación de
los sprints se deberán realizar con la aprobación del dueño de producto.

\subsubsection{Identificación de historias: pila de producto}
La pila de producto, siguiendo en parte las indicaciones del
libro anteriormente mencionado~\cite{scrum_desde_trincheras}, se realizará de
manera que a cada historia se le van a asignar los siguientes campos:

\begin{itemize}
  \item {\bf ID}: Es el identificador único de la historia que permite hacer el
  seguimiento durante todo el desarrollo permitiendo así que cambie de nombre
  en algún momento si se considera adecuado. El identificador usado será un
  numero entero para las historias, y para las subhistorias un número decimal
  donde la parte entera será el identificador de la historia padre. Así si una
  hay dos subhistorias que parten de la historia con id 3, entonces esas
  subhistorias tendrán como ids 3.1 y 3.2 respectivamente.
  \item {\bf Nombre}: Es nombre descriptivo de la funcionalidad que representa,
  debe ser entendida por el dueño de producto.
  \item {\bf Descripción}: Es una pequeña descripción con detalles concretos
  donde se explica en qué consiste la historia.
  \item {\bf Estimación}: Es la estimación inicial del tiempo que va a tardar
  en realizar la historia. La estimación se da en puntos historia
  que se explicarán más adelante.
  \item {\bf Cómo probarlo}: Es una descripción de los test que se deben
  realizar con éxito para entender que la historia ha sido completada. Esto como
  se verá más adelante se podrá utilizar como punto de entrada en el desarrollo
  dirigido por tests.
  \item {\bf Notas}: Son detalles a tener en cuenta a la hora planificar la
  historia en algún sprint. En estos detalles se pueden incluir por
  ejemplo las dependencias entre historias, de manera que si el dueño de producto
  quiere alguna tarea que dependa de otra, ambas deben ser planificadas en el
  sprint o esperar a que la dependencia esté terminada.
  \item {\bf Importancia}: Es valor numérico asignado por el dueño de
  producto y que prioriza la pila de producto. No existe un valor máximo,
  simplemente cuanto mayor sea el número más importante es la historia. Es
  recomendable no usar valores secuenciales a la hora de asignar las
  prioridades para permitir, en caso de necesitarse, que las historias nuevas
  puedan situarse entre medias de historias ya existentes.
\end{itemize}

Aunque en otros proyectos normalmente no se planifican todas las historias en
sprints directamente, sino que se planifican aquellas que corresponden a un
sprint, es decir, se hace una planificación de sprint en sprint; en este
proyecto al tener una fecha límite para su finalización, es necesario realizar
la planificación completa para conocer así el tiempo que se necesita para
completarlo. Para ello se debe realizar la asignación de todas las tareas en
sprints y comprobar así si con esa planificación inicial se dispone del tiempo
suficiente.


\subsubsection{División en Sprints}
En esta adaptación de Scrum se ha asignado a cada sprint una duración de una
semana, de manera que al final de cada semana se haga una reunión de
sprint o se envíe un correo al dueño del producto donde se muestren las
historias completadas. El objetivo es analizar la evolución del proyecto y de
los productos semanalmente; de esta manera se permite detectar las posibles
desviaciones de tiempo muy rápidamente, puesto que en este caso concreto el
alumno no puede tener dedicación completa en el proyecto y eso provoca que toda
desviación pueda desembocar en un retraso excesivo. Así en el caso de detectar
una desviación una semana, se usará como medida correctora, siempre que sea
posible, el aumento el número de horas a dedicar en el siguiente sprint; una
forma de aumentar el número de horas puede ser mediante el uso de
días de vacaciones del trabajo principal.

Para poder realizar la división en sprints primero hay que establecer la
velocidad de desarrollo del equipo de Scrum, y cómo se miden los puntos por
historia. En este proyecto se ha decidido que 4 puntos de historia
corresponde a una semana de desarrollo dedicando 27 horas de trabajo que son las
horas que el miembro del equipo puede dedicar sin hacer una sobre-estimación, por
tanto, la velocidad de equipo se establecerá en 4 puntos.

\subsubsection{Herramienta para planificación}
Para poder llevar la planificación se usará un documento de hojas
de cálculo con tres hojas donde se guardará toda la información necesaria para
realizar la planificación siguiendo la adaptación de Scrum:
\begin{itemize}
  \item {\bf Semanas}: en esta hoja se muestra la información de todas las
  semanas disponibles hasta la fecha límite de entrega del proyecto, y en cada
  una de ellas toda la información resumen de la planificación. Esta tabla tiene
  los siguientes elementos:
	\begin{itemize}
  	\item {\bf Semana}: indica el número de semana.
  	\item {\bf Comienzo}: indica que día del mes comienza la semana de trabajo.
  	\item {\bf Fin}: indica que día del mes en el que finaliza esa semana de
  	trabajo.
  	\item {\bf Objetivo}: es el nombre del objetivo que persigue el sprint
  	planificado esa semana.
  	\item {\bf Historias}: contiene los identificadores de las historias
  	planificadas para esa semana.
  	\item {\bf Velocidad estimada}: es la velocidad que se ha estimado que tendrá
  	el equipo.
  	\item {\bf Velocidad real}: es la velocidad final que el equipo ha tenido en
  	esa semana.
  	\item {\bf Completado Fecha}: contiene la fecha real en la que se han
  	terminado todas las historias del sprint.
  	\end{itemize}
  \item {\bf Historias}: en esta hoja se muestra la información en detalle de
  todas las historias y subhistorias con todos los elementos que contiene una
  historia excepto el campo que indica la importancia. Esta hoja sirve
  para poder conocer los detalles concretos de cada historia de manera que tiene
  como principales usuarios los roles de {\it Scrum master} y miembro de
  equipo, puesto que será la hoja que usen para estimar y para conocer que se
  debe implementar en cada historia.
  \item {\bf Product Backlog}: es la hoja que representa la pila de producto, y
  por tanto la que debe usar el dueño de producto para priorizar las historias.
  Esta hoja contiene una tabla con los siguientes elementos:
  \begin{itemize}
  	\item {\bf ID}: indica el identificador de la historia.
  	\item {\bf Nombre}: indica el nombre de la historia para que el dueño de
  	producto pueda identificarla mejor.
  	\item {\bf Importancia}: es el valor numérico que le debe asignar el dueño de
  	producto, cuanto mayor sea el valor más importancia tendrá. Las historias más
  	importante están más arriba de la pila siendo las primeras en implementarse.
  	\item {\bf Estimación}: son los puntos de historia estimados que indican la
  	duración de esa historia. Hay que tenerlos en cuenta a la hora de poder
  	asignar historias a un sprint.
  	\item {\bf Sprint}: Identificador del sprint al que se ha asignado la
  	historia.
  	\item {\bf Velocidad estimada}: es la velocidad que se ha estimado que tendrá
  	el equipo en ese sprint.
  	\item {\bf Velocidad real}: es la velocidad final que el equipo ha tenido en
  	en ese sprint.
  	\item {\bf Estado}: indica si la historia ha sido completada, es decir, se ha
  	probado que cumple con lo establecido en el campo de {\bf cómo probarlo}.
  	\end{itemize}
\end{itemize}

Las tres tablas anteriores en un documento de hojas de cálculo,
junto con las opciones que proporciona OpenOffice.org Calc como son la de reordenar
por el valor de un campo, añadir nuevas historias, mover elementos, etc; es
suficiente para llevar la información de planificación y gestión del proyecto.

% Local Variables:
% coding: utf-8
% mode: latex
% TeX-master: "main"
% mode: flyspell
% ispell-local-dictionary: "castellano8"
% End:
