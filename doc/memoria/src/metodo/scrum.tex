% -*- coding: utf-8 -*-
\subsection{Scrum}
En esta sección no se pretende dar una explicación extensa y profunda de Scrum,
para eso se cuentan con libros y referencias en la web, si no que el objetivo
perseguido es dar una visión global de Scrum y sus principales conceptos para
entender en otros capítulos cómo se ha adaptado su uso en el proyecto, y
comprender mejor el capítulo de resultados.

\subsubsection{¿Por qué usar Scrum?}
El escenario de negocio ha cambiado de manera que las necesidades tecnológicas
cambian rápidamente y aparecen otras nuevas, por lo que es necesario que los
proyectos cuenten con una gran velocidad a la hora de reaccionar a esos cambios
para dar una respuesta rápida que se traduzca en los productos reclamados por
el mercado.

Algunos autores~\cite{flexibilidad_con_scrum} comentan que puede que estemos en
una etapa en la que ha dejado de existir el concepto de producto final, y en su
lugar existen productos en continua evolución a partir de los incrementos de
funcionalidad de las versiones iniciales o versiones beta de un producto. De
esta manera, el producto va cambiando e incorporando los nuevos requisitos para
ofrecer cada poco tiempo nuevas versiones.

Para permitir llevar a cabo proyectos cuyos productos se adapten de
manera rápida a las necesidades del mercado, existe una metodología ágil para
la gestión de proyectos llamada Scrum.

\subsubsection{¿Qué es Scrum?}
Scrum es una metodología ágil que define un marco de trabajo iterativo e
incremental para la gestión de proyectos. Esta metodología gestiona proyectos
basándose en un modelo ágil, cuyo objetivo es permitir adaptarse rápidamente a
cambios en los requisitos en lugar de intentar identificar todos los requisitos
necesarios al comienzo del proyecto.

Scrum ofrece un conjunto de prácticas y roles que pueden usarse como modelo de
referencia para definir el proceso de desarrollo concreto que se ejecutará en
los proyectos. A continuación se definen estos roles y conceptos.

\subsubsection{Roles}
En Scrum se pueden identificar los siguientes roles implicados directamente en
el proceso:

\begin{itemize}
  \item {\bf Dueño de poducto} ({\it product owner}): es el responsable de
  representar y asegurarse que se cumple con la perspectiva de negocio y los intereses del
  cliente. Las tareas que realiza son:
  \begin{itemize}
    \item Añadir nuevos requisitos o historias de usuario en la pila de
    producto ({\it product backlog}).
    \item Priorizar las historias de usuario.
    \item Comprobar al finalizar un período de desarrollo (\emph{sprint}) que el
      producto resultante cumple con los objetivos comprometidos.
  \end{itemize}
  \item {\bf Scrum master}: es el responsable de que el equipo cumpla sus
  objetivos al finalizar el sprint y de que se cumplan las reglas de Scrum. Las
  tareas que realiza son:
  \begin{itemize}
    \item Convoca y dirige las reuniones para realizar las estimaciones de
    historias y división en tareas. Tanto las estimaciones como la división en
    tareas las realiza conjuntamente todo el equipo.
  	\item Dirige las reuniones diarias de seguimiento llamadas scrum.
  	\item Asigna a los miembros del equipo aquellas tareas que no han sido
  	seleccionadas de manera voluntaria.
  \end{itemize}
  \item {\bf Miembro de equipo} ({\it Scrum member}): es el encargado de cumplir
  con las tareas elegidas o asignadas para que el equipo logre alcanzar el
  objetivo marcado en el sprint. En Scrum los equipos son multidisciplinares por
  lo que un miembro de equipo puede ser un desarrollador, diseñador, etc.
\end{itemize}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=1\textwidth]{../figures/roles.png}
\caption{Roles de Scrum}
\label{fig:roles}
\end{center}
\end{figure}

En la figura~\ref{fig:roles} se pueden ver los tres roles principales con el
elemento del que es responsable cada uno: el dueño de producto es el responsable
de la gestión de la pila de producto, el Scrum master es el responsable de la
gestión de los sprints, y el miembro del equipo es responsable de implementar
las historias y tareas asignadas.

A parte de los roles anteriores, directamente implicados en el proceso de Scrum,
existen otros roles muy importantes que participan a través de la realimentación o
{\it feedback} a partir del análisis o uso de los productos generados en los
sprints. Algunos de estos roles son: clientes, usuarios, gerentes, comerciales,
etc.

\subsubsection{Pila de Producto}
La pila de producto ({\it product backlog}) es una lista priorizada de todos los
requisitos o historias de usuario que se desean implementar en el producto.
Estas historias de usuario, que es como se llaman a los requisitos en Scrum,
son definidas usando la terminología del dueño de producto de modo que pueda
trabajar con ellas sin problemas.

La pila de producto es algo muy dinámico, donde se añaden historias
conforme se detectan nuevas necesidades, y donde la priorización va cambiando
para que al principio de la pila estén siempre las historias que se quieren
implementar primero. El mantenimiento y priorización de la pila es llevado a
cabo por el dueño de producto.

\subsubsection{Sprint}
Un sprint es el periodo de tiempo o iteración con un objetivo concreto, durante
el cuál se implementan aquellas historias planificadas dando como resultado un
incremento funcional del producto. Normalmente la duración de los sprints no
suele exceder los dos meses de desarrollo para cumplir con los principios del
manifiesto ágil.

Un proyecto implementa las historias de la pila de producto a través de su
planificación en sprints. El número de historias que se pueden planificar para
que se implementen en cada sprint depende por un lado de la estimación en tiempo
de cada historia y por otro de la velocidad del equipo.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=1\textwidth]{../figures/scrum.png}
\caption{Representación de un sprint de Scrum}
\label{fig:scrum}
\end{center}
\end{figure}

En la figura~\ref{fig:scrum} se puede ver ilustrado la representación de un
sprint, donde primero se seleccionan en orden las historias que están en
la pila de producto y se asignan al sprint que se va a comenzar, después se
inicia el sprint y una vez ha transcurrido el tiempo fijado para su
desarrollo se obtiene como resultado el producto con las nuevas funcionalidades
implementadas.

Antes del comienzo de un sprint se realiza una reunión del equipo con el dueño
de producto para decidir que historias se van a llevar a cabo en ese sprint,
para ello las historias deben estar estimadas, pues se irán añadiendo al sprint
en el orden establecido en la pila, hasta alcanzar la cantidad máxima de trabajo
que puede realizar el equipo, medida dada por la velocidad de equipo.

Durante el desarrollo del sprint se utiliza un diagrama llamado {\it burn-down},
ver figura~\ref{fig:ejemplo_burndown} en el que se representa el estado de la
implementación de las historias. Con este gráfico se puede comprobar de manera
visual si el sprint sigue con la planificación establecida, o se está
produciendo alguna desviación positiva o negativa, de manera que se pueda
corregir rápidamente.

Al final del sprint se analiza el resultado del mismo y se hace un análisis de
todo los sucedido como por ejemplo si se ha cumplido completamente con la
planificación,los factores que han puesto en peligro el funcionamiento del
mismo, qué cosas mejorar o cambiar, etc.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.7\textwidth]{../figures/ejemplo_burndown.png}
\caption{Ejemplo de gráfico de burndown de un sprint}
\label{fig:ejemplo_burndown}
\end{center}
\end{figure}

\subsubsection{Scrum}
Scrum es una reunión diaria, de no más de quince minutos, que realiza todo el
equipo y donde se analiza el estado de las tareas. Esta reunión, que da nombre a
la metodología, es una potente herramienta de comunicación para todo el equipo,
puesto que permite conocer que está haciendo cada miembro, detectar los
problemas encontrados y además analizar la situación global del sprint.

% Local Variables:
% coding: utf-8
% mode: latex
% TeX-master: "main"
% mode: flyspell
% ispell-local-dictionary: "castellano8"
% End:
