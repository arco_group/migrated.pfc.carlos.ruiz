% -*- coding: utf-8 -*-

\subsection{Microsoft Office y COM}
Existe una alternativa que permite utilizar la funcionalidad de las
aplicaciones de Microsoft Office e incorporarla en otras aplicaciones, se llama
Automation y en este apartado se va explicar en qué consiste.

\subsubsection{Automation}
Segun Microsoft~\cite{office_automation} Automation (antes conocido como
\acs{OLE} Automation) es una tecnología que permite usar la funcionalidad de
otros programas existentes e incorporarla a nuestras aplicaciones.

Para poder utilizar las funcionalidades de otro programa se hace uso de
\acs{COM} que es la arquitectura software de componentes de Microsoft, que
permite la comunicación entre procesos y la creación de objetos dinámicos. El
programa que quiere ofrecer sus funcionalidades para ser usadas con \acs{COM}
debe de generarse teniendo en cuenta la especificación de dicha arquitectura y
hacer públicas las interfaces que va a permitir usar.

Microsoft Excel es una aplicación que implementa \acs{COM} como
servicio de manera que publica sus interfaces para permitir que otras
aplicaciones hagan uso de sus funcionalidades, es través del uso de Automation
como se consigue que aplicaciones que no implementen \acs{COM} puedan acceder a
los servicios de dicha arquitectura.

\subsubsection{Formas de usar un componente COM}
Existen principalmente 3 formas de utilizar los servicios de un componente \acs{COM}
desde Visual C++:
\begin{enumerate}
  \item Usar \acs{MFC}.
  \item Usar la directiva \#import.
  \item Usar C/C++ Automation.
\end{enumerate}

La primera opción, hace uso de \acs{MFC} para generar un envoltorio o
{\it wrapper} de clases de la aplicación cliente (la que va a usar los servicios
de \acs{COM}) a partir de la librería de tipos de la aplicación Office a
utilizar. De esta manera las clases de la aplicación cliente pasan a ser
también clases de \acs{MFC} y por tanto facilita su uso con Automation. Para
poder generar el envoltorio o {\it wrapper} mencionado se hace uso de una
utilidad que proporciona Visual C++ (disponible a partir de su versión 5.0).

La segunda opción, consiste en usar la directiva \#import. Al
incluir dicha directiva se crean puntos de entrada a la librería que se desea
utilizar, proporcionando una opción sencilla y potente. El inconveniente es
que la propia Microsoft~\cite{office_automation} desaconseja su uso por problemas
existentes con el control del número de referencias en aplicaciones como
Microsoft Office.

La tercera y última opción, consiste en usar directamente la \acs{API} de
\acs{COM} con C/C++, haciendo un uso a más bajo nivel de Automation del que
se realiza a través de \acs{MFC}, lo que da mayor versatilidad y
evita el tener que realizar envoltorios o {\it wrappers} de la clase, pero
que a su vez tiene el inconveniente de ser más complejo su uso.

\subsubsection{Ejemplos de operaciones con hojas de cálculo en C/C++}
A continuación, se muestran algunos fragmentos de código con la idea de ilustrar
la dificultad de uso de esta alternativa a través de la tercera de las opciones
mostradas en el apartado anterior, es decir, haciendo uso de C++
Automation. Para que el código sea más sencillo,
Microsoft~\cite{excel_c_plus_plus} en sus ejemplos hace uso de una función
llamada AutoWrap que facilita el uso de Automation.

\begin{listing}[
  float,
  language = C++,
  caption  = {Cómo cargar la aplicación Excel para ser usada través de COM},
  label    = code:load_excel]
// 1) Buscamos el identificador del programa Excel para COM
CLSID clsid;
HRESULT hr = CLSIDFromProgID(L"Excel.Application", &clsid);
if(FAILED(hr)) {
	::MessageBox(NULL, "CLSIDFromProgID() failed", "Error", 0x10010);
	return -1;
}

// 2) Arrancamos el servico Excel a traves de su indetificador
IDispatch *pXlApp;
hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IDispatch, (void **)&pXlApp);
if(FAILED(hr)) {
	::MessageBox(NULL, "Excel not registered properly", "Error", 0x10010);
	return -2;
}
\end{listing}

\begin{listing}[
  float,
  language = C++,
  caption  = {Cómo hacer que la carga del servicio COM sea en segundo plano},
  label    = code:invisible_excel]
{
	VARIANT x;
	x.vt = VT_I4;
	x.lVal = 0; //Con 0 sera invisible (segundo plano) y con 1 se hara visible
	AutoWrap(DISPATCH_PROPERTYPUT, NULL, pXlApp, L"Visible", 1, x);
}
\end{listing}

Como se puede ver en el listado~\ref{code:load_excel} la carga del programa
Excel como servicio \acs{COM} para usarlo en otra aplicación es
medianamente sencilla. Si se quiere usar dicha aplicación en segundo plano
evitando que sea visible para el usuario, basta con asignar el valor cero a la
propiedad de visibilidad como se puede ver en el listado~\ref{code:invisible_excel}.

En \cite{excel_c_plus_plus} se puede ver un ejemplo completo de uso donde se
crea un nuevo documento de hoja de cálculo, se añade una nueva hoja de cálculo,
se selecciona la hoja activa, se selecciona un rango y a cada celda del mismo
le asigna un valor distinto, por último salva el documento y lo cierra.

\begin{listing}[
  float,
  language = C++,
  caption  = {Abrir un fichero Excel a través de COM},
  label    = code:open_excel]
//El objeto donde se carga el documento es un puntero a IDispatch.
{
	VARIANT result, fileName;
	fileName.vt = VT_BSTR;
	fileName.bstrVal = ::SysAllocString(OLESTR("c:\\fichero_prueba.xls"));
	VariantInit(&result);
	AutoWrap(DISPATCH_PROPERTYGET, &result, pXlBooks, L"Open", 1, fileName);
	pXlBook = result.pdispVal; //IDispatch *pXlBook;
}
\end{listing}

\begin{listing}[
  float,
  language = C++,
  caption  = {Seleccionar una hoja de cálculo a través de su nombre usando
  COM}, label    = code:select_sheet_by_name]
//El objeto donde se carga la hoja de calculo es un puntero a IDispatch.
{
	VARIANT result, parm;
	parm.vt = VT_BSTR;
	parm.bstrVal = ::SysAllocString(L"Hoja1");
	VariantInit(&result);
	AutoWrap(DISPATCH_PROPERTYGET, &result, pXlBook, L"Sheets", 1, parm);
	pXlSheet = result.pdispVal; //IDispatch *pXlSheet;
}
\end{listing}

Otras operaciones como la de abrir un documento con formato Excel
(listado~\ref{code:open_excel}) u obtener una determinada hoja del documento a
partir de su nombre (listado~\ref{code:select_sheet_by_name}) también son
operaciones medianamente sencillas una vez se conoce el mecanismo de su uso. El
principal problema es conocer el nombre y cómo usar determinadas operaciones,
siendo en muchas ocasiones necesario crear una macro en Excel con la operación
que se quiere realizar para poder entender como funciona dicha operación y poder
usarla en \acs{COM}.

\subsubsection{Características de la alternativa}
Una vez introducida la alternativa, se van a analizar las diferentes
características comentadas en la introducción del capítulo.

Esta alternativa permite trabajar con cualquier formato que soporte Microsoft
Excel, permitiendo además realizar cualquier operación que se pueda realizar
con dicha aplicación. En realidad se soportarán todas aquellas versiones y
formatos válidos para la versión de Microsoft Excel usada como servicio.

La necesidad de uso de la arquitectura \acs{COM} impide su uso en otras
plataformas distintas de Windows.
Los lenguajes que se pueden
usar para la aplicación, son todos aquellos que permitan el uso de \acs{COM}.

La licencia de la aplicación que implemente esta alternativa puede ser
del tipo que se desee, pero al necesitar hacer uso del programa Microsoft Excel
siempre será necesario contar con una licencia del mismo.

La dificultad de su uso es alta, aunque a primera vista viendo los
ejemplos del apartado anterior pueda parecer un uso algo más sencillo, este se
complica a la hora de buscar cómo realizar las diferentes operaciones, pues no
existe un índice de todas las operaciones disponible {\it online}, y por
tanto a veces es necesario buscar en ejemplos más complejos para localizar un uso concreto del
mismo y la mayoría de las veces la única posibilidad es realizar una macro en la
aplicación Microsoft Excel para analizar el código generado por la misma y así
usarlo de guía para conocer cómo realizar la operación.

Esta alternativa al hacer uso de la tecnología de componentes de Windows y
existir multitud de aplicaciones que hacen uso de ella tiene como resultado una
alternativa con una madurez y actividad muy alta. Además, el soporte también es
muy alto.

% Local Variables:
% coding: utf-8
% mode: latex
% TeX-master: "main"
% mode: flyspell
% ispell-local-dictionary: "castellano8"
% End:
