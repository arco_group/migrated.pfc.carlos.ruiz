% -*- coding: utf-8 -*-

\subsection{OpenOffice.org y UNO}
Existe una alternativa que permite utilizar la funcionalidad de las
aplicaciones de OpenOffice.org e incorporarla en otras aplicaciones, al igual
que ocurre con \acs{COM} y Microsoft Office como se ha visto en el apartado
anterior, esta se llama \acs{UNO} y en este apartado se va explicar en qué
consiste.

\subsubsection{UNO Universal Network Objects}
\acs{UNO}~\cite{ooo_devel_guide} es la base de la tecnología de componentes de
OpenOffice.org que permite utilizar objetos entre diferentes plataformas y
lenguajes a través de la definición de una serie de interfaces que deben de
ser implementadas.

\acs{UNO} tiene soporte para la siguientes plataformas: GNU/Linux, Solaris, Windows,
Power~PC, FreeBSD, MacOS. Y también permite usarse con los lenguajes: Java, C++,
OpenOffice.org Basic, .Net, Python.

La suite de ofimática OpenOffice.org implementa a su vez las interfaces de \acs{UNO}
necesarias para permitir incorporar las funcionalidades de sus aplicaciones en
otras aplicaciones.

\subsubsection{Formas de conectar a OpenOffice.org con UNO}
Existen principalmente 2 formas de utilizar los servicios de OpenOffice.org a
través de \acs{UNO}:
\begin{enumerate}
  \item Usar una comunicación local a través de una tubería o {\it pipe}.
  \item Usar una comunicación remota a través de sockets.
\end{enumerate}

\begin{listing}[
  float,
  language = bash,
  caption  = {Arracar OpenOffice.org en modo servicio escuchando en un socket},
  label    = code:soffice_socket]
soffice -headless -accept="socket,port=8100;urp;"
\end{listing}

La primera opción requiere que el ejecutable soffice sea arrancado con unas
opciones determinadas para que se inicie en modo servicio escuchando en un
puerto. En el listado~\ref{code:soffice_socket} se muestra como
arrancar el proceso de OpenOffice.org como servicio para que se quede a
la escucha de peticiones por socket en el puerto 8100.
Esto permite poder usar toda la funcionalidad de OpenOffice.org Calc, y del
resto de aplicaciones de la suite ofimática, sin necesidad de tener instalado el programa en la misma máquina donde se va a usar,
pues proporciona una comunicación remota a través de un puente o {\it bridge} de
\acs{UNO}.

\begin{listing}[
  float,
  language = bash,
  caption  = {Arracar OpenOffice.org en modo servicio escuchando en una tubería
  o {\it pipe}},
  label    = code:soffice_pipe]
soffice -headless -accept=pipe,name=uno2;urp;"
\end{listing}

La segunda opción requiere que el ejecutable soffice sea arrancando con la
opciones de servicio escuchando en una tubería o {\it pipe} con un
determinado identificador. Esto permite la comunicación con otros procesos en local a
través de \acs{UNO} para hacer uso de la funcionalidad de OpenOffice.org. En el
listado~\ref{code:soffice_pipe} se muestra como arrancar el proceso
OpenOffice.org como servicio para que escuche en una tubería o {\it pipe} de
proceso identificada con el nombre <<uno2>>.

\begin{listing}[
  float,
  language = Java,
  caption  = {Cadena de conexión para conectar con un servicio a través de
  socket},
  label    = code:url_socket]
String sConnect =
"uno:socket,host=localhost,port=8100;urp;StarOffice.ComponentContext";
\end{listing}

\begin{listing}[
  float,
  language = Java,
  caption  = {Cadena de conexión para conectar con un servicio a través de
  una tubería o {\it pipe}},
  label    = code:url_pipe]
String sConnect = "uno:pipe,name=uno2;urp;StarOffice.ComponentContext";
\end{listing}

Una vez se ha arrancado el servicio de OpenOffice.org para conectar con él
desde otra aplicación, en ambas opciones, es necesario resolver la
cadena de conexión o \acs{URL} correspondiente en cada caso
(listado~\ref{code:url_socket} y listado~\ref{code:url_pipe}) a través del
objeto XUnoUrlResolver, que devolverá el objeto XComponentContext del servicio
remoto a través del cuál se pueden obtener el resto de objetos necesarios para
trabajar con OpenOffice.org Calc.

\subsubsection{Método bootstrap}
En el \acs{SDK} de OpenOffice.org se proporciona la clase Bootstrap con
un método estático llamado bootstrap que permite en una sola llamada ejecutar
el servicio de OpenOffice.org y recuperar el objeto XComponentContext. Esto
facilita mucho la conexión con el servicio, pues proporciona directamente el
objeto necesario para trabajar con OpenOffice.org Calc ocultando todos los pasos
necesarios para establecer la comunicación. Este método por
dentro realiza los siguientes pasos:
\begin{enumerate}
  \item Localiza el directorio donde se encuentra el ejecutable soffice (según
  el sistema donde se ejecuta sea GNU/Linux o Windows usa distintos mecanismos).
  \item Ejecuta el fichero soffice con los parámetros necesarios para que actúe
  como servicio y escuche en una tubería o {\it pipe}.
  \item Conecta con el servicio a través de la tubería abierta.
  \item Recupera el XComponentContext y lo devuelve.
\end{enumerate}
 Este método sólo puede ser utilizado cuando la instalación de OpenOffice.org
 está en la misma máquina que la clase que va a usar el método bootstrap.
\subsubsection{Ejemplo de operaciones con hojas de cálculo usando UNO}
En este apartado se va a explicar un ejemplo completo de método que hace uso
de Calc para crear un nuevo documento de hojas de cálculo al que le añade una
nueva hoja de cálculo y asigna valores a dos celdas, para finalmente guardar el
documento con formato xls de Excel. El objetivo de este ejemplo es el de
ilustrar el nivel de complejidad de uso de la alternativa.

\begin{listing}[
  float,
  language = Java,
  caption  = {Ejemplo uso UNO parte 1: Cómo obtener el objeto
  remoto XComponentLoader},
  label    = code:uno_example_part1]
XComponentContext xContext = Bootstrap.bootstrap();
XMultiComponentFactory xServiceManger = xContext.getServiceManager();
Object desktop =
xServiceManger.createInstanceWithContext("com.sun.star.frame.Desktop",xContext);

XComponentLoader xComponentLoader =
(XComponentLoader) UnoRuntime.queryInterface(XComponentLoader.class,desktop);
\end{listing}

El primer paso (listado~\ref{code:uno_example_part1}) es obtener el objeto
remoto XComponentLoader, para ello se utiliza el método bootstrap comentado en
el apartado anterior, y mediante el XComponentContext recuperado se crear una
instancia remota del tipo com.sun.star.frame.Desktop que permite hacer un
casting a la clase del objeto esperado. Los castings en el entorno \acs{UNO} se
realizan a través de una consulta al runtime que comprueba primero que el
objeto puede realizar el casting a una determinada clase y además que el enlace
al objeto remoto sigue activo.

\begin{listing}[
  float,
  language = Java,
  caption  = {Ejemplo uso UNO parte 2: Cómo crear un nuevo documento de hojas
  de cálculo},
  label    = code:uno_example_part2]

PropertyValue[] loadProps = new PropertyValue[1];
loadProps[0] = new PropertyValue();
loadProps[0].Name = "Hidden";
loadProps[0].Value = new Boolean(true);

XComponent xSpreadsheetComponent =
xComponentLoader.loadComponentFromURL("private:factory/scalc", "_blank", 0, loadProps);

XSpreadsheetDocument xSpreadsheetDocument =
	(XSpreadsheetDocument) UnoRuntime.queryInterface(
	XSpreadsheetDocument.class, xSpreadsheetComponent);
\end{listing}

El segundo paso (listado~\ref{code:uno_example_part2}) es cargar un nuevo
documento de hojas de cálculo haciendo uso del objeto remoto XComponentLoader,
para ello se le pasa una lista de propiedades con la propiedad que hace que se
realice la operación de creación en segundo plano, y por tanto de manera
transparente al usuario. Como resultado de la creación obtenemos un objeto
XSpreadsheetDocument.

\begin{listing}[
  float,
  language = Java,
  caption  = {Ejemplo uso UNO parte 3: Añadir una nueva hoja de cálculo al
  documento},
  label    = code:uno_example_part3]
XSpreadsheets xSpreadsheets = xSpreadsheetDocument.getSheets();
xSpreadsheets.insertNewByName("Hoja_prueba", (short) 0);
Object sheet = xSpreadsheets.getByName("Hoja_prueba");
XSpreadsheet xSpreadsheet =
(XSpreadsheet) UnoRuntime.queryInterface(XSpreadsheet.class, sheet);
\end{listing}

El tercer paso (listado~\ref{code:uno_example_part3}) consiste en añadir una
nueva hoja de cálculo y para ello es necesario recuperar el objeto
XSpreadsheets que representa la lista de hojas de cálculo del documento. Puesto
que el método que añade hojas de cálculo no devuelve nada, para poder operar
después con la hoja creada, es necesario recuperarla antes a través de su
nombre. Al final como resultado obtenemos un objeto XSpreadsheet.

\begin{listing}[
  float,
  language = Java,
  caption  = {Ejemplo uso UNO parte 4: Insertar valores a celdas},
  label    = code:uno_example_part4]
XCell xCell = xSpreadsheet.getCellByPosition(0, 0); //celda A1
xCell.setValue(16.6);

xCell = xSpreadsheet.getCellByPosition(1, 1); //celda B2
xCell.setValue(199);
\end{listing}

El siguiente paso (listado~\ref{code:uno_example_part4}) consiste en insertar
valores a dos celdas del objeto XSpreadsheet recuperado en el paso anterior.
Para ello es necesario primero obtener el objeto XCell, que se recupera a
través de la posición que ocupa en la hoja de cálculo, y después usar el método
que permite asignarle un valor.

\begin{listing}[
  float,
  language = Java,
  caption  = {Ejemplo uso UNO parte 5: Guardar un documento con formato xls},
  label    = code:uno_example_part5]
XStorable xStorable =
(XStorable)UnoRuntime.queryInterface(XStorable.class, xSpreadsheetComponent);

PropertyValue[] storeProps = new PropertyValue[1];
storeProps[0] = new PropertyValue();
storeProps[0].Name = "FilterName";
storeProps[0].Value = "MS Excel 97";
File outputFile = new File("salida.xls");
xStorable.storeAsURL("file:///"+outputFile.getAbsolutePath(), storeProps);
\end{listing}

El último paso (listado~\ref{code:uno_example_part5}) consiste en salvar el
documento y las modificaciones realizadas en él en un fichero con formato de
Microsoft Excel concretamente el formato xls. Para ello es necesario obtener a
partir del XComponent, que representa el documento cargado en memoria, un objeto
XStorable que permite realizar el salvado a fichero. Como se desea guardar el
documento con un formato distinto al nativo de la aplicación OpenOffice.org
Calc, es necesario definir una lista de propiedades con un filtro que indique al
método de salvado en qué formato se desea guardar el documento.

\subsubsection{Características de la alternativa}
Una vez introducida la alternativa, se van a analizar las diferentes
características comentadas en la introducción del capítulo.

Esta alternativa permite trabajar con cualquier formato que soporte
OpenOffice.org Calc que incluye los formatos de Microsoft Excel, permitiendo
realizar cualquier operación permitida en dicha aplicación, incluyendo la
conversión entre los formatos permitidos y la exportación a \acs{PDF}.

Como ya se comentó en apartados anteriores \acs{UNO} permite el
uso de diferentes lenguajes y plataformas.

La licencia de la aplicación que implemente esta alternativa puede ser
libre y seguir haciendo uso de OpenOffice.org sin necesidad de tener ningún tipo
de licencia.

La dificultad de su uso de esta alternativa es alta debido a la compleja
jerarquía de objetos que deriva en una gran cantidad de interfaces que siempre debe ser
recuperadas en tiempo de ejecución lo que obliga a conocer dicha estructura
previamente.

La documentación para usar \acs{UNO} es completa y existen gran
cantidad de ejemplos, sobre todo para el desarrollo en Java.

Esta alternativa hace uso de la tecnología de componentes diseñada para usarse
directamente con la suite ofimática OpenOffice.org y forma parte de ella, eso
unido al uso extendido de esta suite desde hace varios años y a la salida
con frecuencia de versiones da como resultado una alternativa con
una madurez y actividad muy alta. Además, el soporte ofrecido tanto por
la comunidad como por los desarrolladores también es muy alto.

% Local Variables:
% coding: utf-8
% mode: latex
% TeX-master: "main"
% mode: flyspell
% ispell-local-dictionary: "castellano8"
% End:
