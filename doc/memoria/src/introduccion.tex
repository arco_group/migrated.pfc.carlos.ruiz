% -*- coding: utf-8 -*-

\chapter{Introducción}

\drop{L}as aplicaciones de hojas de cálculo son programas que permiten la
creación y manipulación de documentos compuestos por una cuadrícula con múltiples celdas
ordenadas por fila y columna, y que pueden contener datos numéricos,
alfanuméricos o fórmulas. Las fórmulas permiten indicar cómo el contenido de
una celda debe ser calculado a partir de otra celda o grupo de celdas.

Las hojas de cálculo son consideradas por algunos autores como un lenguaje de
programación funcional~\cite{spreadsheet_programming}, donde el usuario de
manera visual y directa genera programas de cálculo sin necesidad de
conocimientos previos de programación.  Gracias a su facilidad de uso, a la gran
cantidad de funciones que proporcionan y a la intercomunicación con otras
aplicaciones, las hojas de cálculo ofrecen una gran versatilidad que ha hecho
que su uso sea muy extendido y variado en campos muy diversos. Las hojas de
cálculo son usadas como herramienta en las
ingenierías~\cite{engineering_spreadsheet, analytical_chemistry,
  geometric_constructions} debido a su gran potencia para realizar cálculos y a
la facilidad para automatizarlos a través de fórmulas; y sobre todo son usadas
por muchas empresas y organizaciones para realizar cálculos financieros, guardar
informes, gráficos estadísticos, clasificación de datos, etc. Convirtiéndose
estos documentos en una fuente importante de datos.

Es debido al papel tan importante que juegan las hojas de cálculo, en las
empresas y organizaciones, que  un simple error en una hoja de cálculo puede
ocasionar grandes pérdidas~\cite{spreadsheet_horror_stories}. Por ello se han
realizado diferentes estudios que han dado como resultado, la identificación de
riesgos para cada tipo de organización~\cite{spreadsheet_risk}, las diferencias
de uso entre usuarios con diferente grado de experiencia~\cite{spreadsheet_users_experience},
y lo más importante un conjunto de buenas
prácticas~\cite{spreadsheet_best_practice} en el diseño y modelado de hojas de
cálculo proponiendo un ciclo de vida que se adapta a las diferentes necesidades
según el tipo de modelo.

En el mercado existen diferentes alternativas en lo que a aplicaciones de hojas
de cálculo se refiere, y cada una de ellas con su propio formato, pero la más
utilizada y extendida es Microsoft Excel~\cite{winners_losers}, con su formato
xls~\cite{format_xls}~\cite{format_xlsb}, que se proporciona dentro de la suite de
aplicaciones de oficina Microsoft Office, y que se impuso en los 90 a la suite
Lotus 1-2-3 de IBM y a su formato~\cite{format_lotus}.

Como alternativa libre, destaca la suite OpenOffice.org~\cite{openoffice_org},
que incluye la aplicación de hojas de cálculo Calc. Esta suite ha ido ganando
cada vez más mercado al permitir la utilización de otros formatos como los de
Microsoft Office, y al utilizar como formato nativo Open Document
Format~\cite{open_document}~\cite{open_document2}, formato estándar de la
organización OASIS~\cite{oasis}, para garantizar que los documentos generados no
queden atados a una única aplicación.

Según lo descrito, son muchas las organizaciones que  cuentan con un gran número
de hojas de cálculo que contienen a su vez gran cantidad de datos, por lo que
sería deseable el poder contar con una herramienta que permita la extracción e
inserción de datos en hojas de cálculo de manera automática. Permitiendo así
utilizar en otras aplicaciones los datos almacenados en hojas de cálculo, a
través de la extracción de datos; y viceversa, utilizar los datos disponibles
en otras aplicaciones en las hojas de cálculo a través de la inserción de datos.

Además, a pesar de los estudios mencionados anteriormente sobre buenas prácticas
para el diseño de hojas de cálculo, sigue habiendo un mal uso a la hora de
modelar hojas de cálculo~\cite{mba_spreadsheet_users}, y muchas otras hojas de
cálculo que ya se encuentran en uso, siendo inviable volverlas a modelar usando
el conjunto de buenas prácticas propuestas; por lo tanto la estrategia en este
caso debe ir encaminada a detectar los posibles errores para eliminarlos, de
manera que dichas hojas de cálculo sean correctas y estén libres de errores.
Para ello se deben realizar pruebas con el objetivo de depurar las hojas de
cálculo~\cite{spreadsheet_test_driven} y así poder garantizar un mínimo de
calidad, para evitar riesgos y pérdidas como se comentó anteriormente. Esta
tarea de pruebas se facilitaría mucho si se contara con la herramienta
mencionada anteriormente, puesto que permitiría automatizar las pruebas, usando
un conjunto de datos de entrada que insertaría de manera automática, y
extrayendo los datos obtenidos para que otro programa los comparara con los
datos esperados.

Aunque las aplicaciones como Microsoft Excel permiten la automatización de
algunas tareas a través del uso de macros escritas en lenguajes de tipo script
como Visual Basic~\cite{excel_2010_vba}, sería deseable una herramienta que
permita trabajar con los principales formatos de hojas de cálculo, y no sólo
con el formato nativo de la aplicación donde se ejecuta la macro; además, sería
deseable que permitiera de manera sencilla automatizar diferentes tareas sin
necesidad de utilizar una interfaz gráfica.

Por todas las necesidades anteriores se plantea el diseño de OpenSheet, un
proyecto cuyo objetivo principal es la extracción e inserción de datos en
hojas de cálculo de manera automática y sencilla, soportando los
principales formatos de los productos Microsoft Excel y OpenOffice.org Calc.

\section{Estructura del documento}

El presente documento se encuentra dividido en los diferentes capítulos
establecidos por la normativa académica para proyectos fin de
carrera~\cite{normativa_academica}. El autor recomienda leer cada uno de los
capítulos de manera secuencial para una mejor comprensión.

Los capítulos que puede encontrar el lector en el documento son los siguientes:
\begin{description}
\item[Capítulo 2: Antecedentes] En este capítulo primero se definen los
principales conceptos básicos relacionados con los documentos de hojas de cálculo,
necesarios para poder comprender el proyecto. Y después, se explican las
alternativas, para trabajar con hojas de cálculo, analizadas en un estudio
comparativo.
\item[Capítulo 3: Objetivos del proyecto] En este capítulo se explica de manera
detallada qué se quiere lograr con el proyecto. Para ello se describen todos los
objetivos que se persiguen, tanto el objetivo general como los objetivos específicos.
\item[Capítulo 4: Método y entorno de trabajo] En este capítulo se explican los
principales conceptos de las metodologías aplicadas al desarrollo del
proyecto, y también se describe las herramientas o aplicaciones software que han
formado parte del entorno de trabajo.
\item[Capítulo 5: Resultados] En este capítulo se explica cómo se ha llevado a
cabo las metodologías elegidas, descritas en el capítulo 4, para lograr
alcanzar los objetivos marcados al comienzo del proyecto.
\item[Capítulo 6: Conclusiones] En este capítulo se presentan
diferentes contenidos relacionados con las conclusiones del proyecto. Por un
lado se describe cómo se han logrado cubrir los objetivos marcados al comienzo del
proyecto. Por otro lado se describen diferentes propuestas que pueden ser
llevadas a cabo en futuros desarrollos, y que han quedado fuera del alcance del
proyecto. Y por último, se incluye una sección donde el autor comenta sus
conclusiones personales acerca de la experiencia adquirida durante el
desarrollo del proyecto.
\end{description}


% Local Variables:
% coding: utf-8
% mode: latex
% TeX-master: "main"
% mode: flyspell
% ispell-local-dictionary: "castellano8"
% End:
