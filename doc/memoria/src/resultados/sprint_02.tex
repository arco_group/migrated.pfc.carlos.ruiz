% -*- coding: utf-8 -*-

\section{Sprint 2: Apertura de documentos y modificación de hojas}
En este sprint las historias de producto planificadas son:
\begin{itemize}
  \item Historia 1.2: {\bf Abrir documentos}
  \item Historia 1.3: {\bf Modificar hojas de cálculo de un documento}   
\end{itemize}
Ambas historias forman parte de la historia 1, al igual que el sprint anterior,
y por tanto como resultado de este sprint se obtendrá la librería para
manipulación de documentos de hojas de cálculo pero con funcionalidad
reducida. La funcionalidad de la librería resultante será la del sprint anterior
más la proporcionada por cada una de las historias planificadas. La información
resumen de las historias planificadas se puede ver en la tabla~\ref{tab:historias_sprint2}.

\begin{table}[htbp]
  \centering
  \input{tables/sprint_02_table.tex}
  \caption[Historias añadidas al Sprint 2] 
  {Historias añadidas al Sprint 2}
  \label{tab:historias_sprint2}
\end{table}

En este caso se las dos tareas asignadas suman 4 puntos de historia a realizar
en el sprint, que es lo que se ha considerado una carga normal.

A partir de las historias planificadas se define el objetivo del sprint. El
objetivo perseguido por este sprint es el de proporcionar a la \acs{API} o
librería del sprint anterior la funcionalidad necesaria para abrir documentos
de hojas de cálculo, y realizar modificaciones en la hojas de cálculo de los
documentos.

A continuación, se va a detallar el desarrollo de cada una de las historias
planificadas en este sprint.

\subsection{Historia 1.2 - Abrir documentos}
Para empezar a crear los test que sirven para realizar la implementación de la
historia, se ha utilizado como base el campo de la historia {\bf cómo probarlo}
donde se especifican las pruebas que se deben cumplir para determinar que la
historia se ha implementado correctamente: 
\begin{enumerate}
  \item Abrir un documento docExample.ods, comprobar tanto el número como el
  nombre de sus pestañas u hojas de cálculo.
  \item Repetir la prueba 1 con los formatos soportados.
  \item Abrir un documento docUnknow.ods que no existe, debe fallar y devolver un error.
  \item Abrir un documento con una extensión no permitida, debe fallar y
  devolver un error.
  \item Recuperar una pestaña a partir de su posición y comprobar su nombre.
  \item Recuperar una pestaña a partir de su nombre y comprobar su nombre.
  \item Recuperar una pestaña a partir de una posición que no existe, debe
  fallar y devolver un error.
  \item Recuperar una pestaña a partir de un nombre que no existe, debe fallar y
  devolver un error.     
\end{enumerate} 

Las pruebas realizadas basadas en el campo {\bf cómo probarlo} han sido
las siguientes:
\begin{itemize}
  \item Prueba unitarias de apertura de los distintos tipos de ficheros
  permitidos. A partir de la refactorización en las propias pruebas se ha
  creado un método {\it openNewDocument} que es el que realmente realiza las
  comprobaciones dejando en el resto la ruta del fichero a abrir; de esta
  manera se evita la repetición de código innecesaria.
  \item Pruebas unitarias para provocar excepciones en la apertura de manera que
  estás se incluyan en el código. Estas excepciones son cuando no se soporta la
  extensión del fichero o cuando no existe un documento con ese nombre.
  \item Pruebas unitarias para la recuperación de hojas de cálculo a partir de
  una posición o un nombre.
  \item Pruebas unitarias para provocar excepciones en la recuperación de hojas
  de cálculo al usar posiciones inválidas o al usar nombres nulos o inválidos. 
\end{itemize}

A partir de las pruebas anteriores han emergido los siguientes elementos de
diseño:
\begin{itemize}  
  \item {\bf ISpreadSheet}: es la interfaz que proporciona el acceso a las
  funcionalidades de una hojas de cálculo. En este punto sólo se
  proporciona el método para {\it getName()} para obtener el nombre.
  \item {\bf SpreadSheet}: es la clase que representa una hoja de
  cálculo y que por tanto hace uso de los objetos necesarios de UNO y
  OpenOffice.org, implementando además la interfaz ISpreadSheet.
  \item {\bf LoadOpenSheetDocumentException}: excepción que se produce
  cuando se intenta abrir un nuevo documento y no se puede por algún error.
  \item {\bf SpreadSheetNoFoundException}: excepción que se produce
  cuando se intenta recuperar una hoja de cálculo de un documento y no existe.
\end{itemize}

Además de los elementos de diseño nuevos, se han añadido nuevos métodos a las
clases existentes:
\begin{itemize}
  \item {\it getSpreadSheetsNames()}: es el método de IOpenSheetDocument que
  permite recuperar los nombres de todas sus hojas de cálculo en un array de
  cadenas.
  \item {\it getSpreadSheet(int pos)}: es el método de IOpenSheetDocument que
  permite recuperar una hoja de cálculo u objeto ISpreadSheet a partir de su
  posición.
  \item {\it getSpreadSheet(String spreadSheetName)}: es el método de
  IOpenSheetDocument que permite recuperar una hoja de cálculo u objeto
  ISpreadSheet a partir de su nombre.
  \item {\it openDocument(String documentPath)}: es el método de
  OpenSheetManager que se encarga de abrir un documento de hojas
  de cálculo y devolver el correspondiente objeto IOpenSheetDocument.
\end{itemize}

Estos elementos creados a partir de las pruebas y la refactorización cumplen con
lo especificado para determinar que la historia está completa, para ello basta
con ejecutar las pruebas usadas en el algoritmo de \acs{TDD} para su
implementación.

\subsection{Historia 1.3 - Modificar hojas de cálculo de un documento}
Para empezar a crear los test que sirven para realizar la implementación de la
historia, se ha utilizado como base el campo de la historia {\bf cómo probarlo}
donde se especifican las pruebas que se deben cumplir para determinar que la
historia se ha implementado correctamente: 
\begin{enumerate}
  \item Crear un nuevo documento y añadirle dos nuevas hojas de cálculo, una de
  ellas sin asignarle un nombre.
  \item Repetir la prueba 1 con un documento ya existente.
  \item Crear un nuevo documento y borrar dos pestañas, una por nombre y la
  otra por posición.
  \item Abrir un documento y borrar una pestaña por nombre y otra por posición.
  \item Crear un nuevo documento y renombrar una pestaña.
  \item Abrir un documento y renombrar una pestaña.
  \item Repetir las pruebas 1-6 para cada uno de los formatos permitidos.        
\end{enumerate} 

Las pruebas realizadas basadas en el campo {\bf cómo probarlo} han sido
las siguientes:
\begin{itemize}
  \item Prueba unitarias de renombrado de hojas de cálculo para los diferentes
  tipos de documentos permitidos. A partir de la refactorización en las propias
  pruebas se ha creado un método {\it renameSheet} que es el que realmente realiza las
  comprobaciones del renombrado; de esta manera se evita la repetición de
  código innecesaria.
  \item Pruebas unitarias para provocar excepciones en el renombrado de manera
  que estás se incluyan en el código. Para esto también se ha refactorizado los
  test obteniéndose el método {\it renameSheetFail}.
  \item Prueba unitarias para añadir y borrar hojas de cálculo a un
  documento, probando para los diferentes tipos de documentos permitidos. A
  partir de la refactorización en las propias pruebas se han creado los métodos
  {\it addSheetToNewDocument} y {\it deleteSheet} que son los que
  realmente realizan las comprobaciones evitando la repetición de código.
  \item Pruebas unitarias para provocar excepciones al añadir nuevas hojas de
  cálculo de manera que estás se incluyan en el código. Para esto también se ha
  refactorizado los test obteniéndose el método {\it addSheetToNewDocumentMustFail}.     
  \item Pruebas unitarias para provocar excepciones al eliminar hojas de
  cálculo que no existen.
\end{itemize}

A partir de las pruebas anteriores, se han añadido nuevos métodos a las
clases existentes:
\begin{itemize}
  \item {\it rename()}: es el método de ISpreadSheet que
  permite cambiar el nombre de la hoja de cálculo.
  \item {\it addSpreadSheet()}: es el método de IOpenSheetDocument que
  permite añadir una nueva hoja de cálculo con un nombre por defecto al
  documento.
  \item {\it addSpreadSheet(String spreadSheetName)}: es el método de
  IOpenSheetDocument que permite añadir una nueva hoja de cálculo al documento
  con el nombre dado.
  \item {\it deleteSpreadSheet(int pos)}: es el método de IOpenSheetDocument que
  permite borrar una hoja de cálculo del documento a partir de su posición.
  \item {\it deleteSpreadSheet(String spreadSheetName)}: es el método de
  IOpenSheetDocument que permite borrar una hoja de cálculo del documento a
  partir de su nombre.  
\end{itemize}

Estos elementos creados a partir de las pruebas y la refactorización cumplen con
lo especificado para determinar que la historia está completa, para ello basta
con ejecutar las pruebas usadas en el algoritmo de \acs{TDD} para su
implementación.

\subsection{Resumen de sprint}
En el sprint se han completado las dos historias planificadas antes del plazo
establecido. En el gráfico de burndown, ver la
figura~\ref{fig:burndown_sprint2}, se puede ver la evolución del mismo.
\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.6\textwidth]{../figures/sprint2/burndown.png}
\caption{Gráfico de burndown del sprint 2}
\label{fig:burndown_sprint2}
\end{center}
\end{figure}

En el gráfico se puede observar el avance más rápido durante los primeros
días que hace que antes de lo esperado se termine con las tareas planificadas,
esto es debido a que al partir ya de métodos de pruebas y algunas de las clases
de dominio, se ha podido aumentar la velocidad. Después de este terminar con las
historias de este sprint se ha empezado a trabajar con la siguiente historia de
la pila de producto la 1.4 insertar valores en celdas.

En este sprint como resultado se ha obtenido la segunda versión de la
librería que contiene la funcionalidad de abrir documentos de hojas de cálculo
ya creados y modificar las hojas de cálculo de un documento. En la
figura~\ref{fig:class_diagram_sprint2} se puede ver el diseño de clases
resultante, donde han aparecido nuevas clases y en algunos casos para las
existentes se han añadido nuevos métodos, no se han añadido al diagrama las
clases que representan excepciones para aumentar la legibilidad.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=1\textwidth]{../figures/sprint2/class_diagram.png}
\caption{Diagrama de clases resultante del sprint 2}
\label{fig:class_diagram_sprint2}
\end{center}
\end{figure}


% Local Variables:
% coding: utf-8
% mode: latex
% TeX-master: "main"
% mode: flyspell
% ispell-local-dictionary: "castellano8"
% End: