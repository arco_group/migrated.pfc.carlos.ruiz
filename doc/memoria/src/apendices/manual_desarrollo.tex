% -*- coding: utf-8 -*-

\chapter{Manual de desarrollo}

Este manual está orientado tanto para aquellos desarrolladores que quieren hacer
uso de la \acs{API} de OpenSheet en sus aplicaciones como para los que quieren
ampliar la funcionalidad de OpenSheet Command o Web Service a través de la
creación de nuevos scripts.

\section{Direcciones de interés}
\subsection{Repositorio}
El proyecto OpenSheet cuenta con un repositorio donde siempre estará el último
código disponible para ser descargado por cualquier persona que así lo desee. 
La URL del repositorio es la siguiente: \url{https://arco.esi.uclm.es/svn/public/prj/opensheet/}
En el directorio raíz del repositorio del proyecto se encuentran tres
directorios:
\begin{itemize}
  \item {\bf trunk}: Aquí es donde se puede encontrar el código principal que
  será usado para sacar las versiones a distribuir de los diferentes componentes
  del proyecto. Si se desea tener el código más actual y que vaya a formar parte de
  la siguiente versión, se puede coger el código del trunk.
  \item {\bf branches}: Aquí es donde se generan diferentes ramas del proyecto
  para desarrollar en paralelo funcionalidades que en principio no se
  desean incorporar directamente al proyecto, y de hecho puede que no se
  lleguen a incorporar a ninguna versión del producto. Si alguno de los
  desarrollos que se alojan en este directorio se van a usar, entonces se
  realizará la operación de {\it merge} con el trunk, y por tanto, siempre se
  recomienda tomar el código del trunk, dejando el código de este directorio a
  los desarrolladores del proyecto.
  \item {\bf tags}: En este directorio por cada versión sacada se realiza una
  instantánea del repositorio y se crea un directorio con el nombre de la
  versión. Este es el directorio donde se pueden obtener el código de alguna de
  las versiones distribuidas.
\end{itemize}  

Dentro del directorio trunk la distribución del proyecto es la siguiente:
\begin{itemize}
  \item {\bf doc}: Es el directorio que contiene la documentación del proyecto.
  En este directorio se encuentra el documento de hojas de cálculo usado para la
  planificación, el anteproyecto y los ficheros de latex que corresponden a la
  memoria del proyecto, en los que se incluyen los manuales de usuario y de
  desarrollo.
  \item {\bf src}: Es el directorio donde se encuentra el código fuente del
  proyecto dividido en los diferentes paquetes Java. Dentro del directorio
  src/org/opensheet/ se encuentra a su vez un directorio para los componentes
  del proyecto y para los scripts:
	\begin{itemize}
	  \item {\bf api}: Contiene el código referente a la \acs{API} de OpenSheet.
	  \item {\bf command}: Contiene el código referente a OpenSheet Command y la
	  clase para ejecutar scripts que también es compartida por el servicio web.
	  \item {\bf webservice}: contiene el código de OpenSheet Web Service, excepto
	  la clase de ejecución de scripts que se encuentra en el paquete del comando.
	  \item {\bf scripts}: contiene los scripts escritos en Groovy que se
	  distribuyen con las versiones del comando y los scripts que sirven de test de
	  estos. Actualmente sólo se ha generado un script: OpenSheetScript.groovy.	  
  	\end{itemize}
  \item {\bf test}: Contiene el código de todos los tests del proyecto, y la
  estructura es la misma que la del directorio src, salvo porque no tiene el
  directorio scripts y en su lugar tiene un directorio {\it utils} para las
  clases de utilidades de las pruebas.
\end{itemize}

\subsection{Gestión de incidencias y peticiones de mejora}
El proyecto cuenta con una herramienta de seguimiento de errores o bugs, y de
peticiones de mejora para que los usuarios puedan abrir incidencias de los
problemas encontrados y solicitar mejoras. Esta herramienta se encuentra dentro
de la aplicación Redmine, la URL es
\url{https://arco.esi.uclm.es:3000/projects/opensheet}.

Además, desde la dirección de Redmine se puede ver la actividad del proyecto y
realizar comparación entre versiones desde la propia web.

\section{OpenSheet API}
La librería o \acs{API} de OpenSheet permite incorporar al desarrollador la
funcionalidad necesaria para trabajar con documentos de hojas de cálculo en Java
de una manera sencilla, tanto en sistemas Windows como \acs{GNU}/Linux. 

OpenSheet API hace uso de OpenOffice.org Calc por lo que necesita que
esté instalado en la máquina donde se va a usar, es necesario que la versión
de la suite ofimática OpenOffice.org sea 3.0 o superior. Al usar OpenOffice.org
Calc se garantiza una gran compatibilidad entre los formatos soportados, un
amplio soporte y madurez.

OpenSheet API es compatible con los formatos xls, xlt, xlsx, ods y odt. Y
permite realizar las siguientes operaciones:
\begin{itemize}
  \item Crear nuevos documentos con cualquiera de los formatos soportados.
  \item Abrir documentos ya existentes de los formatos soportados.
  \item Insertar datos numéricos o de texto a celdas o rango de celdas.
  \item Extraer datos numéricos y de texto de celdas y rangos.
  \item Crear nuevas hojas de cálculo en un documento.
  \item Eliminar hojas de cálculo de un documento.
  \item Realizar conversiones entre documentos de diferente formato.
  \item Exportar un documento a PDF.
\end{itemize}

\subsection{Ejemplos de uso}
A continuación se muestran diferentes fragmentos de código donde se muestra como
llevar a cabo las operaciones comentadas más arriba, con la intención de mostrar
al desarrollador la facilidad de uso de OpenSheet API.

\begin{listing}[
  float,
  language = Java,
  caption  = {Crear un nuevo documento con OpenSheet API}, 
  label    = code:api_nuevo_doc]
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.createDocument("balance.xls");
\end{listing}

El listado~\ref{code:api_nuevo_doc} se realiza la operación de creación de un
nuevo documento, simplemente hay que crear el OpenSheetManager que se encarga de
localizar la instalación de OpenOffice.org, arrancar el proceso y conectarse a
él de manera totalmente transparente y automática; y después solicitarle al
objeto creado que cree un documento con la ruta y el nombre que se desee. La
interfaz IOpenSheetDocument es la que nos permite trabajar con los documentos de
hojas de cálculo.

\begin{listing}[
  float,
  language = Java,
  caption  = {Abrir un documento con OpenSheet API}, 
  label    = code:api_abrir_doc]
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");
\end{listing}

El listado~\ref{code:api_abrir_doc} se muestra como abrir un documento de hojas
de cálculo ya existente, donde al igual que en el ejemplo anterior basta con
crear el objeto OpenSheetManager y usar el método {\it openDocument} con la ruta
y el fichero que se quiere abrir, de manera que nos devuelve la interfaz
IOpenSheetDocument para trabajar con él.

\begin{listing}[
  float,
  language = Java,
  caption  = {Insertar datos a celdas con OpenSheet API}, 
  label    = code:api_insertar_datos_doc]
  
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");

ISpreadSheet sheet1 = oSheetDoc.getSpreadSheet(0);
ISpreadSheet sheetJunio = oSheetDoc.getSpreadSheet("Junio");

sheet1.setCellValue(new CellPosition(0,0), "Revisado");
sheet1.setCellValue(new CellPosition("a2"), "Proyecto A-T");
sheet1.setCellValue(new CellPosition(2,1), 1500.0);
sheetJunio.setCellValue(new CellPosition("a2"), 23.45);
\end{listing}

El listado~\ref{code:api_insertar_datos_doc} se muestra como insertar datos en
celdas de las hojas de cálculo de un documento, para ello se realiza la apertura
de un documento {\it balance.xls} y se obtienen las interfaces ISpreadSheet con
la llamada a {\it getSpreadSheet} para trabajar con dos hojas de cálculo, la
hoja de la primera posición y otra con el nombre {\it Junio}. En las hojas se
insertan los datos de texto o numéricos usando el método {\it setCellValue} con
el objeto que representa la posición de la celda. Este objeto CellPosition
puede ser creado mediante las posiciones X e Y o mediante el nombre de columna
y fila.

\begin{listing}[
  float,
  language = Java,
  caption  = {Insertar datos a rangos con OpenSheet API}, 
  label    = code:api_insertar_datos_rangos_doc]
  
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");

ISpreadSheet sheet1 = oSheetDoc.getSpreadSheet(0);
ISpreadSheet sheetJunio = oSheetDoc.getSpreadSheet("Junio");

sheet1.setRangeValue(new CellPosition(0,0), new CellPosition(0,7), "Revisado");
sheet1.setRangeValue(new CellPosition("c1"), new CellPosition("d6"), "OK");
sheet1.setRangeValue(new CellPosition(1,0), new CellPosition(1,7), 1500.0); 
sheetJunio.setCellValue(new CellPosition("aa2"),new CellPosition("aa6"), 23.45);
\end{listing}

El listado~\ref{code:api_insertar_datos_rangos_doc} se muestra como insertar datos en
rangos de celdas en las hojas de cálculo de un documento, para ello se realiza
la apertura de un documento {\it balance.xls} y se obtienen las interfaces
ISpreadSheet con la llamada a {\it getSpreadSheet} para trabajar con dos hojas
de cálculo, la hoja de la primera posición y otra con el nombre {\it Junio}. En
las hojas se insertan los datos de texto o numéricos usando el método {\it
setRangeValue} con los objetos que representan las posición de las celdas que
definen el rango, la superior izquierda y la inferior derecha. Este objeto
CellPosition puede ser creado mediante las posiciones X e Y o mediante el
nombre de columna y fila. La operación setRangeValue inserta el mismo valor a
todas las celdas del rango.

\begin{listing}[
  float,
  language = Java,
  caption  = {Extraer datos de celdas con OpenSheet API}, 
  label    = code:api_extraer_datos_doc]
  
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");

ISpreadSheet sheet = oSheetDoc.getSpreadSheet("Nominas");

double salario = sheet.getCellValue(new CellPosition("d34"));
String empleado = sheet.getCellText(new CellPosition("c34"));
Object celdaAA1 = sheet.getCellContent(new CellPosition("aa1")); 
\end{listing}

El listado~\ref{code:api_extraer_datos_doc} se muestra como extraer datos de
celdas de una hoja de cálculo de un documento, para ello se realiza la
apertura de un documento {\it balance.xls} y se obtiene la interfaz ISpreadSheet con
la llamada a {\it getSpreadSheet} para trabajar con la hoja de cálculo con el
nombre {\it Nominas}. De la hoja se extraen valores de diferentes celdas, usando
diferentes métodos: {\it getCellValue} para obtener los valores numéricos,
{\it getCellText} para extraer las cadenas de texto y {\it getCellContent} que
extrae lo que contenga la celda sea un valor numérico o texto y lo devuelve
como un Object.

\begin{listing}[
  float,
  language = Java,
  caption  = {Extraer datos de rangos de celdas con OpenSheet API}, 
  label    = code:api_extraer_datos_rango_doc]
  
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");

ISpreadSheet sheet = oSheetDoc.getSpreadSheet("Nominas");

List<Double> salarios = sheet.getRangeValues(new CellPosition("d34"), new
CellPosition("d289")); 
List<String> empleados = sheet.getRangeTexts(new CellPosition("c34"), new
CellPosition("c289"));
List<Object> celdasAA1_AB23 = sheet.getRangeContent(new CellPosition("aa1"),new
CellPosition("ab23"));
\end{listing}

El listado~\ref{code:api_extraer_datos_rango_doc} se muestra como extraer datos
de rangos de celdas de una hoja de cálculo de un documento, para ello se realiza
la apertura de un documento {\it balance.xls} y se obtiene la interfaz ISpreadSheet con
la llamada a {\it getSpreadSheet} para trabajar con la hoja de cálculo con el
nombre {\it Nominas}. De la hoja se extraen diferentes listas de valores de los
diferentes rangos de celdas, usando distintos métodos: {\it
getRangeValues} para obtener la lista con los valores numéricos, {\it
getRangeTexts} para extraer una lista con las cadenas de texto de las celdas y
{\it getRangeContent} que extrae lo que contenga cada una de las celdas del
rango sea un valor numérico o texto y lo devuelve como una lista de Object.

\begin{listing}[
  float,
  language = Java,
  caption  = {Añadir hojas de cálculo con OpenSheet API}, 
  label    = code:api_add_sheet_doc]
  
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");

ISpreadSheet sheetNueva =  oSheetDoc.addSpreadSheet();
ISpreadSheet sheetCierre =  oSheetDoc.addSpreadSheet("Cierre");
\end{listing}

El listado~\ref{code:api_add_sheet_doc} se muestra como añadir nuevas hojas de
cálculo a un documento, para ello se realiza la apertura de un documento {\it
balance.xls} y añaden hojas a través del método {\it addSpreadSheet}. Si se
llama al método sin nombre la crea con uno por defecto, y si se le pasa
un nombre crea una nueva hoja con dicho nombre.

\begin{listing}[
  float,
  language = Java,
  caption  = {Borrar hojas de cálculo con OpenSheet API}, 
  label    = code:api_delete_sheet_doc]
  
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");

oSheetDoc.deleteSpreadSheet(0);
oSheetDoc.deleteSpreadSheet("Balance 2009");
\end{listing}

El listado~\ref{code:api_delete_sheet_doc} se muestra como borrar hojas
de cálculo de un documento, para ello se realiza la apertura de un documento
{\it balance.xls} y borran las hojas deseadas a través del método {\it
deleteSpreadSheet}, bien pasándole la posición de la hoja en el documento o el
nombre de la hoja.

\begin{listing}[
  float,
  language = Java,
  caption  = {Convertir un documento de un formato a otro con OpenSheet API}, 
  label    = code:api_convertir_doc]
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");
oDoc.saveAs("balance.ods");
\end{listing}

El listado~\ref{code:api_convertir_doc} se muestra como convertir un
documento de un formato a otro, para ello basta con salvar un documento de
hojas de cálculo a través del método {\it saveAs} usando la extensión deseada.

\begin{listing}[
  float,
  language = Java,
  caption  = {Exportar un documento a formato PDF con OpenSheet API}, 
  label    = code:api_exportar_doc]
OpenSheetManager oSheetManager = new OpenSheetManager();
IOpenSheetDocument oDoc = oSheetManager.openDocument("balance.xls");
oDoc.exportToPDF("balance.pdf");
\end{listing}

El listado~\ref{code:api_exportar_doc} se muestra como exportar un
documento de un formato a PDF, para ello basta con llamar al método
{\it exportToPDF} con la ruta y el nombre del fichero que se desee y con la
extensión .pdf.

\subsection{Clases y métodos principales}
En este apartado se explican las clases y métodos principales que permiten
al desarrollador hacerse una idea de las herramientas de las que dispone, pero
para entrar en más detalles se debe consultar la documentación en Javadoc del
proyecto OpenSheet que se distribuye con la propia OpenSheet API o
bien generarlo a partir del código disponible en el repositorio.
 
OpenSheet API tiene las siguientes clases principales:
\begin{itemize}
  \item {\bf OpenSheetManager}: Es la clase factoría que permite crear objetos
  con la interfaz IOpenSheetDocument, es decir, los objetos que representan los
  documentos de hojas de cálculo. Cuando se crea un objeto de esta clase, se
  realiza una búsqueda de la instalación de OpenOffice.org en la máquina para
  arrancar el servicio y conectarse a él, todo de manera automática y
  transparente, haciendo uso de la clase modificada Bootstrap del \acs{SDK} de
  \acs{UNO}. Esta clase tiene los siguientes métodos:
	\begin{itemize}
	  \item {\it createDocument(String documentPath)}: Crea un documento con el
	  nombre en la ruta indicada, y devuelve la interfaz IOpenSheetDocument para
	  trabajar con él.
	  \item {\it openDocument(String documentPath)}: Abre un documento en el path
	  indicado, y devuelve la interfaz IOpenSheetDocument para trabajar con él.
	  \item {\it terminate()}: Es la operación que permite liberar los recursos de
	  OpenOffice.org. Al dejar de trabajar con los objetos de OpenSheet API se debe
	  llamar siempre a este método, por lo tanto se debe guardar la instancia de
	  OpenSheetManager hasta que se llame a este método.
	\end{itemize}
  \item {\bf IOpenSheetDocument}: Es la interfaz que permite trabajar con un
  documento de hojas de cálculo. Las operaciones que permite son:
	\begin{itemize}
	  \item {\it getSpreadSheetsNames()}: Devuelve un array con los
	  nombres de todas las hojas de cálculo del documento.
	  \item {\it getSpreadSheet}: Mediante la sobrecarga de métodos permite
	  recuperar el objeto ISpreadSheet, que representa una hoja de cálculo, bien
	  indicando su posición en el documento o indicando su nombre.
	  \item {\it addSpreadSheet}: Mediante la sobrecarga de métodos permite añadir
	  una nueva hoja de cálculo con un nombre por defecto o dándole un nombre
	  específico.
	  \item {\it deleteSpreadSheet}: Mediante la sobrecarga de métodos permite
	  eliminar una hoja de cálculo a través de su posición o de su nombre.
	  \item {\it save()}: Este método permite salvar los cambios realizados en un
	  documento.
	  \item {\it saveAs(String documentPath)}: Permite tanto salvar el
	  documento actual con otro nombre, como realizar una conversión de formato si
	  se usa otra extensión.
	  \item {\it exportToPDF(String documenPath)}: Permite exportar un
	  documento al formato PDF.
	  \item {\it containsSpreadSheet(String name)}: Permite comprobar si el
	  documento contiene una hoja de cálculo con el nombre indicado por parámetro.
	  \item {\it close()}: Cierra el documento y el fichero asociado. Este método
	  debe llamarse siempre después de trabajar con un documento.
	  \end{itemize}
  \item {\bf ISpreadSheet}: Es la interfaz que permite trabajar con un
  hoja de cálculo de un documento y sus celdas. Las operaciones que permite son:
	\begin{itemize}
	  \item {\it setCellValue}: A través de la sobrecarga de métodos permite
	  insertar un valor numérico o un texto a una celda representada por el objeto
	  CellPosition.
	  \item {\it setRangeValue}: A través de la sobrecarga de métodos permite
	  insertar un valor numérico o un texto a un rango de celdas representado por
	  dos objetos CellPosition, el primero que indica la celda de la esquina
	  superior izquierda, y el segundo que indica la celda de la esquina inferior
	  derecha.
	  \item {\it getCellValue(CellPosition position)}: Permite recuperar un {\it
	  double} de una celda representada por el objeto CellPosition.
	  \item {\it getCellText(CellPosition position)}: Permite recuperar un {\it
	  String} de una celda representada por el objeto CellPosition.
	  \item {\it getRangeValues(CellPosition firstCellPosition, CellPosition
	  lastCellPosition)}: Permite recuperar una lista de {\it
	  double} de un rango de celdas representado por dos objetos CellPosition, el
	  primero que indica la celda de la esquina superior izquierda, y el segundo
	  que indica la celda de la esquina inferior derecha.
	  \item {\it getRangeTexts(CellPosition firstCellPosition, CellPosition
	  lastCellPosition)}: Permite recuperar una lista de {\it
	  String} de un rango de celdas representado por dos objetos CellPosition, el
	  primero que indica la celda de la esquina superior izquierda, y el segundo
	  que indica la celda de la esquina inferior derecha.
	  \item {\it getCellContent(CellPosition position)}: Permite recuperar un {\it
	  Object} que contiene el valor de una celda, representada por el
	  objeto CellPosition, sea numérico o texto.
	  \item {\it getRangeContent(CellPosition firstCellPosition, CellPosition
	  lastCellPosition)}: Permite recuperar una lista de {\it
	  Object} que contiene los valores de las celdas de un rango representado por
	  dos objetos CellPosition, el primero que indica la celda de la esquina
	  superior izquierda, y el segundo que indica la celda de la esquina inferior
	  derecha, independientemente de que el contenido sea numérico o texto.
	  \item {\it getName()}: Devuelve el nombre de la hoja de cálculo.
	  \item {\it rename(String spreadSheetNewName)}: Permite cambiar el nombre de
	  la hoja de cálculo.
	 \end{itemize}  
\end{itemize}

\section{OpenSheet Command}
OpenSheet Command es una herramienta escrita en Java que permite operar con
documentos de hojas de cálculo de manera automática a partir de los datos
contenidos en un fichero .properties pasado en la ejecución y de un script,
escrito en Groovy, de acciones seleccionado. Esta herramienta esta diseñada
para ser ejecutada desde un terminal o consola del sistema, y hace uso de
OpenSheet \acs{API} y OpenOffice.org Calc.

La configuración y uso del comando se puede consultar en el manual de usuario.
En este manual se explicará como ampliar su funcionalidad a través de scripts
escritos en Groovy.

\subsection{Clases y métodos principales}
Las clases que componen el comando no son necesarias explicarlas para poder
ampliar su funcionalidad, puesto que basta con entender como funciona la
inyección de dependencias y crear un script en Groovy. Si se desea conocer como
funciona el comando para realizar mejoras o modificaciones se debe consultar la
documentación en Javadoc del proyecto OpenSheet que se distribuye con la propia
OpenSheet API o bien generarlo a partir del código disponible en el repositorio.
 
\subsection{Creación de nuevos scripts}
La principal característica de OpenSheet Command es la posibilidad de ampliar su
funcionalidad a través de scripts a los que se les inyecta las cadenas leídas de
un fichero de datos u objetos si se han usado valores especiales.

El script debe identificar a que variables se les va a inyectar valores del
fichero de datos, y si esperan cadenas o alguno de los objetos que se pueden
inyectar al usar los valores reservados.

Todo valor reservado tiene la sintaxis {\bf @palabra\_reservada;} seguido de
los valores necesarios según la variable especial a utilizar. Existen los
siguientes valores reservados:
\begin{itemize}
  \item {\bf @OpenSheetManager;}: Permite inyectar el objeto OpenSheetManager
  para poder crear y abrir documentos de hojas de cálculo desde el script.
  \item {\bf @OpenSheetDocument;create,}{\it ruta\_fichero}: Crea un nuevo
  documento de hojas de cálculo en la ruta indicada, representado por un objeto
  IOpenSheetDocument que será inyectado a la variable del script indicada.
  \item {\bf @OpenSheetDocument;open,}{\it ruta\_fichero}: Abre un documento de
  hojas de cálculo que se encuentra en la ruta indicada, representado por un
  objeto IOpenSheetDocument que será inyectado a la variable del script indicada.
  \item {\bf @list;}{\it valor1,valor2}: Permite crear un objeto List,
  concretamente una lista de cadenas, donde cada valor va separado por una coma.
  \item {\bf @number;}{\it numero}: Permite crear objetos de tipo Double.
\end{itemize}

Utilizando esos valores y luego mediante el análisis de las cadenas pasadas se
puede conseguir recibir los elementos necesarios para realizar cualquier acción
de manera que pueda cambiar según el fichero de datos utilizado.

Es necesario que las variables a las que se les vaya a inyectar un valor estén
definidas y sean usadas pero sin realizar ninguna asignación, porque la
asignación sobreescribe el valor inyectado.

Se recomienda analizar el script que se distribuye por defecto con el comando,
OpenSheetScript.groovy y leerse el manual de usuario para entender como
funciona y así tener un ejemplo de uso de scripts.

\subsubsection{Limitación de uso de scripts}
Existe la limitación de que actualmente OpenSheet Command no permite ejecutar
scripts de Groovy con dependencias a otros ficheros.

\section{OpenSheet Web Service}
OpenSheet Web Service es un servicio web escrito en Java usando JAX-WS que
permite operar con documentos de hojas de cálculo a partir de la selección de un
script de Groovy y una lista de variables o pares clave/valor. Este servicio
hace uso de OpenSheet \acs{API} y OpenOffice.org Calc.

OpenSheet Web Service proporciona dos operaciones:
\begin{itemize}
  \item {\bf Operación listScripts}: Esta operación devuelve una lista de
  objetos ScriptInfo con la información necesaria para utilizar cada uno de los
  scripts disponibles.
  \item {\bf Operación executeScript}: Esta operación permite ejecutar un script
  pasándole una lista de variables y un objeto Document que puede ser vacío; y
  una vez ejecutado el script se devolverán los objetos Document que indiquen
  las variables de salida configuradas en la información del script.
\end{itemize}

OpenSheet Web Service por tanto permite ejecutar scripts de Groovy de manera
remota para operar con documentos de hojas de cálculo, al igual que se hace con
el comando pero en este caso hay teniendo en cuenta la problemática de la
invocación desde fuera del sistema:
\begin{enumerate}
  \item Los scripts que se pueden ejecutar son aquellos que se encuentran en el
  directorio configurado y que además tienen un fichero .properties con su mismo
  nombre, que contiene las variables con la información necesaria para
  generar objetos ScriptInfo necesarios para la operación listScripts, y las
  variables que necesita el servicio web. Al impedir que se puedan ejecutar
  scripts enviados por el usuario y limitarlos a un directorio donde se
  además se añada un fichero con la información de uso, se evitan problemas de
  seguridad.
  \item Ahora existe el problema de que el mismo script puede ser invocado a la
  vez y que los documentos que envía el cliente del servicio web deben
  almacenarse sin sobreescribir otros y además que se puedan identificar desde
  la lista de variables enviada, que sustituye al fichero de datos. Para ello el
  servicio web en el directorio temporal, crea un nuevo directorio para cada
  llamada a la operación de ejecución donde se almacena el documento enviado, y
  que se le asigna al script para que lo use cuando tenga que guardar ficheros.
  \item Antes de enviar las variables para ejecutar el script, el servicio web
  debe parsear los valores de la variables especiales {\bf @OpenSheetDocument;}
  tanto {\it create} como {\it open} para que el nombre del documento incluya el
  directorio temporal creado por el servicio web.
  \item Se deben identificar todos los ficheros de salida que el servicio web
  debe enviar al cliente, y no borrar dichos ficheros. Estos deben almacenarse
  en el directorio temporal haciendo uso de la valor que el servicio web
  inyectará a la variable configurada para ello, de esta forma una vez leídos
  los ficheros de salida, el servicio web puede borrar todo el contenido del
  directorio temporal. 
\end{enumerate}

\subsection{Clases y métodos principales}
Las clases que componen el servicio web no son necesarias explicarlas para poder
ampliar su funcionalidad, puesto que basta con entender como funciona la
inyección de dependencias y crear un script en Groovy. Si se desea conocer como
funciona el servicio web para realizar mejoras o modificaciones se debe
consultar la documentación en Javadoc del proyecto OpenSheet que se distribuye
con la propia OpenSheet API o bien generarlo a partir del código disponible en
el repositorio.

\subsection{Creación de nuevos scripts}
La principal característica de OpenSheet Web Service es que permite la
posibilidad de ampliar su funcionalidad a través de scripts, al igual que
OpenSheet Command, a los que se les inyecta las cadenas leídas de una lista de
variables enviadas como parámetros u objetos si se han usado
valores especiales.

El script debe identificar a que variables se les va a inyectar valores del
fichero de datos, y si esperan cadenas o alguno de los objetos que se pueden
inyectar al usar los valores reservados.

Todo valor reservado tiene la sintaxis {\bf @palabra\_reservada;} seguido de
los valores necesarios según la variable especial a utilizar, estos son los
mismos que los usados en OpenSheet Commnad.

Para los scripts creados para usar desde el servicio web hay que tener en cuenta
que es necesario tener una variable donde se inyecte la ruta del directorio
temporal creado para cada ejecución, y que será donde se deben almacenar los
ficheros que se generen. Además, las variables que se ha configurado en la
variable {\bf outputVariables} del fichero .properties deben contener la ruta a
los ficheros que el servicio web debe devolver al cliente que invocó la
operación, y estos ficheros no deben ser borrados, si no que serán eliminados
junto con el directorio temporal por el servicio web.

Se recomienda analizar el script que se distribuye por defecto con el comando,
OpenSheetScript.groovy y leerse el manual de usuario para entender como
funciona y como se debe configurar para usarse desde el servicio web.

A continuación se explican los pasos de una ejecución de un script para que se
comprenda mejor el proceso.

\subsection{Pasos de la operación executeScript}
El servicio web cuando tiene recibe una petición a la operación {\it
executeScript} para ejecutar un script realiza los siguientes pasos:
\begin{enumerate}
  \item Se Genera el directorio temporal único para esa llamada.
  \item Si se envía un documento con datos, este se guarda en el directorio
  temporal creado.
  \item Se preprocesan las variables de la lista enviada, añadiendo al nombre
  del documento asignado a las directivas {\it @OpenSheetDocument;create} y
  {\it @OpenSheetDocument;open} la ruta del directorio temporal creado.
  \item Se crea la instancia de ScriptExecuter, que es el mismo objeto que
  ejecuta scripts en el comando, con los valores preprocesados del paso
  anterior.
  \item Se inyecta el valor del directorio temporal a la variable
  {\bf temporalDirectoryVariable} configurada en el fichero .properties del
  script.
  \item Se ejecuta el script.
  \item Se leen todos los ficheros a los que apuntan las variables
  configuradas en el fichero .properties del script a través de la variable
  {\bf outputVariables}.
  \item Se borra el directorio temporal.
  \item Se envía la respuesta.
\end{enumerate}

Con la ejecución de estos pasos se entiende mejor la necesidad de configurar en
el fichero de .properties del script las variables {\it
temporalDirectoryVariable} y {\it outputVariables}.


\subsection{Cliente de ejemplo}
En este apartado se muestra un ejemplo muy básico de un cliente
del servicio web que hace uso del script OpenSheetScript.groovy usando siempre
el mismo valor de las variables al estar escritas directamente en el código.
 
Para poder configurar OpenSheetScript.groovy para poder ser usado como script
del servicio web es necesario añadir un fichero .properties junto a él dentro
del directorio de scripts configurado en el web.xml. El fichero .properties debe
contener la información del
listado~\ref{code:fichero_properties_script_ejemplo2}.

\begin{listing}[ float, language = Bash, 
caption= {Ejemplo de fichero Properties para script de servicio web}, 
label = code:fichero_properties_script_ejemplo2] 
description=OpenSheetScript es el script por defecto que permite realizar las
operaciones basicas de la API. Para saber como se debe usar consultar el manual
de usuario.
outputVariables=saveDocumentTo;extractCellsValuesFile 
temporalDirectoryVariable=tempDir
\end{listing}

Al usar el saveDocumentTo como variable de salida, si se quiere recuperar ese
documento tras la ejecución se le debe asignar un valor en la lista de variables
en cada llamada.

A continuación, se muestra un ejemplo de cliente del servicio web generado
automáticamente a partir del WSDL del servicio usando un plugin que trae
Netbeans para trabajar con aplicaciones web. Concretamente se muestran los
métodos añadidos al código generado automáticamente.

En el listado~\ref{code:ejemplo_cliente_servicio_web} se puede en el método
{\it main} que primero se llama a la operación {\it listScripts()} del servicio
web, imprimiendo por pantalla la información recibida. A continuación, se
construye un mapa de propiedades que se transformará posteriormente en una 
lista de ScriptVariable; en esas propiedades se meten los valores para realizar
las siguientes operaciones:
\begin{enumerate}
  \item Crear un nuevo documento llamado salida.ods
  \item Añadir al documento tres hojas de cálculo One\_Sheet, tab1 y spread.1
  \item Insertar valores diferentes celdas de las hojas de cálculo del
  documento, referenciándolas tanto por nombre como por posición.
  \item Extraer los valores insertados a un fichero.
\end{enumerate}

Después de crear las propiedades se llama a la operación {\it executeScript} del
servicio web, pasándole un documento vacío, puesto que se va a crear en el
servidor, la lista de variables comentada antes y el nombre del primer script
encontrado al listarlos, en este caso al configurar sólo uno sería el
OpenSheetScript.groovy. Con los documentos recibidos se ejecuta una operación
que permite salvarlos a disco. En el
listado~\ref{code:ejemplo_cliente_servicio_web2} se pueden ver los métodos
auxiliares empleados para pasar las propiedades a lista de variables que
necesita la operación, y para almacenar los objetos Document como ficheros.

\begin{listing}[ float, language = Java, 
caption= {Ejemplo de cliente de OpenSheet Web Service - Parte 1}, 
label = code:ejemplo_cliente_servicio_web] 

public class Main {    
    public static void main(String[] args) throws Exception 
    {
        OpenSheetWebServiceService service = new OpenSheetWebServiceService();
        OpenSheetWebService port = service.getOpenSheetWebServicePort();
        List<ScriptInfo> listScripts = port.listScripts();
        for (ScriptInfo sInfo : listScripts)
        {
            System.out.println("name:"+sInfo.getName());
            System.out.println("descr:"+sInfo.getDescription());
            System.out.println("output size:"+sInfo.getOutputVariables().size());
            if (sInfo.getOutputVariables().size() > 0)
                System.out.println("output 0:"+sInfo.getOutputVariables().get(0));
        }

        HashMap propertiesData = new HashMap();
        propertiesData.put("openSheetDocument", "@OpenSheetDocument;create,salida.ods");
        propertiesData.put("logFileName", "");
        propertiesData.put("deleteSheets", "");
        propertiesData.put("deleteSheetsByPosition", "");        
        propertiesData.put("addSheets", "One_Sheet;tab1;spread.1");
        propertiesData.put("insertCellsValues", "tab1:c6=hola mundo!,d5=hello world!;spread.1:aa1=Welcome,a2=11/04/2011");
        propertiesData.put("insertCellsValuesBySheetPosition", "0:a1=sheet with position 0;1:a2= Bienvenido,c7=OpenSheet;2:cc1=Nat");
        propertiesData.put("insertCellsNumericValues", "tab1:a11=2.45;One_Sheet:b2=10.0,c1=345.678");
        propertiesData.put("insertCellsNumericValuesBySheetPosition", "0:a11=11.12;3:b22=23.0,c2=987.6543");
        propertiesData.put("extractCellsValues", "tab1:c6,d5,a11;spread.1:aa1,a2;One_Sheet:b2,c1");
        propertiesData.put("extractCellsValuesBySheetPosition", "0:a1,a11;1:a2,c7;2:cc1;3:b22,c2");
        propertiesData.put("extractCellsValuesFile", "");
        propertiesData.put("saveDocumentTo", "salida1.ods");        

        List<Document> documents = port.executeScript(new Document(), getScriptVariables(propertiesData), listScripts.get(0).getName());
        saveDocuments(documents);
    }
 }
\end{listing}

\begin{listing}[ float, language = Java, 
caption= {Ejemplo de cliente de OpenSheet Web Service - Parte 2}, 
label = code:ejemplo_cliente_servicio_web2] 
    
    public static List <ScriptVariable> getScriptVariables(Map<String,String> variablesMap)
    {
        List<ScriptVariable> list = new ArrayList<ScriptVariable>();

        for (String key : variablesMap.keySet())
        {
            ScriptVariable var = new ScriptVariable();
            var.setName(key);
            var.setValue(variablesMap.get(key));
            list.add(var);
        }
        return list;
    }

    private static void saveDocuments(List<Document> documents) throws IOException
    {
        for (Document doc : documents)
        {
            System.out.println("\nSave document '"+doc.getName()+"'");
            saveDocument(doc);
        }
    }

    public static void saveDocument(Document document) throws IOException
    {
        FileOutputStream fos = null;
        try
        {
            File documentFile = new File(document.getName());
            fos = new FileOutputStream(documentFile);
            fos.write(document.getContent());
        }
        finally
        {
            fos.close();
        }
    }
\end{listing}

% Local Variables:
% coding: utf-8
% mode: latex
% TeX-master: "main"
% mode: flyspell
% ispell-local-dictionary: "castellano8"
% End:
