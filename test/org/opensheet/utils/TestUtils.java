/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author cruiz
 */
public class TestUtils {
    public static void writeScriptToFile(String fileName, String scriptText) throws Exception
    {
        File scriptFile = new File(fileName);
        Writer output = new BufferedWriter(new FileWriter(scriptFile));
        output.write(scriptText);
        output.close();
    }

    public static void writePropertiesFile(String fileName, Map<String, Object> propertiesData) throws Exception
    {
        Properties properties = new Properties();
        for (String key : propertiesData.keySet())
        {
            properties.put(key, propertiesData.get(key));
        }
        properties.store(new FileOutputStream(fileName), "properties file with script data");
    }
}
