/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.command;

import org.opensheet.api.exceptions.LoadOpenSheetDocumentException;
import org.opensheet.api.exceptions.UnsupportedOpenSheetDocumentException;
import org.opensheet.api.exceptions.OverwriteOpenSheetDocumentException;
import org.opensheet.command.exceptions.OpenSheetPropertyFormatException;
import org.opensheet.command.exceptions.LoadOpenSheetDataFileException;
import org.opensheet.command.exceptions.OpenSheetRunScriptException;
import com.sun.star.comp.helper.BootstrapException;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import org.opensheet.command.exceptions.OpenSheetCommandInvalidParamsException;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.mockito.Mockito.*;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.junit.runner.RunWith;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opensheet.api.OpenSheetManager;
import org.opensheet.utils.TestUtils;
import static org.junit.Assert.*;

/**
 *
 * @author cruiz
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({OpenSheetCommand.class})
@PowerMockIgnore({"org.codehaus.groovy.*", "groovy.lang.*"})
public class OpenSheetCommandTest {

    public OpenSheetCommandTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetParamsWithHelpParam() throws Exception
    {
        String [] args = {"-help"};
        verifyInvokeShowHelpInfo(args);
    }

    @Test
    public void testSetParamsWithoutParams() throws Exception
    {
        String [] args = {};
        verifyInvokeShowHelpInfo(args);
    }

    private void verifyInvokeShowHelpInfo(String args[]) throws Exception
    {
        OpenSheetCommand commandSpy = spy(new OpenSheetCommand());
        commandSpy.setParams(args);
        commandSpy.run();
        verifyPrivate(commandSpy, times(1)).invoke("showHelpInfo");
    }

    @Test
    public void testSetParamsWithValidParams() throws Exception
    {
        String dataFile = "file.properties";
        String scriptFile = "script.groovy";
        String[] args =
        {
            "-data=" + dataFile, "-script=" + scriptFile
        };

        new File(dataFile).delete();
        new File(scriptFile).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("name", "World");
        propertiesData.put("myFirstScript", " This is my first script.");

        TestUtils.writeScriptToFile(scriptFile, "package groovyapp\n\noutput = \"Hello $name!\" + myFirstScript");
        TestUtils.writePropertiesFile(dataFile, propertiesData);

        OpenSheetCommand command = new OpenSheetCommand();
        command.setParams(args);

        assertEquals(dataFile, command.getDataFile());
        assertEquals(new File(scriptFile), command.getScriptFile());

        new File(dataFile).delete();
        new File(scriptFile).delete();
    }

    @Test
    public void testSetParamsWithNoExistsDataFile() throws Exception
    {
        String dataFile = "file.properties";
        String scriptFile = "script.groovy";
        String[] args =
        {
            "-data=" + dataFile, "-script=" + scriptFile
        };

        new File(dataFile).delete();
        new File(scriptFile).delete();
       
        TestUtils.writeScriptToFile(scriptFile, "package groovyapp\n\noutput = \"Hello $name!\" + myFirstScript");        

        checkSetParamsCommandInvalidParamsException(args);
        new File(scriptFile).delete();
    }

    @Test
    public void testSetParamsWithNoExistsScriptFile() throws Exception
    {
        String dataFile = "file.properties";
        String scriptFile = "script.groovy";
        String[] args =
        {
            "-data=" + dataFile, "-script=" + scriptFile
        };

        new File(dataFile).delete();
        new File(scriptFile).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("name", "World");
        propertiesData.put("myFirstScript", " This is my first script.");

        TestUtils.writePropertiesFile(dataFile, propertiesData);

        checkSetParamsCommandInvalidParamsException(args);
        new File(dataFile).delete();
    }

    @Test
    public void testSetParamsWithOneInvalidParam() throws Exception
    {
        String [] args = {"invalidParam"};
        checkSetParamsCommandInvalidParamsException(args);
    }

    @Test
    public void testSetParamsWithOnlyOneValidParam() throws Exception
    {
        String [] args = {"-data=file.properties"};
        checkSetParamsCommandInvalidParamsException(args);
    }        

    @Test
    public void testSetParamsWithTwoParamsInvalid() throws Exception
    {
        String [] args = {"-data=file.properties", "-scpt=script.groovy", };
        checkSetParamsCommandInvalidParamsException(args);
    }

    @Test
    public void testSetParamsWithThreeParams() throws Exception
    {
        String [] args = {"showHelpInfo","-data=file.properties", "-script=script.groovy", };
        checkSetParamsCommandInvalidParamsException(args);
    }

    private void checkSetParamsCommandInvalidParamsException(String []args)
    {
        OpenSheetCommand command = new OpenSheetCommand();

        try
        {
            command.setParams(args);
            fail("'setParams' must throw OpenSheetCommandInvalidParamsException");
        }
        catch (OpenSheetCommandInvalidParamsException ex)
        {
        }
    }


    @Test
    public void testCallsInRunWithValidParams() throws Exception
    {
        String dataFile = "file.properties";
        String scriptFile = "script.groovy";
        String[] args =
        {
            "-data=" + dataFile, "-script=" + scriptFile
        };

        new File(dataFile).delete();
        new File(scriptFile).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("name", "World");
        propertiesData.put("myFirstScript", " This is my first script.");

        TestUtils.writeScriptToFile(scriptFile, "package groovyapp\n\noutput = \"Hello $name!\" + myFirstScript");
        TestUtils.writePropertiesFile(dataFile, propertiesData);

        OpenSheetCommand commandSpy = spy(new OpenSheetCommand()); //Spy OpenSheetCommand to check private methods calls
        OpenSheetManager managerMock = mock(OpenSheetManager.class);
        ScriptExecuter scriptExecuterSpy = spy(new ScriptExecuter(managerMock, dataFile));
        
        commandSpy.setParams(args);        

        when(commandSpy, "createOpenSheetManager").thenReturn(managerMock);
        when(commandSpy, "createScriptExecuter", managerMock).thenReturn(scriptExecuterSpy);        

        commandSpy.run();
                
        //check calls into 'run' method
        verify(scriptExecuterSpy, times(1)).run(commandSpy.getScriptFile());
        verifyPrivate(commandSpy, times(0)).invoke("showHelpInfo");
        verifyPrivate(commandSpy, times(1)).invoke("checkParams");
        verifyPrivate(commandSpy, times(1)).invoke("createOpenSheetManager");
        verifyPrivate(commandSpy, times(1)).invoke("createScriptExecuter", managerMock);                
        verify(managerMock, times(1)).terminate();

        new File(dataFile).delete();
        new File(scriptFile).delete();
    }

    @Test
    public void testRunWithValidParams() throws Exception
    {
        String dataFile = "file.properties";
        String scriptFile = "script.groovy";
        String[] args =
        {
            "-data=" + dataFile, "-script=" + scriptFile
        };

        new File(dataFile).delete();
        new File(scriptFile).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("name", "World");
        propertiesData.put("myFirstScript", " This is my first script.");

        TestUtils.writeScriptToFile(scriptFile, "package groovyapp\n\noutput = \"Hello $name!\" + myFirstScript");
        TestUtils.writePropertiesFile(dataFile, propertiesData);

        OpenSheetCommand command = new OpenSheetCommand();        
        command.setParams(args);        
        Map runVars = command.run();

        //check the script was executed
        assertEquals(runVars.get("output").toString(), "Hello World! This is my first script.");

        new File(dataFile).delete();
        new File(scriptFile).delete();
    }

    @Test
    public void testRunWithoutSetParams() throws Exception
    {
        OpenSheetCommand command = new OpenSheetCommand();
        try
        {
            command.run();
            fail("The method must throw OpenSheetCommandInvalidParamsException");
        }
        catch (OpenSheetCommandInvalidParamsException ex)
        {
        }        
    }
    
    @Test
    public void testRunWithBootstrapException() throws Exception
    {
        checkOpenSheetRunScriptException(new BootstrapException(), false);
    }    

    @Test
    public void testRunWithLoadOpenSheetDataFileException() throws Exception
    {
        checkOpenSheetRunScriptException(new LoadOpenSheetDataFileException(""), true);
    }

    @Test
    public void testRunWithOpenSheetPropertyFormatException() throws Exception
    {
        checkOpenSheetRunScriptException(new OpenSheetPropertyFormatException(""), true);
    }

    @Test
    public void testRunWithOverwriteOpenSheetDocumentException() throws Exception
    {
        checkOpenSheetRunScriptException(new OverwriteOpenSheetDocumentException(""), true);
    }

    @Test
    public void testRunWithUnsupportedOpenSheetDocumentException() throws Exception
    {
        checkOpenSheetRunScriptException(new UnsupportedOpenSheetDocumentException(""), true);
    }
    
    @Test
    public void testRunWithLoadOpenSheetDocumentException() throws Exception
    {
        checkOpenSheetRunScriptException(new LoadOpenSheetDocumentException(""), true);
    }

    void checkOpenSheetRunScriptException(Exception exception, boolean invokeCreateScriptExecuter) throws Exception
    {
        String dataFile = "file.properties";
        String scriptFile = "script.groovy";
        String[] args =
        {
            "-data=" + dataFile, "-script=" + scriptFile
        };

        new File(dataFile).delete();
        new File(scriptFile).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("name", "World");
        propertiesData.put("myFirstScript", " This is my first script.");

        TestUtils.writeScriptToFile(scriptFile, "package groovyapp\n\noutput = \"Hello $name!\" + myFirstScript");
        TestUtils.writePropertiesFile(dataFile, propertiesData);

        OpenSheetCommand commandSpy = spy(new OpenSheetCommand());

        commandSpy.setParams(args);

        if (invokeCreateScriptExecuter)
        {            
            when(commandSpy, "createScriptExecuter", any()).thenThrow(exception);
        }
        else
        {
            when(commandSpy, "createOpenSheetManager").thenThrow(new BootstrapException());
        }

        try
        {
            commandSpy.run();
            fail("Method must throw OpenSheetRunScriptException");
        }
        catch (OpenSheetRunScriptException ex)
        {
        }

        //check calls into 'run' method
        verifyPrivate(commandSpy, times(0)).invoke("showHelpInfo");
        verifyPrivate(commandSpy, times(1)).invoke("checkParams");
        verifyPrivate(commandSpy, times(1)).invoke("createOpenSheetManager");
        
        if (invokeCreateScriptExecuter)
        {
            verifyPrivate(commandSpy, times(1)).invoke("createScriptExecuter", any());
        }
        else
        {
            verifyPrivate(commandSpy, times(0)).invoke("createScriptExecuter", any());
        }

        new File(dataFile).delete();
        new File(scriptFile).delete();
    }

}