/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.command;

import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Properties;
import org.opensheet.api.IOpenSheetDocument;
import org.opensheet.utils.TestUtils;
import org.opensheet.api.OpenSheetManager;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
/**
 *
 * @author cruiz
 */
public class OpenSheetScriptTest {

    public OpenSheetScriptTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createAndSaveTest() throws Exception
    {
        String dataFile = "test.properties";
        String docName = "docNuevoODS.ods";               

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("openSheetDocument", "@OpenSheetDocument;create,"+docName);
        propertiesData.put("logFileName", "");
        propertiesData.put("deleteSheets", "");
        propertiesData.put("deleteSheetsByPosition", "");
        propertiesData.put("addSheets", "");
        propertiesData.put("insertCellsValues", "");
        propertiesData.put("insertCellsValuesBySheetPosition", "");
        propertiesData.put("insertCellsNumericValues", "");
        propertiesData.put("insertCellsNumericValuesBySheetPosition", "");
        propertiesData.put("extractCellsValues", "");
        propertiesData.put("extractCellsValuesBySheetPosition", "");
        propertiesData.put("extractCellsValuesFile", "");
        propertiesData.put("saveDocumentTo", "");        
        
        TestUtils.writePropertiesFile(dataFile, propertiesData);

        File docFile = new File(docName);
        File logFile = new File("opensheet.log");
        File outputFile = new File("opensheet.out");        

        docFile.delete();
        logFile.delete();
        outputFile.delete();

        assertFalse(docFile.exists());
        assertFalse(logFile.exists());
        assertFalse(outputFile.exists());

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);
        OpenSheetManager manager = new OpenSheetManager();
        IOpenSheetDocument oSheetDocSpy = spy(manager.createDocument(docName));
        
        when(openSheetManagerMock.createDocument(anyString())).thenReturn(oSheetDocSpy);

        ScriptExecuter scriptExecuter = new ScriptExecuter(openSheetManagerMock, dataFile);

        File script =  new File("./src-link/org/opensheet/scripts/OpenSheetScript.groovy");
        assertTrue(script.exists());

        scriptExecuter.run(script);

        assertTrue(docFile.exists());
        assertTrue(logFile.exists());
        assertFalse(outputFile.exists());

        verify(openSheetManagerMock, times(1)).createDocument(docName);
        verify(oSheetDocSpy, times(0)).deleteSpreadSheet(anyString());
        verify(oSheetDocSpy, times(0)).deleteSpreadSheet(anyInt());
        verify(oSheetDocSpy, times(0)).addSpreadSheet();
        verify(oSheetDocSpy, times(0)).addSpreadSheet(anyString());
        verify(oSheetDocSpy, times(0)).getSpreadSheet(anyString());
        verify(oSheetDocSpy, times(0)).getSpreadSheet(anyInt());
        verify(oSheetDocSpy, times(0)).saveAs(anyString());
        verify(oSheetDocSpy, times(1)).save();
        verify(oSheetDocSpy, times(1)).close();

        docFile.delete();
        logFile.delete();
        new File(dataFile).delete();        
    }

    @Test
    public void OpenAndDeleteSheetsByNameTest() throws Exception
    {
        String dataFile = "test.properties";
        String docName = "docNuevoODS.ods";

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("openSheetDocument", "@OpenSheetDocument;open,"+docName);
        propertiesData.put("logFileName", "");
        propertiesData.put("deleteSheets", "One Sheet;tab1;spread.1");
        propertiesData.put("deleteSheetsByPosition", "");
        propertiesData.put("addSheets", "");
        propertiesData.put("insertCellsValues", "");
        propertiesData.put("insertCellsValuesBySheetPosition", "");
        propertiesData.put("insertCellsNumericValues", "");
        propertiesData.put("insertCellsNumericValuesBySheetPosition", "");
        propertiesData.put("extractCellsValues", "");
        propertiesData.put("extractCellsValuesBySheetPosition", "");
        propertiesData.put("extractCellsValuesFile", "");
        propertiesData.put("saveDocumentTo", "");

        TestUtils.writePropertiesFile(dataFile, propertiesData);

        File docFile = new File(docName);
        File logFile = new File("opensheet.log");
        File outputFile = new File("opensheet.out");

        docFile.delete();
        logFile.delete();
        outputFile.delete();

        assertFalse(docFile.exists());
        assertFalse(logFile.exists());
        assertFalse(outputFile.exists());

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);
        OpenSheetManager manager = new OpenSheetManager();
        IOpenSheetDocument oSheetDoc = manager.createDocument(docName);
        oSheetDoc.addSpreadSheet("One Sheet");
        oSheetDoc.addSpreadSheet("tab1");
        oSheetDoc.addSpreadSheet("spread.1");
        IOpenSheetDocument oSheetDocSpy = spy(oSheetDoc);

        when(openSheetManagerMock.openDocument(anyString())).thenReturn(oSheetDocSpy);

        ScriptExecuter scriptExecuter = new ScriptExecuter(openSheetManagerMock, dataFile);

        File script =  new File("./src-link/org/opensheet/scripts/OpenSheetScript.groovy");
        assertTrue(script.exists());

        scriptExecuter.run(script);

        assertTrue(docFile.exists());
        assertTrue(logFile.exists());
        assertFalse(outputFile.exists());

        verify(openSheetManagerMock, times(0)).createDocument(anyString());
        verify(openSheetManagerMock, times(1)).openDocument(docName);
        verify(oSheetDocSpy, times(3)).deleteSpreadSheet(anyString());
        verify(oSheetDocSpy, times(1)).deleteSpreadSheet("One Sheet");
        verify(oSheetDocSpy, times(1)).deleteSpreadSheet("tab1");
        verify(oSheetDocSpy, times(1)).deleteSpreadSheet("spread.1");        
        verify(oSheetDocSpy, times(0)).deleteSpreadSheet(anyInt());
        verify(oSheetDocSpy, times(0)).addSpreadSheet();
        verify(oSheetDocSpy, times(0)).addSpreadSheet(anyString());
        verify(oSheetDocSpy, times(0)).getSpreadSheet(anyString());
        verify(oSheetDocSpy, times(0)).getSpreadSheet(anyInt());
        verify(oSheetDocSpy, times(0)).saveAs(anyString());
        verify(oSheetDocSpy, times(1)).save();
        verify(oSheetDocSpy, times(1)).close();

        docFile.delete();
        logFile.delete();
        new File(dataFile).delete();
    }

    @Test
    public void OpenAddSheetsAndDeleteByPosTest() throws Exception
    {
        String dataFile = "test.properties";
        String docName = "docNuevoODS.ods";

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("openSheetDocument", "@OpenSheetDocument;open,"+docName);
        propertiesData.put("logFileName", "");
        propertiesData.put("deleteSheets", "");
        propertiesData.put("deleteSheetsByPosition", "2;1;0");
        propertiesData.put("addSheets", "One Sheet;tab1;spread.1");
        propertiesData.put("insertCellsValues", "");
        propertiesData.put("insertCellsValuesBySheetPosition", "");
        propertiesData.put("insertCellsNumericValues", "");
        propertiesData.put("insertCellsNumericValuesBySheetPosition", "");
        propertiesData.put("extractCellsValues", "");
        propertiesData.put("extractCellsValuesBySheetPosition", "");
        propertiesData.put("extractCellsValuesFile", "");
        propertiesData.put("saveDocumentTo", "");

        TestUtils.writePropertiesFile(dataFile, propertiesData);

        File docFile = new File(docName);
        File logFile = new File("opensheet.log");
        File outputFile = new File("opensheet.out");

        docFile.delete();
        logFile.delete();
        outputFile.delete();        

        assertFalse(docFile.exists());
        assertFalse(logFile.exists());
        assertFalse(outputFile.exists());

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);
        OpenSheetManager manager = new OpenSheetManager();        
        IOpenSheetDocument oSheetDocSpy = spy(manager.createDocument(docName));

        when(openSheetManagerMock.openDocument(anyString())).thenReturn(oSheetDocSpy);

        ScriptExecuter scriptExecuter = new ScriptExecuter(openSheetManagerMock, dataFile);

        File script =  new File("./src-link/org/opensheet/scripts/OpenSheetScript.groovy");
        assertTrue(script.exists());

        scriptExecuter.run(script);

        assertTrue(docFile.exists());
        assertTrue(logFile.exists());
        assertFalse(outputFile.exists());

        verify(openSheetManagerMock, times(0)).createDocument(anyString());
        verify(openSheetManagerMock, times(1)).openDocument(docName);        
        verify(oSheetDocSpy, times(3)).deleteSpreadSheet(anyInt());
        verify(oSheetDocSpy, times(1)).deleteSpreadSheet(2);
        verify(oSheetDocSpy, times(1)).deleteSpreadSheet(1);
        verify(oSheetDocSpy, times(1)).deleteSpreadSheet(0);
        verify(oSheetDocSpy, times(0)).addSpreadSheet();
        verify(oSheetDocSpy, times(3)).addSpreadSheet(anyString());
        verify(oSheetDocSpy, times(1)).addSpreadSheet("One Sheet");
        verify(oSheetDocSpy, times(1)).addSpreadSheet("tab1");
        verify(oSheetDocSpy, times(1)).addSpreadSheet("spread.1");
        verify(oSheetDocSpy, times(0)).saveAs(anyString());
        verify(oSheetDocSpy, times(1)).save();
        verify(oSheetDocSpy, times(1)).close();

        docFile.delete();
        logFile.delete();
        new File(dataFile).delete();
    }

    @Test
    public void OpenInsertCellsValuesExportValuesAndSaveToTest() throws Exception
    {
        String dataFile = "test.properties";
        String docName = "docNuevoODS.ods";

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("openSheetDocument", "@OpenSheetDocument;open,"+docName);
        propertiesData.put("logFileName", "prueba.log");
        propertiesData.put("deleteSheets", "");
        propertiesData.put("deleteSheetsByPosition", "");
        propertiesData.put("addSheets", "One_Sheet;tab1;spread.1");
        propertiesData.put("insertCellsValues", "tab1:c6=hola mundo!,d5=hello world!;spread.1:aa1=Welcome,a2=11/04/2011");
        propertiesData.put("insertCellsValuesBySheetPosition", "0:a1=sheet with position 0;1:a2= Bienvenido,c7=OpenSheet;2:cc1=Nat");
        propertiesData.put("insertCellsNumericValues", "tab1:a11=2.45;One_Sheet:b2=10.0,c1=345.678");
        propertiesData.put("insertCellsNumericValuesBySheetPosition", "0:a11=11.12;3:b22=23.0,c2=987.6543");
        propertiesData.put("extractCellsValues", "tab1:c6,d5,a11;spread.1:aa1,a2;One_Sheet:b2,c1");
        propertiesData.put("extractCellsValuesBySheetPosition", "0:a1,a11;1:a2,c7;2:cc1;3:b22,c2");
        propertiesData.put("extractCellsValuesFile", "");
        propertiesData.put("saveDocumentTo", "prueba.pdf");

        TestUtils.writePropertiesFile(dataFile, propertiesData);

        File docFile = new File(docName);
        File logFile = new File("prueba.log");
        File outputFile = new File("opensheet.out");

        docFile.delete();
        logFile.delete();
        outputFile.delete();
        new File("prueba.pdf").delete();        

        assertFalse(docFile.exists());
        assertFalse(logFile.exists());
        assertFalse(outputFile.exists());
        assertFalse(new File("prueba.pdf").exists());

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);
        OpenSheetManager manager = new OpenSheetManager();
        IOpenSheetDocument oSheetDocSpy = spy(manager.createDocument(docName));

        when(openSheetManagerMock.openDocument(anyString())).thenReturn(oSheetDocSpy);

        ScriptExecuter scriptExecuter = new ScriptExecuter(openSheetManagerMock, dataFile);

        File script =  new File("./src-link/org/opensheet/scripts/OpenSheetScript.groovy");
        assertTrue(script.exists());

        scriptExecuter.run(script);

        assertTrue(docFile.exists());
        assertTrue(logFile.exists());
        assertTrue(outputFile.exists());
        assertTrue(new File("prueba.pdf").exists());

        verify(openSheetManagerMock, times(0)).createDocument(anyString());
        verify(openSheetManagerMock, times(1)).openDocument(docName);
        verify(oSheetDocSpy, times(0)).deleteSpreadSheet(anyInt());
        verify(oSheetDocSpy, times(0)).deleteSpreadSheet(anyString());
        verify(oSheetDocSpy, times(0)).addSpreadSheet();
        verify(oSheetDocSpy, times(3)).addSpreadSheet(anyString());
        verify(oSheetDocSpy, times(1)).exportToPDF("prueba.pdf");
        verify(oSheetDocSpy, times(0)).save();
        verify(oSheetDocSpy, times(1)).close();

        Properties prop = new Properties();
        InputStream is = new FileInputStream("opensheet.out");
        prop.load(is);
        
        assertTrue(prop.getProperty("tab1.c6").equals("hola mundo!"));
        assertTrue(prop.getProperty("tab1.d5").equals("hello world!"));
        assertTrue(prop.getProperty("tab1.d5").equals("hello world!"));
        assertTrue(prop.getProperty("tab1.a11").equals("2.45"));
        assertTrue(prop.getProperty("spread.1.aa1").equals("Welcome"));
        assertTrue(prop.getProperty("spread.1.a2").equals("11/04/2011"));
        assertTrue(prop.getProperty("One_Sheet.b2").equals("10.0"));
        assertTrue(prop.getProperty("One_Sheet.c1").equals("345.678"));
        assertTrue(prop.getProperty("0.a1").equals("sheet with position 0"));
        assertTrue(prop.getProperty("0.a11").equals("11.12"));
        assertTrue(prop.getProperty("1.a2").equals("Bienvenido"));
        assertTrue(prop.getProperty("1.c7").equals("OpenSheet"));
        assertTrue(prop.getProperty("2.cc1").equals("Nat"));
        assertTrue(prop.getProperty("3.b22").equals("23.0"));
        assertTrue(prop.getProperty("3.c2").equals("987.6543"));

        docFile.delete();
        logFile.delete();
        new File(dataFile).delete();
        outputFile.delete();
        new File("prueba.pdf").delete();

    }

}