/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.command;

import java.util.Collection;
import java.util.List;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Parameterized;
import org.junit.runner.RunWith;
import java.io.FileInputStream;
import org.opensheet.utils.TestUtils;
import org.opensheet.command.exceptions.OpenSheetRunScriptException;
import org.codehaus.groovy.control.CompilationFailedException;
import groovy.lang.GroovyShell;
import groovy.lang.Binding;
import groovy.lang.Script;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.Writer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;

import org.junit.Test;
import org.opensheet.api.IOpenSheetDocument;
import org.opensheet.api.OpenSheetManager;
import org.opensheet.command.exceptions.LoadOpenSheetDataFileException;
import org.opensheet.command.exceptions.OpenSheetPropertyFormatException;
import static org.mockito.Mockito.*;

/**
 *
 * @author cruiz
 */
@RunWith(Parameterized.class)
public class ScriptExecuterTest
{

    private static OpenSheetManager openSheetManager;
    private int constructor; //Indicates what construtor method will be use.
    public final static int PROPERTIES_FILE_CONTRUCTOR_METHOD = 0;
    public final static int PROPERTIES_OBJECT_CONTRUCTOR_METHOD = 1;

    public ScriptExecuterTest(int constructor)
    {
        this.constructor = constructor;
    }

    @Parameters
    public static Collection<Integer[]> data()
    {
        Collection<Integer[]> constructorMethods = new ArrayList();
        constructorMethods.add(new Integer[] {PROPERTIES_FILE_CONTRUCTOR_METHOD});
        constructorMethods.add(new Integer[] {PROPERTIES_OBJECT_CONTRUCTOR_METHOD});
        return constructorMethods;
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        openSheetManager = new OpenSheetManager();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        openSheetManager.terminate();
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    private ScriptExecuter createScriptExecuter(OpenSheetManager manager, String dataFile) throws Exception
    {
        ScriptExecuter scriptExecuter = null;
        switch (this.constructor)
        {
            case PROPERTIES_FILE_CONTRUCTOR_METHOD:
                scriptExecuter = new ScriptExecuter(manager, dataFile);
                break;
            case PROPERTIES_OBJECT_CONTRUCTOR_METHOD:
                try 
                {
                    Properties properties = new Properties();
                    properties.load(new FileInputStream(dataFile));
                    scriptExecuter = new ScriptExecuter(manager, properties);
                }catch (IOException ex)
                {
                    throw new LoadOpenSheetDataFileException(ex);
                }                                
                break;
        }

        return scriptExecuter;
    }

    @Test
    public void testBindingBlankPropertiesFile() throws Exception
    {
        String dataFile = "test.properties";
        new File(dataFile).delete();

        TestUtils.writePropertiesFile(dataFile, new HashMap<String, Object>());

        ScriptExecuter scriptExecuter = createScriptExecuter(openSheetManager, dataFile);
        Binding binding = scriptExecuter.getBinding();

        assertTrue(binding.getVariables().isEmpty());

        new File(dataFile).delete();
    }

    @Test
    public void testBindingBasicStringPropertiesFile() throws Exception
    {

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "2.5");
        propertiesData.put("var.property.key", "OpenSheet");

        checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, propertiesData);
    }

    @Test
    public void testBindingNumberPropertiesFile() throws Exception
    {
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.5");
        propertiesData.put("var.property.key", "OpenSheet");
        propertiesData.put("var2.property.key", "@number;99.4562");

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", 2.5);
        expectedProperties.put("var.property.key", "OpenSheet");
        expectedProperties.put("var2.property.key", 99.4562);

        checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, expectedProperties);
    }

    @Test
    public void testBindingBadNumberPropertiesFile() throws Exception
    {
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;aa?");
        propertiesData.put("var.property.key", "OpenSheet");
        propertiesData.put("var2.property.key", "@number;99.4562");

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", "aa?");
        expectedProperties.put("var.property.key", "OpenSheet");
        expectedProperties.put("var2.property.key", 99.4562);

        try
        {
            checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, expectedProperties);
            fail("'ScriptExecuter constructor must launch OpenSheetPropertyFormatException'");
        }
        catch (OpenSheetPropertyFormatException ex)
        {
        }
    }

    @Test
    public void testBindingEmptyNumberPropertiesFile() throws Exception
    {
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;");
        propertiesData.put("var.property.key", "OpenSheet");
        propertiesData.put("var2.property.key", "@number;99.4562");

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", "");
        expectedProperties.put("var.property.key", "OpenSheet");
        expectedProperties.put("var2.property.key", 99.4562);

        try
        {
            checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, expectedProperties);
            fail("'ScriptExecuter constructor must launch OpenSheetPropertyFormatException'");
        }
        catch (OpenSheetPropertyFormatException ex)
        {
        }
    }

    @Test
    public void testBindingWithEmptyValueInPropertiesFile() throws Exception
    {

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "");
        propertiesData.put("var.property.key", "OpenSheet");

        checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, propertiesData);
    }

    @Test
    public void testBindingSimpleListPropertiesFile() throws Exception
    {
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.34");
        propertiesData.put("var2.property.key", "@number;99.4562");
        propertiesData.put("var.ls.key", "@list;saludos ,hello!,¡hola bienvenido!,OpenSheet, Welcome to ... OpenSheet;");

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", 2.34);
        expectedProperties.put("var2.property.key", 99.4562);

        ArrayList list = new ArrayList();

        list.add("saludos ");
        list.add("hello!");
        list.add("¡hola bienvenido!");
        list.add("OpenSheet");
        list.add(" Welcome to ... OpenSheet;");

        expectedProperties.put("var.ls.key", list);

        checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, expectedProperties);
    }

    @Test
    public void testBindingListPropertiesFileWithCommaScape() throws Exception
    {
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.34");
        propertiesData.put("var2.property.key", "@number;99.4562");
        propertiesData.put("var.ls.key", "@list;saludos ,hello!,¡hola bienvenido!,OpenSheet,/home/crr/, Welcome to ... OpenSheet;,//,value enter commas//,");

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", 2.34);
        expectedProperties.put("var2.property.key", 99.4562);

        ArrayList list = new ArrayList();

        list.add("saludos ");
        list.add("hello!");
        list.add("¡hola bienvenido!");
        list.add("OpenSheet");
        list.add("/home/crr/");
        list.add(" Welcome to ... OpenSheet;");
        list.add(",value enter commas,");

        expectedProperties.put("var.ls.key", list);

        checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, expectedProperties);
    }

    @Test
    public void testBindingListPropertiesFileWithEmptyString() throws Exception
    {
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.34");
        propertiesData.put("var2.property.key", "@number;99.4562");
        propertiesData.put("var.ls.key", "@list;,saludos ,,¡hola bienvenido!,OpenSheet, Welcome to ... OpenSheet;");

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", 2.34);
        expectedProperties.put("var2.property.key", 99.4562);

        ArrayList list = new ArrayList();

        list.add("");
        list.add("saludos ");
        list.add("");
        list.add("¡hola bienvenido!");
        list.add("OpenSheet");
        list.add(" Welcome to ... OpenSheet;");

        expectedProperties.put("var.ls.key", list);

        checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, expectedProperties);
    }

    @Test
    public void testBindingOpenSheetManagerPropertiesFile() throws Exception
    {
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.5");
        propertiesData.put("manager", "@OpenSheetManager;");

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", 2.5);
        /* Caso especial al tener como entrada @OpenSheetManager se ignara el valor esperado,
         * se debe comprobar que el valor devuelto es un objecto del tipo esperado.
         */
        expectedProperties.put("manager", "");

        checkBindingPropertiesFile(openSheetManager, "test.properties", propertiesData, expectedProperties);
    }

    @Test
    public void testBindingOpenSheetDocumentPropertiesFileWithCreateParam() throws Exception
    {
        String docName = "test.ods";
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.5");
        propertiesData.put("document", "@OpenSheetDocument;create," + docName);

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", 2.5);

        new File(docName).delete();

        IOpenSheetDocument openSheetdoc = openSheetManager.createDocument(docName);

        expectedProperties.put("document", openSheetdoc);

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);

        when(openSheetManagerMock.createDocument(docName)).thenReturn(openSheetdoc);

        checkBindingPropertiesFile(openSheetManagerMock, "test.properties", propertiesData, expectedProperties);

        verify(openSheetManagerMock, times(1)).createDocument(docName);

        openSheetdoc.close();
        new File(docName).delete();
    }

    @Test
    public void testBindingOpenSheetDocumentPropertiesFileWithOpenParam() throws Exception
    {
        String docName = "test.ods";
        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.5");
        propertiesData.put("document", "@OpenSheetDocument;open," + docName);

        Map<String, Object> expectedProperties = new HashMap();
        expectedProperties.put("prop1", "test 1 with String");
        expectedProperties.put("prop2", 2.5);

        new File(docName).delete();

        IOpenSheetDocument openSheetdoc = openSheetManager.createDocument(docName);

        expectedProperties.put("document", openSheetdoc);

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);

        when(openSheetManagerMock.openDocument(docName)).thenReturn(openSheetdoc);

        checkBindingPropertiesFile(openSheetManagerMock, "test.properties", propertiesData, expectedProperties);

        verify(openSheetManagerMock, times(1)).openDocument(docName);

        openSheetdoc.close();
        new File(docName).delete();
    }

    @Test
    public void testBindingOpenSheetDocumentPropertiesFileWithUnknowParam() throws Exception
    {
        bindingOpenSheetDocumentPropertiesFileWithError("@OpenSheetDocument;ssaeNew,test.ods");
    }

    @Test
    public void testBindingOpenSheetDocumentPropertiesFileWithNameOnly() throws Exception
    {
        bindingOpenSheetDocumentPropertiesFileWithError("@OpenSheetDocument;test.ods");
    }

    @Test
    public void testBindingOpenSheetDocumentPropertiesFileWithActionParamOnly() throws Exception
    {
        bindingOpenSheetDocumentPropertiesFileWithError("@OpenSheetDocument;create");
    }

    @Test
    public void testBindingOpenSheetDocumentPropertiesFileWithoutParams() throws Exception
    {
        bindingOpenSheetDocumentPropertiesFileWithError("@OpenSheetDocument;");
    }

    @Test
    public void testBindingOpenSheetDocumentPropertiesFileWithMore2Params() throws Exception
    {
        bindingOpenSheetDocumentPropertiesFileWithError("@OpenSheetDocument;create,test.ods,new");
    }

    public void bindingOpenSheetDocumentPropertiesFileWithError(String propertyValue) throws Exception
    {
        String dataFile = "testNoFound.properties";
        new File(dataFile).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.5");
        propertiesData.put("document", propertyValue);

        TestUtils.writePropertiesFile(dataFile, propertiesData);

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);

        try
        {
            ScriptExecuter scriptExecuter = createScriptExecuter(openSheetManagerMock, dataFile);
            fail("ScritExecuter must throw OpenSheetPropertyFormatException when the param is invalid");
        }
        catch (OpenSheetPropertyFormatException ex)
        {
        }

        new File(dataFile).delete();

        verify(openSheetManagerMock, times(0)).openDocument(anyString());
        verify(openSheetManagerMock, times(0)).createDocument(anyString());
    }

    @Test
    public void testBindingWithNoFoundPropertiesFile() throws Exception
    {
        String dataFile = "testNoFound.properties";
        new File(dataFile).delete();

        try
        {
            ScriptExecuter scriptExecuter = createScriptExecuter(openSheetManager, dataFile);
            fail("'ScriptExecuter' method must launch LoadOpenSheetDataFileException");
        }
        catch (LoadOpenSheetDataFileException ex)
        {
        }
    }

    private void checkBindingPropertiesFile(OpenSheetManager manager, String dataFile, Map<String, Object> propertiesData, Map<String, Object> expectedProperties) throws Exception
    {
        new File(dataFile).delete();

        TestUtils.writePropertiesFile(dataFile, propertiesData);

        ScriptExecuter scriptExecuter = createScriptExecuter(manager, dataFile);

        Binding binding = scriptExecuter.getBinding();

        assertTrue(binding.getVariables().size() == expectedProperties.size());

        for (String key : expectedProperties.keySet())
        {
            assertTrue(binding.getVariables().containsKey(key));

            if (propertiesData.get(key).equals(ScriptExecuter.OPENSHEET_MANAGER_PROPERTY_NAME))
            {
                assertTrue(binding.getVariables().get(key) instanceof OpenSheetManager);
            }
            else
            {
                assertEquals(expectedProperties.get(key), binding.getVariables().get(key));
            }
        }

        new File(dataFile).delete();
    }

    @Test
    public void testExecuteScriptFile() throws Exception
    {
        String dataFile = "test.properties";

        new File(dataFile).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("name", "World");

        TestUtils.writePropertiesFile(dataFile, propertiesData);

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);

        ScriptExecuter scriptExecuter = createScriptExecuter(openSheetManagerMock, dataFile);

        GroovyShell groovyShellMock = mock(GroovyShell.class);
        Script groovyScriptMock = mock(Script.class);

        when(groovyShellMock.parse((File) notNull())).thenReturn(groovyScriptMock);

        scriptExecuter.setGroovyShell(groovyShellMock);
        File scriptFile = new File("");
        scriptExecuter.run(scriptFile);

        verify(groovyShellMock, times(1)).parse(scriptFile);
        verify(groovyScriptMock, times(1)).setBinding(scriptExecuter.getBinding());
        verify(groovyScriptMock, times(1)).run();

        new File(dataFile).delete();
    }

    @Test
    public void testExecuteScriptFileWithCompilationFailedException() throws Exception
    {
        executeScriptFileWithParseException(new CompilationFailedException(0, null));
    }

    @Test
    public void testExecuteScriptFileWithIOException() throws Exception
    {
        executeScriptFileWithParseException(new IOException());
    }

    private void executeScriptFileWithParseException(Exception exception2Throw) throws Exception
    {
        String dataFile = "test.properties";

        new File(dataFile).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("name", "World");

        TestUtils.writePropertiesFile(dataFile, propertiesData);

        OpenSheetManager openSheetManagerMock = mock(OpenSheetManager.class);
        ScriptExecuter scriptExecuter = createScriptExecuter(openSheetManagerMock, dataFile);

        GroovyShell groovyShellMock = mock(GroovyShell.class);

        when(groovyShellMock.parse((File) notNull())).thenThrow(exception2Throw);

        scriptExecuter.setGroovyShell(groovyShellMock);
        File scriptFile = new File("");

        try
        {
            scriptExecuter.run(scriptFile);
            fail("'run' method must throw a OpenSheetRunScriptException");
        }
        catch (OpenSheetRunScriptException ex)
        {
        }

        verify(groovyShellMock, times(1)).parse(scriptFile);

        new File(dataFile).delete();
    }

    @Test
    public void testExecuteScriptFileAndGetOutputValue() throws Exception
    {
        String dataFile = "test.properties";
        String script = "script.groovy";

        new File(dataFile).delete();
        new File(script).delete();

        Map<String, Object> propertiesData = new HashMap();
        propertiesData.put("name", "World");
        propertiesData.put("myFirstScript", " This is my first script.");

        TestUtils.writeScriptToFile(script, "package groovyapp\n\noutput = \"Hello $name!\" + myFirstScript");
        TestUtils.writePropertiesFile(dataFile, propertiesData);

        ScriptExecuter scriptExecuter = createScriptExecuter(openSheetManager, dataFile);

        File scriptFile = new File(script);
        scriptExecuter.run(scriptFile);

        assertEquals(scriptExecuter.getBinding().getVariable("output").toString(), "Hello World! This is my first script.");

        new File(dataFile).delete();
        scriptFile.delete();
    }
}
