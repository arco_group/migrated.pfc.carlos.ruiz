/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.api;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.opensheet.api.exceptions.CellNameException;

/**
 *
 * @author cruiz
 */
public class CellPositionTest {

    public CellPositionTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void equalsSameObjectTest()
    {
        CellPosition a1 = new CellPosition(0,0);

        assertEquals(a1, a1);
    }

    @Test
    public void equalsObjectsWithSameValueTest()
    {
        CellPosition a1 = new CellPosition(3,4);
        CellPosition b1 = new CellPosition(3,4);
        assertEquals(a1, b1);
    }

    @Test
    public void equalsObjectsWithDiffXValueTest()
    {
        CellPosition a1 = new CellPosition(3,4);
        CellPosition b1 = new CellPosition(2,4);
        assertFalse(a1.equals(b1));
    }

    @Test
    public void equalsObjectsWithDiffYValueTest()
    {
        CellPosition a1 = new CellPosition(3,0);
        CellPosition b1 = new CellPosition(3,3);
        assertFalse(a1.equals(b1));
    }

    @Test
    public void equalsNullObjectValueTest()
    {
        CellPosition a1 = new CellPosition(3,0);
        CellPosition b1 = null;
        try
        {
            assertFalse(a1.equals(b1));
            fail("Method must throw NullPointerException");
        }
        catch (NullPointerException ex)
        {            
        }
    }

    @Test
    public void equalsDiffObjectClassTest()
    {
        CellPosition a1 = new CellPosition(3,0);
        Boolean b1 = true;
        try
        {
            a1.equals(b1);
            fail("Method must throw ClassCastException");
        }
        catch (ClassCastException ex)
        {
        }
    }

    @Test
    public void createCellWithCellName1Test() throws Exception
    {
        CellPosition cell_0_0 = new CellPosition(0,0);
        CellPosition cell_a1 = new CellPosition("a1");
        assertEquals(cell_0_0,cell_a1);
    }

    @Test
    public void createCellWithCellName1WithCapitalLetterTest() throws Exception
    {
        CellPosition cell_0_0 = new CellPosition(0,0);
        CellPosition cell_a1 = new CellPosition("A1");
        assertEquals(cell_0_0,cell_a1);
    }

    @Test
    public void createCellWithCellName2Test() throws Exception
    {
        CellPosition cell_0_9 = new CellPosition(0,9);
        CellPosition cell_a10 = new CellPosition("a10");
        assertEquals(cell_0_9,cell_a10);
    }

    @Test
    public void createCellWithCellName3Test() throws Exception
    {
        CellPosition cell_31_248 = new CellPosition(31,248);
        CellPosition cell_af249 = new CellPosition("af249");
        assertEquals(cell_31_248,cell_af249);
    }

    @Test
    public void createCellWithCellName4Test() throws Exception
    {
        CellPosition cell_54_2049 = new CellPosition(54,2049);
        CellPosition cell_bc2050 = new CellPosition("bc2050");
        assertEquals(cell_54_2049,cell_bc2050);
    }

    @Test
    public void createCellWithCellName5Test() throws Exception
    {
        CellPosition cell_54_2049 = new CellPosition(54,2049);
        CellPosition cell_bc2050 = new CellPosition("bc2050");
        assertEquals(cell_54_2049,cell_bc2050);
    }

    @Test
    public void createCellWithCellName6Test() throws Exception
    {
        CellPosition cell_1023_1048575 = new CellPosition(1023,1048575);
        CellPosition cell_amj1048576 = new CellPosition("amj1048576");
        assertEquals(cell_1023_1048575,cell_amj1048576);
    }

    @Test
    public void createCellWithCellName6WithCapitalLetterTest() throws Exception
    {
        CellPosition cell_1023_1048575 = new CellPosition(1023,1048575);
        CellPosition cell_amj1048576 = new CellPosition("AMJ1048576");
        assertEquals(cell_1023_1048575,cell_amj1048576);
    }

    @Test
    public void createCellWithVoidCellNameTest()
    {
        createCellWithCellNameException("");
    }

    @Test
    public void createCellWithNullCellNameTest()
    {
        createCellWithCellNameException(null);
    }

    @Test
    public void createCellWithOnlyLetterInCellNameTest()
    {
        createCellWithCellNameException("a");
    }

    @Test
    public void createCellWithOnlyLettersInCellNameTest()
    {
        createCellWithCellNameException("aaa");
    }

    @Test
    public void createCellWithOnlyNumberInCellNameTest()
    {
        createCellWithCellNameException("7");
    }

    @Test
    public void createCellWithOnlyNumbersInCellNameTest()
    {
        createCellWithCellNameException("56799");
    }

    @Test
    public void createCellWithNumbersAndLettersInCellNameTest()
    {
        createCellWithCellNameException("56799aa");
    }

    @Test
    public void createCellWithInvalidCellNameTest()
    {
        createCellWithCellNameException("a1bc2");
    }

    private void createCellWithCellNameException(String name)
    {
        try
        {
            CellPosition cell = new CellPosition(name);
            fail("Method must throw CellNameException");
        }
        catch (CellNameException ex)
        {
        }
    }
}