/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.api;

import com.sun.star.comp.helper.BootstrapException;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.opensheet.api.exceptions.LoadOpenSheetDocumentException;
import org.opensheet.api.exceptions.OverwriteOpenSheetDocumentException;
import org.opensheet.api.exceptions.OverwriteSpreadSheetException;
import org.opensheet.api.exceptions.SpreadSheetNoFoundException;
import org.opensheet.api.exceptions.StoreOpenSheetDocumentException;
import org.opensheet.api.exceptions.UnsupportedOpenSheetDocumentException;

/**
 *
 * @author cruiz
 */
public class OpenSheetDocumentTest
{

    private static OpenSheetManager oSheetManager;

    public OpenSheetDocumentTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        oSheetManager = new OpenSheetManager();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        oSheetManager.terminate();
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void createNewODSDocumentTest() throws Exception
    {        
        createNewDocument("docNuevo.ods", true, true);
    }

    @Test
    public void createNewXLSDocumentTest() throws Exception
    {        
        createNewDocument("docNuevo.xls", true, true);
    }

    @Test
    public void createNewXLTDocumentTest() throws Exception
    {        
        createNewDocument("docNuevo.xlt", true, true);
    }

    @Test
    public void createNewOTSDocumentTest() throws Exception
    {        
        createNewDocument("docNuevo.ots", true, true);
    }

    @Test
    public void createNewXLSXDocumentTest() throws Exception
    {
        createNewDocument("docNuevo.xlsx", true, true);
    }

    @Test
    public void createDocumentWithInvalidExtensionTest() throws Exception
    {        
        createDocumentWithUnsupportedOpenSheetDocumentException("docNuevo.doc");
    }

    @Test
    public void createDocumentWithInvalidPDFExtensionTest() throws Exception
    {        
        createDocumentWithUnsupportedOpenSheetDocumentException("docNuevo.pdf");
    }

    @Test
    public void createDocumentWithoutExtensionTest() throws Exception
    {        
        createDocumentWithUnsupportedOpenSheetDocumentException("docNuevo");
    }

    private void createDocumentWithUnsupportedOpenSheetDocumentException(String docName) throws BootstrapException, OverwriteOpenSheetDocumentException
    {
        try
        {
            createNewDocument(docName, true, true);
            fail("A exception must be thrown because the document extension is not supported");
        }
        catch (UnsupportedOpenSheetDocumentException ex)
        {
        }
    }

    @Test
    public void createExistingDocumentTest() throws Exception
    {
        String docName = "docNuevo.ods";

        createNewDocument(docName, true, false);
        try
        {
            createNewDocument(docName, false, false);
            fail("Create document method must be fail because the document already exists");
        }
        catch (OverwriteOpenSheetDocumentException ex)
        {
        }
    }

    @Test
    public void saveAsODS2XLSTest() throws Exception
    {        
        saveAsDocument("docNuevo.ods", "otroNuevo.xls");
    }

    @Test
    public void saveAsODS2XLTTest() throws Exception
    {       
        saveAsDocument("docNuevo.ods", "otroNuevo.xlt");
    }

    @Test
    public void saveAsODS2OTSTest() throws Exception
    {        
        saveAsDocument("docNuevo.ods", "otroNuevo.ots");
    }

    @Test
    public void saveAsODS2ODSTest() throws Exception
    {        
        saveAsDocument("docNuevo.ods", "otroNuevo.ods");
    }

    @Test
    public void saveAsODS2XLSXTest() throws Exception
    {
        saveAsDocument("docNuevo.ods", "otroNuevo.xlsx");
    }

    @Test
    public void saveAsXLS2ODSTest() throws Exception
    {        
        saveAsDocument("docNuevo.xls", "otroNuevo.ods");
    }

    @Test
    public void saveAsXLS2OTSTest() throws Exception
    {       
        saveAsDocument("docNuevo.xls", "otroNuevo.ots");
    }

    @Test
    public void saveAsXLS2XLTTest() throws Exception
    {        
        saveAsDocument("docNuevo.xls", "otroNuevo.xlt");
    }

    @Test
    public void saveAsXLS2XLSTest() throws Exception
    {       
        saveAsDocument("docNuevo.xls", "otroNuevo.xls");
    }

    @Test
    public void saveAsXLS2XLSXTest() throws Exception
    {
        saveAsDocument("docNuevo.xls", "otroNuevo.xlsx");
    }

    @Test
    public void saveAsXLT2XLSTest() throws Exception
    {        
        saveAsDocument("docNuevo.xlt", "otroNuevo.xls");
    }

    @Test
    public void saveAsXLT2ODSTest() throws Exception
    {       
        saveAsDocument("docNuevo.xlt", "otroNuevo.ods");
    }

    @Test
    public void saveAsXLT2OTSTest() throws Exception
    {       
        saveAsDocument("docNuevo.xlt", "otroNuevo.ots");
    }

    @Test
    public void saveAsXLT2XLTTest() throws Exception
    {
       saveAsDocument("docNuevo.xlt", "otroNuevo.xlt");
    }

    public void saveAsXLT2XLSXTest() throws Exception
    {
       saveAsDocument("docNuevo.xlt", "otroNuevo.xlsx");
    }

    @Test
    public void saveAsOTS2ODSTest() throws Exception
    {
      saveAsDocument("docNuevo.ots", "otroNuevo.ods");
    }

    @Test
    public void saveAsOTS2XLSTest() throws Exception
    {        
        saveAsDocument("docNuevo.ots", "otroNuevo.xls");
    }

    @Test
    public void saveAsOTS2XLTTest() throws Exception
    {
       saveAsDocument("docNuevo.ots", "otroNuevo.xlt");
    }

    @Test
    public void saveAsOTS2OTSTest() throws Exception
    {
       saveAsDocument("docNuevo.ots", "otroNuevo.ots");
    }

    @Test
    public void saveAsOTS2XLSXTest() throws Exception
    {
       saveAsDocument("docNuevo.ots", "otroNuevo.xlsx");
    }

    @Test
    public void saveAsXLSX2ODSTest() throws Exception
    {
      saveAsDocument("docNuevo.xlsx", "otroNuevo.ods");
    }

    @Test
    public void saveAsXLSX2XLSTest() throws Exception
    {
        saveAsDocument("docNuevo.xlsx", "otroNuevo.xls");
    }

    @Test
    public void saveAsXLSX2XLTTest() throws Exception
    {
       saveAsDocument("docNuevo.xlsx", "otroNuevo.xlt");
    }

    @Test
    public void saveAsXLSX2OTSTest() throws Exception
    {
       saveAsDocument("docNuevo.xlsx", "otroNuevo.ots");
    }

    @Test
    public void saveAsXLSX2XLSXTest() throws Exception
    {
       saveAsDocument("docNuevo.xlsx", "otroNuevo.xlsx");
    }

    private void saveAsDocument(String sourceDoc, String destinyDoc) throws UnsupportedOpenSheetDocumentException, Exception
    {
        File document = new File("." + File.separatorChar + sourceDoc);
        File newDocument = new File("." + File.separatorChar + destinyDoc);

        deleteDocument(document);
        deleteDocument(newDocument);

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + sourceDoc);

        try
        {
            oSheetDoc.saveAs(destinyDoc);
            oSheetDoc.close();
        }
        catch (Exception ex)
        {
            if (oSheetDoc != null)
            {
                oSheetDoc.close();
            }
            deleteDocument(document);

            throw ex;
        }

        assertTrue(document.exists());
        assertTrue(newDocument.exists());
        deleteDocument(document);
        deleteDocument(newDocument);
    }

    @Test
    public void saveAsWithInvalidExtensionTest() throws Exception
    {
        try
        {
            saveAsDocument("docNuevo.ods", "otroNuevo.pdf");
            fail("A exception must be thrown because the document extension is not supported");
        }
        catch (UnsupportedOpenSheetDocumentException ex)
        {
        }
    }

    @Test
    public void exportToPDFTest() throws Exception
    {
        createAndExportToPDF("docNuevo.ods", "docNuevo.pdf", true);
    }

    private void createAndExportToPDF(String docName, String docPDFName, boolean deleteCreatedDocuments) throws OverwriteOpenSheetDocumentException, StoreOpenSheetDocumentException, UnsupportedOpenSheetDocumentException, Exception
    {
        File document = new File("." + File.separatorChar + docName);
        File newDocument = new File("." + File.separatorChar + docPDFName);

        deleteDocument(document);
        deleteDocument(newDocument);

        IOpenSheetDocument oSheetDoc = null;
        try
        {

            oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

            oSheetDoc.exportToPDF(docPDFName);
            oSheetDoc.close();
        }
        catch (Exception ex)
        {
            if (oSheetDoc != null)
            {
                oSheetDoc.close();
                deleteDocument(document);
            }
            throw ex;
        }

        assertTrue(document.exists());
        assertTrue(newDocument.exists());

        if (deleteCreatedDocuments)
        {
            deleteDocument(document);
            deleteDocument(newDocument);
        }
    }

    @Test
    public void exportToPDFWithInvalidExtensionTest() throws Exception
    {
        String docName = "docNuevo.ods";
       
        try
        {
            createAndExportToPDF(docName, "otroNuevo.xls", true);
            fail("A exception must be thrown because the document extension is not supported");
        }
        catch (UnsupportedOpenSheetDocumentException ex)
        {
            new File(docName).delete();
        }
    }

    private void createNewDocument(String docName, boolean deleteAtBegin, boolean deleteAtEnd) throws BootstrapException, OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException
    {
        File document = new File("." + File.separatorChar + docName);
        if (deleteAtBegin)
        {
            assertTrue(deleteDocument(document));
        }
        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);
        oSheetDoc.close();

        document = new File("." + File.separatorChar + docName);
        assertTrue(document.exists());
        if (deleteAtEnd)
        {
            assertTrue(deleteDocument(document));
        }
    }

    private boolean deleteDocument(File document)
    {
        boolean result = true;
        if (document.exists())
        {
            document.delete();

            if (document.exists())
            {
                result = false;
            }
        }

        return result;
    }

    @Test
    public void openODSDocumentTest() throws Exception
    {        
        openNewDocument("testDocument.ods");
    }

    @Test
    public void openXLSDocumentTest() throws Exception
    {        
        openNewDocument("testDocument.xls");
    }

    @Test
    public void openOTSDocumentTest() throws Exception
    {        
        openNewDocument("testDocument.ots");
    }

    @Test
    public void openXLTDocumentTest() throws Exception
    {        
        openNewDocument("testDocument.xlt");
    }

    @Test
    public void openXLSXDocumentTest() throws Exception
    {
        openNewDocument("testDocument.xlsx");
    }

    @Test
    public void openNoExistDocumentTest() throws Exception
    {
        String docName = "noExistUnknow.ods";
        try
        {
            String sheetNames[] =
            {
            };
            String subStringNames[] =
            {
            };

            deleteDocument(new File(docName));
            openDocument(docName, sheetNames, subStringNames);
            fail("'" + docName + "' document doesn't exist therefore openDocument method must throw a LoadOpenSheetDocumentException");
        }
        catch (LoadOpenSheetDocumentException ex)
        {
        }
    }

    @Test
    public void openInvalidDocumentExtensionTest() throws Exception
    {
        String docName = "testDocument.ods";
        String docPDFName = "testDocument.pdf";

        createAndExportToPDF(docName, docPDFName, false);
        try
        {
            String sheetNames[] =
            {
            };
            String subStringNames[] =
            {
            };

            deleteDocument(new File(docName));
            openDocument(docPDFName, sheetNames, subStringNames);
            fail("'" + docPDFName + "' document hasn't a supported file extension therefore open method must throw a UnsupportedOpenSheetDocumentException");
        }
        catch (UnsupportedOpenSheetDocumentException ex)
        {
            deleteDocument(new File(docPDFName));
        }
    }

    private void openNewDocument(String docName) throws BootstrapException, OverwriteOpenSheetDocumentException, UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {
        String sheetNames[] =
        {
            "Sheet1", "Sheet2", "Sheet3"
        };
        String subStringNames[] =
        {
            "1", "2", "3"
        }; /* to support no English language */

        createNewDocument(docName, true, false);
        openDocument(docName, sheetNames, subStringNames);
    }

    private void openDocument(String docName, String sheetNames[], String subStringNames[]) throws UnsupportedOpenSheetDocumentException, LoadOpenSheetDocumentException
    {

        IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);

        assertNotNull(oSheetDoc);

        String[] names = oSheetDoc.getSpreadSheetsNames();

        assertTrue(names.length == sheetNames.length);

        for (int i = 0; i < names.length; i++)
        {
            assertTrue(names[i].equalsIgnoreCase(sheetNames[i]) || names[i].indexOf(subStringNames[i]) != -1);
        }

        oSheetDoc.close();
        deleteDocument(new File(docName));
    }

    @Test
    public void getSheetWithFistPosTest() throws Exception
    {
        String docName = "testDocument.ods";

        createNewDocument(docName, true, false);
        IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(0);
        assertTrue(spreadSheet.getName().equals(oSheetDoc.getSpreadSheetsNames()[0]));
        deleteDocument(new File(docName));
    }

    @Test
    public void getSheetWithLastPosTest() throws Exception
    {
        String docName = "testDocument.xls";

        createNewDocument(docName, true, false);
        IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(2);
        assertTrue(spreadSheet.getName().equals(oSheetDoc.getSpreadSheetsNames()[2]));
        deleteDocument(new File(docName));
    }

    @Test
    public void getSheetWithValidPosTest() throws Exception
    {
        String docName = "testDocument.xls";

        createNewDocument(docName, true, false);
        IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(1);
        assertTrue(spreadSheet.getName().equals(oSheetDoc.getSpreadSheetsNames()[1]));
        deleteDocument(new File(docName));
    }

    @Test
    public void getSheetWithNegativePosTest() throws Exception
    {
        String docName = "testDocument.ods";

        try
        {
            createNewDocument(docName, true, false);
            IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
            ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(-1);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
        deleteDocument(new File(docName));
    }

    @Test
    public void getSheetWithInvalidPosTest() throws Exception
    {
        String docName = "testDocument.ods";

        try
        {
            createNewDocument(docName, true, false);
            IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
            ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(3);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
        deleteDocument(new File(docName));
    }

    @Test
    public void getSheetWithValidNameTest() throws Exception
    {
        String docName = "testDocument.ods";

        createNewDocument(docName, true, false);
        IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(oSheetDoc.getSpreadSheetsNames()[0]);
        assertTrue(spreadSheet.getName().equals(oSheetDoc.getSpreadSheetsNames()[0]));
        deleteDocument(new File(docName));
    }

    @Test
    public void getSheetWithNullNameTest() throws Exception
    {
        String docName = "testDocument.ods";

        createNewDocument(docName, true, false);
        IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
        try
        {
            ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(null);
            fail("Null name in getSpreadSheet method must throw SpreadSheetNotFoundException.");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }

        deleteDocument(new File(docName));
    }

    @Test
    public void getSheetWithUnknowNameTest() throws Exception
    {
        String docName = "testDocument.xls";

        createNewDocument(docName, true, false);
        IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
        try
        {
            ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet("UnknowSheet14");
            fail("This name in getSpreadSheet method must throw SpreadSheetNotFoundException.");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }

        deleteDocument(new File(docName));
    }

    @Test
    public void addSheetWithoutNameToNewODSDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.ods", null, true);
    }

    @Test
    public void addSheetWithNameToNewODSDocumentTest() throws Exception
    {      
        addSheetToNewDocument("testDocument.ods", "sheetPrueba1", false);
    }

    @Test
    public void addSheetWithVoidNameToNewODSDocumentTest() throws Exception
    {
       addSheetToNewDocumentMustFail("testDocument.ods", "");
    }

    @Test
    public void addSheetWithNullNameToNewODSDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.ods", null);
    }

    @Test
    public void addSheetWithBlankNameToNewODSDocumentTest() throws Exception
    {
      addSheetToNewDocument("testDocument.ods", " ", false);
    }

    @Test
    public void addSheetWithoutNameToNewXLSDocumentTest() throws Exception
    {
       addSheetToNewDocument("testDocument.xls", null, true);
    }

    @Test
    public void addSheetWithNameToNewXLSDocumentTest() throws Exception
    {
       addSheetToNewDocument("testDocument.xls", "sheetPrueba1", false);
    }

    @Test
    public void addSheetWithVoidNameToNewXLSDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.xls", "");
    }

    @Test
    public void addSheetWithNullNameToNewXLSDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.xls", null);
    }

    @Test
    public void addSheetWithBlankNameToNewXLSDocumentTest() throws Exception
    {
       addSheetToNewDocument("testDocument.xls", " ", false);
    }

    @Test
    public void addSheetWithoutNameToNewOTSDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.ots", null, true);
    }

    @Test
    public void addSheetWithNameToNewOTSDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.ots", "sheetPrueba1", false);
    }

    @Test
    public void addSheetWithVoidNameToNewOTSDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.ots", "");
    }

    @Test
    public void addSheetWithNullNameToNewOTSDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.ots", null);
    }

    @Test
    public void addSheetWithBlankNameToNewOTSDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.ots", " ", false);
    }

    @Test
    public void addSheetWithoutNameToNewXLTDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.xlt", null, true);
    }

    @Test
    public void addSheetWithNameToNewXLTDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.xlt", "sheetPrueba1", false);
    }

    @Test
    public void addSheetWithVoidNameToNewXLTDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.xlt", "");
    }

    @Test
    public void addSheetWithNullNameToNewXLTDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.xlt", null);
    }

    @Test
    public void addSheetWithBlankNameToNewXLTDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.xlt", " ", false);
    }

    @Test
    public void addSheetWithoutNameToNewXLSXDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.xlsx", null, true);
    }

    @Test
    public void addSheetWithNameToNewXLSXDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.xlsx", "sheetPrueba1", false);
    }

    @Test
    public void addSheetWithVoidNameToNewXLSXDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.xlsx", "");
    }

    @Test
    public void addSheetWithNullNameToNewXLSXDocumentTest() throws Exception
    {
        addSheetToNewDocumentMustFail("testDocument.xlsx", null);
    }

    @Test
    public void addSheetWithBlankNameToNewXLSXDocumentTest() throws Exception
    {
        addSheetToNewDocument("testDocument.xlsx", " ", false);
    }

    @Test
    public void addSheetWithoutNameToODSDocumentTest() throws Exception
    {
        String docName = "testDocument.ods";

        createNewDocument(docName, true, false);
        addSheetToDocument(docName, null, true);

        deleteDocument(new File(docName));
    }

    @Test
    public void addSheetWithNameToODSDocumentTest() throws Exception
    {
        String docName = "testDocument.ods";
        String sheetName = "sheetPrueba1";

        createNewDocument(docName, true, false);
        addSheetToDocument(docName, sheetName, false);

        deleteDocument(new File(docName));
    }

    @Test
    public void containsSpreadSheetTest() throws Exception
    {
        String docName = "testDocument.ods";
        String sheetName = "Sheet12";

        deleteDocument(new File(docName));

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);
        assertFalse(oSheetDoc.containsSpreadSheet(sheetName));
        oSheetDoc.addSpreadSheet(sheetName);
        assertTrue(oSheetDoc.containsSpreadSheet(sheetName));

        deleteDocument(new File(docName));
    }

    @Test
    public void addSheetsWithSameNameTest() throws Exception
    {
        String docName = "testDocument.ods";
        String sheetName = "Sheet12";

        deleteDocument(new File(docName));
        
        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);
        oSheetDoc.addSpreadSheet(sheetName);
        try 
        {
            oSheetDoc.addSpreadSheet(sheetName);
            fail("Method must throw OverwriteSpreadSheetException");
        }
        catch (OverwriteSpreadSheetException ex)
        {
        }
        oSheetDoc.close();

        deleteDocument(new File(docName));
    }

    private void addSheetToNewDocumentMustFail(String docName, String sheetName) throws Exception
    {
        deleteDocument(new File(docName));

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        assertNull(oSheetDoc.addSpreadSheet(sheetName));

        oSheetDoc.close();

        deleteDocument(new File(docName));
    }

    private void addSheetToNewDocument(String docName, String sheetName, boolean withoutName) throws Exception
    {
        deleteDocument(new File(docName));
        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        addSheetToOpenSheetDocument(oSheetDoc, sheetName, withoutName);

        oSheetDoc.close();
        deleteDocument(new File(docName));
    }

    private void addSheetToDocument(String docName, String sheetName, boolean withoutName) throws Exception
    {
        IOpenSheetDocument oSheetDoc = oSheetManager.openDocument("." + File.separatorChar + docName);
        addSheetToOpenSheetDocument(oSheetDoc, sheetName, withoutName);
        oSheetDoc.close();
    }

    private void addSheetToOpenSheetDocument(IOpenSheetDocument oSheetDoc, String sheetName, boolean withoutName) throws Exception
    {

        ISpreadSheet spreadSheet;
        if (withoutName)
        {
            spreadSheet = oSheetDoc.addSpreadSheet();
        }
        else
        {
            spreadSheet = oSheetDoc.addSpreadSheet(sheetName);
        }

        assertNotNull(spreadSheet);

        String[] names = oSheetDoc.getSpreadSheetsNames();

        assertEquals(spreadSheet.getName(), oSheetDoc.getSpreadSheet(names.length - 1).getName());
    }

    @Test
    public void deleteSheetFromODSWithFirstPositionTest() throws Exception
    {        
        deleteSheet("testDocument.ods", 0, false);
    }

    @Test
    public void deleteSheetFromXLSWithFirstPositionTest() throws Exception
    {       
        deleteSheet("testDocument.xls", 0, false);
    }

    @Test
    public void deleteSheetFromXLTWithFirstPositionTest() throws Exception
    {        
        deleteSheet("testDocument.xlt", 0, false);
    }

    @Test
    public void deleteSheetFromOTSWithFirstPositionTest() throws Exception
    {        
        deleteSheet("testDocument.ots", 0, false);
    }

    @Test
    public void deleteSheetFromXLSXWithFirstPositionTest() throws Exception
    {
        deleteSheet("testDocument.xlsx", 0, false);
    }

    @Test
    public void deleteSheetFromODSWithLastPositionTest() throws Exception
    {        
        deleteSheet("testDocument.ods", 2, false);
    }

    @Test
    public void deleteSheetFromXLSWithLastPositionTest() throws Exception
    {        
        deleteSheet("testDocument.xls", 2, false);
    }

    @Test
    public void deleteSheetFromXLTWithLastPositionTest() throws Exception
    {        
        deleteSheet("testDocument.xlt", 2, false);
    }

    @Test
    public void deleteSheetFromOTSWithLastPositionTest() throws Exception
    {        
        deleteSheet("testDocument.ots", 2, false);
    }

    @Test
    public void deleteSheetFromXLSXWithLastPositionTest() throws Exception
    {
        deleteSheet("testDocument.xlsx", 2, false);
    }

    @Test
    public void deleteSheetFromODSWithValidPositionTest() throws Exception
    {        
        deleteSheet("testDocument.ods", 1, false);
    }

    @Test
    public void deleteSheetFromXLSWithValidPositionTest() throws Exception
    {       
        deleteSheet("testDocument.xls", 1, false);
    }

    @Test
    public void deleteSheetFromXLTWithValidPositionTest() throws Exception
    {        
        deleteSheet("testDocument.xlt", 1, false);
    }

    @Test
    public void deleteSheetFromOTSWithValidPositionTest() throws Exception
    {        
        deleteSheet("testDocument.ots", 1, false);
    }

    @Test
    public void deleteSheetFromXLSXWithValidPositionTest() throws Exception
    {
        deleteSheet("testDocument.xlsx", 1, false);
    }
    
    @Test
    public void deleteSheetFromODSWithInvalidPositionTest() throws Exception
    {        
        try
        {
            deleteSheet("testDocument.ods", 100, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromXLSWithInvalidPositionTest() throws Exception
    {        
        try
        {
            deleteSheet("testDocument.xls", 100, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromXLTWithInvalidPositionTest() throws Exception
    {        
        try
        {
            deleteSheet("testDocument.xlt", 100, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromOTSWithInvalidPositionTest() throws Exception
    {        
        try
        {
            deleteSheet("testDocument.ots", 100, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromXLSXWithInvalidPositionTest() throws Exception
    {
        try
        {
            deleteSheet("testDocument.xlsx", 100, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromODSWithNegativePositionTest() throws Exception
    {        
        try
        {
            deleteSheet("testDocument.ods", -1, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromXLSWithNegativePositionTest() throws Exception
    {        
        try
        {
            deleteSheet("testDocument.xls", -1, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromXLTWithNegativePositionTest() throws Exception
    {        
        try
        {
            deleteSheet("testDocument.xlt", -1, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromOTSWithNegativePositionTest() throws Exception
    {        
        try
        {
            deleteSheet("testDocument.ots", -1, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    @Test
    public void deleteSheetFromXLSXWithNegativePositionTest() throws Exception
    {
        try
        {
            deleteSheet("testDocument.xlsx", -1, false);
            fail("SpreaSheet doesn't exist at this position therefore get method must throw a SpreadSheetNoFoundException");
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }
    }

    private void deleteSheet(String docName, int pos, boolean deleteByName) throws Exception
    {
        deleteDocument(new File(docName));

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        String[] originalNames = oSheetDoc.getSpreadSheetsNames();

        if (deleteByName)
        {
            oSheetDoc.deleteSpreadSheet(originalNames[pos]);
        }
        else
        {
            oSheetDoc.deleteSpreadSheet(pos);
        }

        String[] newNames = oSheetDoc.getSpreadSheetsNames();

        assertTrue(newNames.length == originalNames.length - 1);

        int i = 0;
        for (String name : newNames)
        {
            if (pos == i)
            {
                i++;
            }

            name.equals(originalNames[i]);

            i++;
        }

        oSheetDoc.close();
        deleteDocument(new File(docName));
    }

    @Test
    public void deleteSheetFromODSWithValidNameTest() throws Exception
    {        
        deleteSheet("testDocument.ods", 0, true);
    }

    @Test
    public void deleteSheetFromXLSWithValidNameTest() throws Exception
    {        
        deleteSheet("testDocument.xls", 0, true);
    }

    @Test
    public void deleteSheetFromXLTWithValidNameTest() throws Exception
    {        
        deleteSheet("testDocument.xlt", 0, true);
    }

    @Test
    public void deleteSheetFromOTSWithValidNameTest() throws Exception
    {
        deleteSheet("testDocument.ots", 0, true);
    }

    @Test
    public void deleteSheetFromXLSXWithValidNameTest() throws Exception
    {
        deleteSheet("testDocument.xlsx", 0, true);
    }

    @Test
    public void deleteSheetFromODSWithNullNameTest() throws Exception
    {        
        deleteSheetByNameFail("testDocument.ods", null, 
                "Null name in deleteSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromXLSWithNullNameTest() throws Exception
    {                
        deleteSheetByNameFail("testDocument.xls", null, 
                "Null name in deleteSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromXLTWithNullNameTest() throws Exception
    {
        
        deleteSheetByNameFail("testDocument.xlt", null, 
                "Null name in deleteSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromOTSWithNullNameTest() throws Exception
    {                
        deleteSheetByNameFail("testDocument.ots", null,
                "Null name in deleteSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromXLSXWithNullNameTest() throws Exception
    {
        deleteSheetByNameFail("testDocument.xlsx", null,
                "Null name in deleteSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromODSWithUnknowNameTest() throws Exception
    {        
        deleteSheetByNameFail("testDocument.ods", "UnknowSheet14",
                "This name in getSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromXLSWithUnknowNameTest() throws Exception
    {
        deleteSheetByNameFail("testDocument.xls", "UnknowSheet14",
                "This name in getSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromXLTWithUnknowNameTest() throws Exception
    {
        deleteSheetByNameFail("testDocument.xlt", "UnknowSheet14",
                "This name in getSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromOTSWithUnknowNameTest() throws Exception
    {        
        deleteSheetByNameFail("testDocument.ots", "UnknowSheet14",
                "This name in getSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    @Test
    public void deleteSheetFromXLSXWithUnknowNameTest() throws Exception
    {
        deleteSheetByNameFail("testDocument.xlsx", "UnknowSheet14",
                "This name in getSpreadSheet method must throw SpreadSheetNotFoundException.");
    }

    private void deleteSheetByNameFail(String docName, String sheetName, String failMsg) throws Exception
    {

        deleteDocument(new File(docName));

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        try
        {
            ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(sheetName);
            fail(failMsg);
        }
        catch (SpreadSheetNoFoundException ex)
        {
        }

        oSheetDoc.close();

        deleteDocument(new File(docName));
    }
    
}
