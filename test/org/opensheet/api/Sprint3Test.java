/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.api;

import java.util.List;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cruiz
 */
public class Sprint3Test {
    private static OpenSheetManager oSheetManager;

    public Sprint3Test() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        oSheetManager = new OpenSheetManager();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        oSheetManager.terminate();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void insert20valuesInNewDocumentCellsODSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest01.ods";
        
        insert20ValuesInDocumentCells(docName, true);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInNewDocumentCellsXLSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest01.xls";

        insert20ValuesInDocumentCells(docName, true);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInNewDocumentCellsXLTTest() throws Exception
    {
        String docName = "Sprint3DocumentTest01.xlt";

        insert20ValuesInDocumentCells(docName, true);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInNewDocumentCellsOTSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest01.ots";

        insert20ValuesInDocumentCells(docName, true);
        check20ValuesInDocumentCells(docName, true);
    }

    private void insert20ValuesInDocumentCells(String docName, boolean createDocument) throws Exception
    {        
        IOpenSheetDocument oSheetDocument;
        
        if (createDocument)
        {
            new File(docName).delete();
            oSheetDocument = oSheetManager.createDocument(docName);
        }
        else
        {
            oSheetDocument = oSheetManager.openDocument(docName);
        }

        ISpreadSheet spreadSheet0 = oSheetDocument.getSpreadSheet(0);

        ISpreadSheet spreadSheet1 = oSheetDocument.getSpreadSheet(1);

        ISpreadSheet spreadSheet2 = oSheetDocument.getSpreadSheet(2);

        spreadSheet0.setCellValue(new CellPosition(0, 0), "Sprint 3 - Test 01: 20 values in diferent cells");
        spreadSheet1.setCellValue(new CellPosition(0, 0), "Sprint 3 - Test 01: SpreadSheet pos = 1");
        spreadSheet2.setCellValue(new CellPosition(0, 0), "Sprint 3 - Test 01: SpreadSheet pos = 2");

        spreadSheet0.setCellValue(new CellPosition(0, 5), 19.9);
        spreadSheet0.setCellValue(new CellPosition(2, 5), 22.9);
        spreadSheet0.setCellValue(new CellPosition(5, 15), 3.9);

        spreadSheet1.setCellValue(new CellPosition(1, 5), 19.9);
        spreadSheet1.setCellValue(new CellPosition(3, 5), 22.9);
        spreadSheet1.setCellValue(new CellPosition(6, 15), 3.9);

        spreadSheet2.setCellValue(new CellPosition(1, 6), 19.9);
        spreadSheet2.setCellValue(new CellPosition(3, 6), 22.9);
        spreadSheet2.setCellValue(new CellPosition(6, 16), 3.9);

        spreadSheet0.setCellValue(new CellPosition(0, 18), "Total:");
        spreadSheet0.setCellValue(new CellPosition(0, 19), 141003);

        spreadSheet1.setCellValue(new CellPosition(1, 18), "Total:");
        spreadSheet1.setCellValue(new CellPosition(1, 19), 141003);

        spreadSheet2.setCellValue(new CellPosition(1, 19), "Total:");
        spreadSheet2.setCellValue(new CellPosition(1, 20), 141003);

        spreadSheet0.setCellValue(new CellPosition(0, 22), "19 data tests");
        spreadSheet0.setCellValue(new CellPosition(0, 23), "20 data tests");

        oSheetDocument.save();
        oSheetDocument.close();
    }

    @Test
    public void insert20valuesInNewDocumentRangesODSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest02.ods";

        insert20ValuesInDocumentRanges(docName, true);
        check20ValuesInDocumentRanges(docName, true);
    }

    @Test
    public void insert20valuesInNewDocumentRangesXLSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest02.xls";

        insert20ValuesInDocumentRanges(docName, true);
        check20ValuesInDocumentRanges(docName, true);
    }

    @Test
    public void insert20valuesInNewDocumentRangesXLTTest() throws Exception
    {
        String docName = "Sprint3DocumentTest02.xlt";

        insert20ValuesInDocumentRanges(docName, true);
        check20ValuesInDocumentRanges(docName, true);
    }

    @Test
    public void insert20valuesInNewDocumentRangesOTSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest02.ots";

        insert20ValuesInDocumentRanges(docName, true);
        check20ValuesInDocumentRanges(docName, true);
    }

    private void insert20ValuesInDocumentRanges(String docName, boolean createDocument) throws Exception
    {        
        IOpenSheetDocument oSheetDocument;

        if (createDocument)
        {
            new File(docName).delete();
            oSheetDocument = oSheetManager.createDocument(docName);
        }
        else
        {
            oSheetDocument = oSheetManager.openDocument(docName);
        }

        ISpreadSheet spreadSheet0 = oSheetDocument.getSpreadSheet(0);

        ISpreadSheet spreadSheet1 = oSheetDocument.getSpreadSheet(1);

        ISpreadSheet spreadSheet2 = oSheetDocument.getSpreadSheet(2);

        spreadSheet0.setRangeValue(new CellPosition(0, 2), new CellPosition(0, 4), "Sprint 3 - Test 02: 20 values in diferent ranges");
        spreadSheet1.setRangeValue(new CellPosition(0, 2), new CellPosition(0, 4), "Sprint 3 - Test 02: SpreadSheet pos = 1");
        spreadSheet2.setRangeValue(new CellPosition(0, 2), new CellPosition(0, 4), "Sprint 3 - Test 02: SpreadSheet pos = 2");

        spreadSheet0.setRangeValue(new CellPosition(0, 7), new CellPosition(1, 7), 199.23);
        spreadSheet0.setRangeValue(new CellPosition(9, 4), new CellPosition(11, 6), 999);
        spreadSheet0.setRangeValue(new CellPosition(6, 15), new CellPosition(7, 17), 4256777.12);

        spreadSheet1.setRangeValue(new CellPosition(1, 7), new CellPosition(2, 7), 199.23);
        spreadSheet1.setRangeValue(new CellPosition(10, 4), new CellPosition(12, 6), 999);
        spreadSheet1.setRangeValue(new CellPosition(7, 15), new CellPosition(8, 17), 4256777.12);

        spreadSheet2.setRangeValue(new CellPosition(1, 8), new CellPosition(2, 8), 199.23);
        spreadSheet2.setRangeValue(new CellPosition(10, 5), new CellPosition(12, 7), 999);
        spreadSheet2.setRangeValue(new CellPosition(7, 16), new CellPosition(8, 18), 4256777.12);
        
        spreadSheet0.setRangeValue(new CellPosition(0, 28), new CellPosition(0, 28), "Total Range:");
        spreadSheet0.setRangeValue(new CellPosition(0, 29), new CellPosition(0, 29), 22141003);

        spreadSheet1.setRangeValue(new CellPosition(0, 28), new CellPosition(0, 28), "Total Range:");
        spreadSheet1.setRangeValue(new CellPosition(0, 29), new CellPosition(0, 29), 22141003);

        spreadSheet2.setRangeValue(new CellPosition(0, 28), new CellPosition(0, 28), "Total Range:");
        spreadSheet2.setRangeValue(new CellPosition(0, 29), new CellPosition(0, 29), 22141003);

        spreadSheet0.setRangeValue(new CellPosition(8, 22), new CellPosition(12, 25), "19 data in range test");
        spreadSheet0.setRangeValue(new CellPosition(8, 26), new CellPosition(12, 29), "20 data in range test");
        
        oSheetDocument.save();
        oSheetDocument.close();
    }

    @Test
    public void insert20valuesInDocumentCellsODSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest03_01.ods";

        insert20ValuesInDocumentRanges(docName, true);        
        insert20ValuesInDocumentCells(docName, false); /*Will use generated documents from range cells test*/

        check20ValuesInDocumentRanges(docName, false);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInDocumentCellsXLSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest03_01.xls";

        insert20ValuesInDocumentRanges(docName, true);
        insert20ValuesInDocumentCells(docName, false); /*Will use generated documents from range cells test*/

        check20ValuesInDocumentRanges(docName, false);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInDocumentCellsXLTTest() throws Exception
    {
        String docName = "Sprint3DocumentTest03_01.xls";

        insert20ValuesInDocumentRanges(docName, true);
        insert20ValuesInDocumentCells(docName, false); /*Will use generated documents from range cells test*/

        check20ValuesInDocumentRanges(docName, false);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInDocumentCellsOTSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest03_01.ots";

        insert20ValuesInDocumentRanges(docName, true);
        insert20ValuesInDocumentCells(docName, false); /*Will use generated documents from range cells test*/

        check20ValuesInDocumentRanges(docName, false);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInDocumentRangesODSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest03_02.ods";

        insert20ValuesInDocumentCells(docName, true);
        insert20ValuesInDocumentRanges(docName, false); /*Will use generated documents from cells test*/

        check20ValuesInDocumentRanges(docName, false);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInDocumentRangesXLSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest03_02.xls";

        insert20ValuesInDocumentCells(docName, true);
        insert20ValuesInDocumentRanges(docName, false); /*Will use generated documents from cells test*/

        check20ValuesInDocumentRanges(docName, false);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInDocumentRangesXLTTest() throws Exception
    {
        String docName = "Sprint3DocumentTest03_02.xlt";

        insert20ValuesInDocumentCells(docName, true);
        insert20ValuesInDocumentRanges(docName, false); /*Will use generated documents from cells test*/

        check20ValuesInDocumentRanges(docName, false);
        check20ValuesInDocumentCells(docName, true);
    }

    @Test
    public void insert20valuesInDocumentRangesOTSTest() throws Exception
    {
        String docName = "Sprint3DocumentTest03_02.ots";

        insert20ValuesInDocumentCells(docName, true);
        insert20ValuesInDocumentRanges(docName, false); /*Will use generated documents from cells test*/

        check20ValuesInDocumentRanges(docName, false);
        check20ValuesInDocumentCells(docName, true);
    }

    private void check20ValuesInDocumentCells(String docName, boolean deleteDocument) throws Exception
    {
        IOpenSheetDocument oSheetDocument;

        oSheetDocument = oSheetManager.openDocument(docName);        

        ISpreadSheet spreadSheet0 = oSheetDocument.getSpreadSheet(0);

        ISpreadSheet spreadSheet1 = oSheetDocument.getSpreadSheet(1);

        ISpreadSheet spreadSheet2 = oSheetDocument.getSpreadSheet(2);

        assertEquals(spreadSheet0.getCellText(new CellPosition(0, 0)), "Sprint 3 - Test 01: 20 values in diferent cells");
        assertEquals(spreadSheet0.getCellContent(new CellPosition(0, 0)), "Sprint 3 - Test 01: 20 values in diferent cells");

        assertEquals(spreadSheet1.getCellText(new CellPosition(0, 0)), "Sprint 3 - Test 01: SpreadSheet pos = 1");
        assertEquals(spreadSheet1.getCellContent(new CellPosition(0, 0)), "Sprint 3 - Test 01: SpreadSheet pos = 1");

        assertEquals(spreadSheet2.getCellText(new CellPosition(0, 0)), "Sprint 3 - Test 01: SpreadSheet pos = 2");
        assertEquals(spreadSheet2.getCellContent(new CellPosition(0, 0)), "Sprint 3 - Test 01: SpreadSheet pos = 2");

        assertTrue(spreadSheet0.getCellValue(new CellPosition(0, 5)) == 19.9);
        assertTrue( (Double) spreadSheet0.getCellContent(new CellPosition(0, 5)) == 19.9);
        assertTrue(spreadSheet0.getCellValue(new CellPosition(2, 5)) == 22.9);
        assertTrue( (Double) spreadSheet0.getCellContent(new CellPosition(2, 5)) == 22.9);
        assertTrue(spreadSheet0.getCellValue(new CellPosition(5, 15)) == 3.9);
        assertTrue( (Double) spreadSheet0.getCellContent(new CellPosition(5, 15)) == 3.9);

        assertTrue(spreadSheet1.getCellValue(new CellPosition(1, 5)) == 19.9);
        assertTrue( (Double) spreadSheet1.getCellContent(new CellPosition(1, 5)) == 19.9);
        assertTrue(spreadSheet1.getCellValue(new CellPosition(3, 5)) == 22.9);
        assertTrue( (Double) spreadSheet1.getCellContent(new CellPosition(3, 5)) == 22.9);
        assertTrue(spreadSheet1.getCellValue(new CellPosition(6, 15)) == 3.9);
        assertTrue( (Double) spreadSheet1.getCellContent(new CellPosition(6, 15)) == 3.9);

        assertTrue(spreadSheet2.getCellValue(new CellPosition(1, 6)) == 19.9);
        assertTrue( (Double) spreadSheet2.getCellContent(new CellPosition(1, 6)) == 19.9);
        assertTrue(spreadSheet2.getCellValue(new CellPosition(3, 6)) == 22.9);
        assertTrue( (Double) spreadSheet2.getCellContent(new CellPosition(3, 6)) == 22.9);
        assertTrue(spreadSheet2.getCellValue(new CellPosition(6, 16)) == 3.9);
        assertTrue( (Double) spreadSheet2.getCellContent(new CellPosition(6, 16)) == 3.9);
        
        assertEquals(spreadSheet0.getCellText(new CellPosition(0, 18)) , "Total:");
        assertEquals(spreadSheet0.getCellContent(new CellPosition(0, 18)) , "Total:");
        assertTrue(spreadSheet0.getCellValue(new CellPosition(0, 19)) == 141003);
        assertTrue( (Double) spreadSheet0.getCellContent(new CellPosition(0, 19)) == 141003);

        assertEquals(spreadSheet1.getCellText(new CellPosition(1, 18)) , "Total:");
        assertEquals(spreadSheet1.getCellContent(new CellPosition(1, 18)) , "Total:");
        assertTrue(spreadSheet1.getCellValue(new CellPosition(1, 19)) == 141003);
        assertTrue( (Double) spreadSheet1.getCellContent(new CellPosition(1, 19)) == 141003);
        
        assertEquals(spreadSheet2.getCellText(new CellPosition(1, 19)) , "Total:");
        assertEquals(spreadSheet2.getCellContent(new CellPosition(1, 19)) , "Total:");
        assertTrue(spreadSheet2.getCellValue(new CellPosition(1, 20)) == 141003);
        assertTrue( (Double) spreadSheet2.getCellContent(new CellPosition(1, 20)) == 141003);

        assertEquals(spreadSheet0.getCellText(new CellPosition(0, 22)) , "19 data tests");
        assertEquals(spreadSheet0.getCellContent(new CellPosition(0, 22)) , "19 data tests");
        assertEquals(spreadSheet0.getCellText(new CellPosition(0, 23)) , "20 data tests");
        assertEquals(spreadSheet0.getCellContent(new CellPosition(0, 23)) , "20 data tests");
        
        oSheetDocument.close();

        if (deleteDocument)
            new File(docName).delete();
    }

    
    private void check20ValuesInDocumentRanges(String docName, boolean deleteDocument) throws Exception
    {
        IOpenSheetDocument oSheetDocument;

        oSheetDocument = oSheetManager.openDocument(docName);        

        ISpreadSheet spreadSheet0 = oSheetDocument.getSpreadSheet(0);

        ISpreadSheet spreadSheet1 = oSheetDocument.getSpreadSheet(1);

        ISpreadSheet spreadSheet2 = oSheetDocument.getSpreadSheet(2);

        assertAllListElementsEqualsValue(spreadSheet0.getRangeTexts(new CellPosition(0, 2), new CellPosition(0, 4)), "Sprint 3 - Test 02: 20 values in diferent ranges");
        assertAllListElementsEqualsValue(spreadSheet0.getRangeContent(new CellPosition(0, 2), new CellPosition(0, 4)), "Sprint 3 - Test 02: 20 values in diferent ranges");
      
        assertAllListElementsEqualsValue(spreadSheet1.getRangeTexts(new CellPosition(0, 2), new CellPosition(0, 4)), "Sprint 3 - Test 02: SpreadSheet pos = 1");
        assertAllListElementsEqualsValue(spreadSheet1.getRangeContent(new CellPosition(0, 2), new CellPosition(0, 4)), "Sprint 3 - Test 02: SpreadSheet pos = 1");

        assertAllListElementsEqualsValue(spreadSheet2.getRangeTexts(new CellPosition(0, 2), new CellPosition(0, 4)), "Sprint 3 - Test 02: SpreadSheet pos = 2");
        assertAllListElementsEqualsValue(spreadSheet2.getRangeContent(new CellPosition(0, 2), new CellPosition(0, 4)), "Sprint 3 - Test 02: SpreadSheet pos = 2");

        assertAllListElementsEqualsValue(spreadSheet0.getRangeValues(new CellPosition(0, 7), new CellPosition(1, 7)), 199.23);
        assertAllListElementsEqualsValue(spreadSheet0.getRangeContent(new CellPosition(0, 7), new CellPosition(1, 7)), 199.23);

        assertAllListElementsEqualsValue(spreadSheet0.getRangeValues(new CellPosition(9, 4), new CellPosition(11, 6)), new Double(999));
        assertAllListElementsEqualsValue(spreadSheet0.getRangeContent(new CellPosition(9, 4), new CellPosition(11, 6)), new Double(999));

        assertAllListElementsEqualsValue(spreadSheet0.getRangeValues(new CellPosition(6, 15), new CellPosition(7, 17)), 4256777.12);
        assertAllListElementsEqualsValue(spreadSheet0.getRangeContent(new CellPosition(6, 15), new CellPosition(7, 17)), 4256777.12);

        assertAllListElementsEqualsValue(spreadSheet1.getRangeValues(new CellPosition(1, 7), new CellPosition(2, 7)), 199.23);
        assertAllListElementsEqualsValue(spreadSheet1.getRangeContent(new CellPosition(1, 7), new CellPosition(2, 7)), 199.23);

        assertAllListElementsEqualsValue(spreadSheet1.getRangeValues(new CellPosition(10, 4), new CellPosition(12, 6)), new Double(999));
        assertAllListElementsEqualsValue(spreadSheet1.getRangeContent(new CellPosition(10, 4), new CellPosition(12, 6)), new Double(999));

        assertAllListElementsEqualsValue(spreadSheet1.getRangeValues(new CellPosition(7, 15), new CellPosition(8, 17)), 4256777.12);
        assertAllListElementsEqualsValue(spreadSheet1.getRangeContent(new CellPosition(7, 15), new CellPosition(8, 17)), 4256777.12);

        assertAllListElementsEqualsValue(spreadSheet2.getRangeValues(new CellPosition(1, 8), new CellPosition(2, 8)), 199.23);
        assertAllListElementsEqualsValue(spreadSheet2.getRangeContent(new CellPosition(1, 8), new CellPosition(2, 8)), 199.23);

        assertAllListElementsEqualsValue(spreadSheet2.getRangeValues(new CellPosition(10, 5), new CellPosition(12, 7)), new Double(999));
        assertAllListElementsEqualsValue(spreadSheet2.getRangeContent(new CellPosition(10, 5), new CellPosition(12, 7)), new Double(999));

        assertAllListElementsEqualsValue(spreadSheet2.getRangeValues(new CellPosition(7, 16), new CellPosition(8, 18)), 4256777.12);
        assertAllListElementsEqualsValue(spreadSheet2.getRangeContent(new CellPosition(7, 16), new CellPosition(8, 18)), 4256777.12);

        assertAllListElementsEqualsValue(spreadSheet0.getRangeTexts(new CellPosition(0, 28), new CellPosition(0, 28)), "Total Range:");
        assertAllListElementsEqualsValue(spreadSheet0.getRangeContent(new CellPosition(0, 28), new CellPosition(0, 28)), "Total Range:");

        assertAllListElementsEqualsValue(spreadSheet0.getRangeValues(new CellPosition(0, 29), new CellPosition(0, 29)), new Double(22141003));
        assertAllListElementsEqualsValue(spreadSheet0.getRangeContent(new CellPosition(0, 29), new CellPosition(0, 29)), new Double(22141003));

        assertAllListElementsEqualsValue(spreadSheet1.getRangeTexts(new CellPosition(0, 28), new CellPosition(0, 28)), "Total Range:");
        assertAllListElementsEqualsValue(spreadSheet1.getRangeContent(new CellPosition(0, 28), new CellPosition(0, 28)), "Total Range:");

        assertAllListElementsEqualsValue(spreadSheet1.getRangeValues(new CellPosition(0, 29), new CellPosition(0, 29)), new Double(22141003));
        assertAllListElementsEqualsValue(spreadSheet1.getRangeContent(new CellPosition(0, 29), new CellPosition(0, 29)), new Double(22141003));
        
        assertAllListElementsEqualsValue(spreadSheet2.getRangeTexts(new CellPosition(0, 28), new CellPosition(0, 28)), "Total Range:");
        assertAllListElementsEqualsValue(spreadSheet2.getRangeContent(new CellPosition(0, 28), new CellPosition(0, 28)), "Total Range:");

        assertAllListElementsEqualsValue(spreadSheet2.getRangeValues(new CellPosition(0, 29), new CellPosition(0, 29)), new Double(22141003));
        assertAllListElementsEqualsValue(spreadSheet2.getRangeContent(new CellPosition(0, 29), new CellPosition(0, 29)), new Double(22141003));

        assertAllListElementsEqualsValue(spreadSheet0.getRangeTexts(new CellPosition(8, 22), new CellPosition(12, 25)), "19 data in range test");
        assertAllListElementsEqualsValue(spreadSheet0.getRangeContent(new CellPosition(8, 22), new CellPosition(12, 25)), "19 data in range test");

        assertAllListElementsEqualsValue(spreadSheet0.getRangeTexts(new CellPosition(8, 26), new CellPosition(12, 29)), "20 data in range test");
        assertAllListElementsEqualsValue(spreadSheet0.getRangeContent(new CellPosition(8, 26), new CellPosition(12, 29)), "20 data in range test");
        
        oSheetDocument.close();

        if (deleteDocument)
            new File(docName).delete();
    }

    private void assertAllListElementsEqualsValue(List values, Object expectedValue){
        for (Object value : values)
        {            
            assertEquals(value, expectedValue);
        }
    }
}