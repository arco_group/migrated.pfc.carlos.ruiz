/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package org.opensheet.api;

import java.util.List;
import org.opensheet.api.exceptions.CellNoFoundException;
import java.io.File;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cruiz
 */
public class SpreadSheetTest
{

    private static OpenSheetManager oSheetManager;

    public SpreadSheetTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
        oSheetManager = new OpenSheetManager();
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
        oSheetManager.terminate();
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }
    
    @Test
    public void renameSheetFromODSWithValidNameTest() throws Exception
    {        
        renameSheet("testDocument.ods", 0, "newSheet OpenSheet");
    }

    @Test
    public void renameSheetFromXLSWithValidNameTest() throws Exception
    {
        renameSheet("testDocument.xls", 1, "newSheet OpenSheet");
    }

    @Test
    public void renameSheetFromXLTWithValidNameTest() throws Exception
    {
        renameSheet("testDocument.xlt", 2, "newSheet OpenSheet");
    }

    @Test
    public void renameSheetFromOTSWithValidNameTest() throws Exception
    {
        renameSheet("testDocument.ots", 0, "newSheet OpenSheet");
    }

    @Test
    public void renameSheetFromXLSXWithValidNameTest() throws Exception
    {
        renameSheet("testDocument.xlsx", 0, "newSheet OpenSheet");
    }

    @Test
    public void renameSheetFromODSWithNullNameTest() throws Exception
    {
        renameSheetFail("testDocument.ods", 0, null);
    }

    @Test
    public void renameSheetFromXLSWithNullNameTest() throws Exception
    {
        renameSheetFail("testDocument.xls", 1, null);
    }

    @Test
    public void renameSheetFromXLTWithNullNameTest() throws Exception
    {
        renameSheetFail("testDocument.xlt", 2, null);
    }

    @Test
    public void renameSheetFromOTSWithNullNameTest() throws Exception
    {        
        renameSheetFail("testDocument.ots", 0, null);
    }

    @Test
    public void renameSheetFromXLSXWithNullNameTest() throws Exception
    {
        renameSheetFail("testDocument.xlsx", 0, null);
    }

    @Test
    public void renameSheetFromODSWithVoidNameTest() throws Exception
    {        
        renameSheetFail("testDocument.ods", 1, "");
    }

    @Test
    public void renameSheetFromXLSWithVoidNameTest() throws Exception
    {        
        renameSheetFail("testDocument.xls", 0, "");
    }

    @Test
    public void renameSheetFromXLTWithVoidNameTest() throws Exception
    {        
        renameSheetFail("testDocument.xlt", 1, "");
    }

    @Test
    public void renameSheetFromOTSWithVoidNameTest() throws Exception
    {        
        renameSheetFail("testDocument.ots", 2, "");
    }

    @Test
    public void renameSheetFromXLSXWithVoidNameTest() throws Exception
    {
        renameSheetFail("testDocument.xlsx", 2, "");
    }

    @Test
    public void renameSheetFromODSWithBlankNameTest() throws Exception
    {        
        renameSheet("testDocument.ods", 2, " ");
    }

    @Test
    public void renameSheetFromXLSWithBlankNameTest() throws Exception
    {        
        renameSheet("testDocument.xls", 2, " ");
    }

    @Test
    public void renameSheetFromXLTWithBlankNameTest() throws Exception
    {        
        renameSheet("testDocument.xlt", 0, " ");
    }

    @Test
    public void renameSheetFromOTSWithBlankNameTest() throws Exception
    {        
        renameSheet("testDocument.ots", 1, " ");
    }

    @Test
    public void renameSheetFromXLSXWithBlankNameTest() throws Exception
    {
        renameSheet("testDocument.xlsx", 1, " ");
    }

    private void renameSheet(String docName, int pos, String sheetNewName) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);
        String[] originalNames = oSheetDoc.getSpreadSheetsNames();

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(pos);

        assertTrue(spreadSheet.rename(sheetNewName));

        assertFalse(originalNames[pos].equals(sheetNewName));

        assertEquals(oSheetDoc.getSpreadSheetsNames()[pos], sheetNewName);

        oSheetDoc.close();

        new File(docName).delete();
    }

    private void renameSheetFail(String docName, int pos, String sheetNewName) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);
        String[] originalNames = oSheetDoc.getSpreadSheetsNames();

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(pos);

        assertFalse(spreadSheet.rename(sheetNewName));

        assertEquals(oSheetDoc.getSpreadSheetsNames()[pos], originalNames[pos]);

        oSheetDoc.close();

        new File(docName).delete();
    }

    @Test
    public void insertDoubleToCellODSTest() throws Exception
    {        
        insertDoubleToCell("testDocument.ods", 0, new CellPosition(0, 0), 19.982);
    }

    @Test
    public void insertDoubleToCellXLSTest() throws Exception
    {        
        insertDoubleToCell("testDocument.xls", 0, new CellPosition(0, 0), 19.982);
    }

    @Test
    public void insertDoubleToCellWithNegativeXPositionODSTest() throws Exception
    {        
        insertDoubleToCellFail("testDocument.ods", 0, new CellPosition(-1, 0), 10.2);
    }

    @Test
    public void insertDoubleToCellWithNegativeXPositionXLSTest() throws Exception
    {        
        insertDoubleToCellFail("testDocument.xls", 0, new CellPosition(-1, 0), 10.2);
    }

    @Test
    public void insertDoubleToCellWithNegativeYPositionODSTest() throws Exception
    {        
        insertDoubleToCellFail("testDocument.ods", 0, new CellPosition(1, -1), 0.5);
    }

    @Test
    public void insertDoubleToCellWithNegativeYPositionXLSTest() throws Exception
    {        
        insertDoubleToCellFail("testDocument.xls", 0, new CellPosition(1, -1), 0.5);
    }

    @Test
    public void insertDoubleToCellWithNegativePositionODSTest() throws Exception
    {
        insertDoubleToCellFail("testDocument.ods", 0, new CellPosition(-1, -1), 0.5);
    }

    @Test
    public void insertDoubleToCellWithNegativePositionXLSTest() throws Exception
    {       
        insertDoubleToCellFail("testDocument.xls", 0, new CellPosition(-1, -1), 0.5);
    }

    @Test
    public void insertDoubleToCellWithXPositionOutOfBoundsODSTest() throws Exception
    {        
        insertDoubleToCellFail("testDocument.ods", 0, new CellPosition(999999999, 0), 345);
    }

    @Test
    public void insertDoubleToCellWithXPositionOutOfBoundsXLSTest() throws Exception
    {
        insertDoubleToCellFail("testDocument.ods", 0, new CellPosition(999999999, 0), 345);
    }

    @Test
    public void insertDoubleToCellWithYPositionOutOfBoundsODSTest() throws Exception
    {
        insertDoubleToCellFail("testDocument.ods", 0, new CellPosition(0, 999999999), 9.98);
    }

    @Test
    public void insertDoubleToCellWithYPositionOutOfBoundsXLSTest() throws Exception
    {
        insertDoubleToCellFail("testDocument.xls", 0, new CellPosition(0, 999999999), 9.98);
    }

    @Test
    public void insertDoubleToCellWithPositionOutOfBoundsODSTest() throws Exception
    {
        insertDoubleToCellFail("testDocument.ods", 0, new CellPosition(999999999, 999999999), 9.98);
    }

    @Test
    public void insertDoubleToCellWithPositionOutOfBoundsXLSTest() throws Exception
    {
        insertDoubleToCellFail("testDocument.xls", 0, new CellPosition(999999999, 999999999), 9.98);
    }

    private void insertDoubleToCell(String docName, int spreadSheetIndex, CellPosition cellPosition, double value) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        try
        {
            spreadSheet.setCellValue(cellPosition, value);
        }
        catch (Exception ex)
        {
            oSheetDoc.close();
            new File(docName).delete();

            throw ex;
        }

        oSheetDoc.close();

        new File(docName).delete();
    }

    private void insertDoubleToCellFail(String docName, int spreadSheetIndex, CellPosition cellPosition, double value) throws Exception
    {
        try
        {
            insertDoubleToCell(docName, spreadSheetIndex, cellPosition, value);
            fail("setCellValue method must fail");
        }
        catch (CellNoFoundException ex)
        {
        }
    }

    @Test
    public void insertStringToCellODSTest() throws Exception
    {
        insertStringToCell("testDocument.ods", 0, new CellPosition(0, 0), "text string test");
    }

    @Test
    public void insertStringToCellXLSXTest() throws Exception
    {
        insertStringToCell("testDocument.xlsx", 0, new CellPosition(0, 0), "text string test");
    }

    @Test
    public void insertStringToCellXLSTest() throws Exception
    {        
        insertStringToCell("testDocument.xls", 0, new CellPosition(0, 0), "text string test");
    }

    @Test
    public void insertStringToCellWithNegativeXPositionODSTest() throws Exception
    {        
        insertStringToCellFail("testDocument.ods", 0, new CellPosition(-1, 0), null);
    }

    @Test
    public void insertStringToCellWithNegativeXPositionXLSTest() throws Exception
    {        
        insertStringToCellFail("testDocument.xls", 0, new CellPosition(-1, 0), null);
    }

    @Test
    public void insertStringToCellWithNegativeYPositionODSTest() throws Exception
    {        
        insertStringToCellFail("testDocument.ods", 0, new CellPosition(5, -1), null);
    }

    @Test
    public void insertStringToCellWithNegativeYPositionXLSTest() throws Exception
    {
        insertStringToCellFail("testDocument.xls", 0, new CellPosition(5, -1), null);
    }

    @Test
    public void insertStringToCellWithNegativePositionODSTest() throws Exception
    {
        insertStringToCellFail("testDocument.ods", 0, new CellPosition(-2, -1), null);
    }

    @Test
    public void insertStringToCellWithNegativePositionXLSTest() throws Exception
    {
        insertStringToCellFail("testDocument.xls", 0, new CellPosition(-5, -1), null);
    }

    @Test
    public void insertNullStringToCellODSTest() throws Exception
    {        
        insertStringToCell("testDocument.ods", 0, new CellPosition(0, 0), null);
    }

    @Test
    public void insertNullStringToCellXLSTest() throws Exception
    {
        insertStringToCell("testDocument.xls", 0, new CellPosition(0, 0), null);
    }

    @Test
    public void insertNullStringToCellXLTTest() throws Exception
    {        
        insertStringToCell("testDocument.xlt", 0, new CellPosition(0, 0), null);
    }

    @Test
    public void insertNullStringToCellOTSTest() throws Exception
    {
        insertStringToCell("testDocument.ots", 0, new CellPosition(0, 0), null);
    }

    @Test
    public void insertNullStringToCellXLSXTest() throws Exception
    {
        insertStringToCell("testDocument.xlsx", 0, new CellPosition(0, 0), null);
    }

    private void insertStringToCell(String docName, int spreadSheetIndex, CellPosition cellPosition, String value) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        try
        {
            spreadSheet.setCellValue(cellPosition, value);
        }
        catch (Exception ex)
        {
            oSheetDoc.close();
            new File(docName).delete();

            throw ex;
        }
        oSheetDoc.close();

        new File(docName).delete();
    }

    private void insertStringToCellFail(String docName, int spreadSheetIndex, CellPosition cellPosition, String value) throws Exception
    {
        try
        {
            insertStringToCell(docName, spreadSheetIndex, cellPosition, value);
            fail("setCellValue method must fail");
        }
        catch (CellNoFoundException ex)
        {
        }
    }

    @Test
    public void insertDoubleToRangeTest() throws Exception
    {
        insertDoubleToRange("testDocument.ods", 0, new CellPosition(3, 3), new CellPosition(9, 18), 2.0);
    }

    @Test
    public void insertDoubleToRangeWithNegativePositionInFirstCellTest() throws Exception
    {
        try
        {
            insertDoubleToRange("testDocument.ods", 0, new CellPosition(-1, 0), new CellPosition(6, 15), 2.0);
            fail("the insertDoubleToRange method must fail");
        }
        catch (CellNoFoundException ex)
        {
        }
    }

    private void insertDoubleToRange(String docName, int spreadSheetIndex, CellPosition firstCellPosition, CellPosition lastCellPosition, double value) throws Exception
    {

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        try
        {
            spreadSheet.setRangeValue(firstCellPosition, lastCellPosition, value);
        }
        catch (Exception ex)
        {
            oSheetDoc.close();
            new File(docName).delete();

            throw ex;
        }
        oSheetDoc.close();

        new File(docName).delete();
    }

    @Test
    public void insertStringToRangeTest() throws Exception
    {
        insertStringToRange("testDocument.ods", 0, new CellPosition(3, 3), new CellPosition(9, 18), "Text string test");
    }

    @Test
    public void insertStringToRangeWithNegativePositionInFirstCellTest() throws Exception
    {
        try
        {
            insertStringToRange("testDocument.ods", 0, new CellPosition(-1, 0), new CellPosition(6, 15), "Text string test");
            fail("the insertStringToRange method must fail");
        }
        catch (CellNoFoundException ex)
        {
        }
    }

    private void insertStringToRange(String docName, int spreadSheetIndex, CellPosition firstCellPosition, CellPosition lastCellPosition, String value) throws Exception
    {

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        try
        {
            spreadSheet.setRangeValue(firstCellPosition, lastCellPosition, value);
        }
        catch (Exception ex)
        {
            oSheetDoc.close();
            new File(docName).delete();

            throw ex;
        }
        oSheetDoc.close();

        new File(docName).delete();
    }

    @Test
    public void getValueFromNewDocumentCellTest() throws Exception
    {
        getValueFromNewDocumentCell("testDocument.ods", 0, new CellPosition(4, 4), 8452.3445);
    }

    @Test
    public void getNegativeValueFromNewDocumentCellTest() throws Exception
    {
        getValueFromNewDocumentCell("testDocument.ods", 1, new CellPosition(0, 0), -34.2);
    }

    @Test
    public void getValueFromNewDocumentCellWithInvalidPositionTest() throws Exception
    {
        String docName = "testDocument.ods";        

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);
        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(0);

        try
        {
            spreadSheet.getCellValue(new CellPosition(-4, 4));
            fail("'getCellValue' method must throw exception");
        }
        catch (CellNoFoundException ex)
        {
        }

        oSheetDoc.close();
        new File(docName).delete();
    }

    private void getValueFromNewDocumentCell(String docName, int spreadSheetIndex, CellPosition cell, double value) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        spreadSheet.setCellValue(cell, value);

        assertTrue(value == spreadSheet.getCellValue(cell));

        oSheetDoc.close();
        new File(docName).delete();
    }

    @Test
    public void getTextFromNewDocumentCellTest() throws Exception
    {
       getTextFromNewDocumentCell("testDocument.ods", 0, new CellPosition(0, 0), "text insert with SpreadSheet Test");
    }

    @Test
    public void getVoidTextFromNewDocumentCellTest() throws Exception
    {
       getTextFromNewDocumentCell("testDocument.ods", 1, new CellPosition(6, 3), "");
    }

    @Test
    public void getNullTextFromNewDocumentCellTest() throws Exception
    {
       getTextFromNewDocumentCell("testDocument.ods", 1, new CellPosition(2, 5), null);
    }

    @Test
    public void getTextFromNewDocumentCellWithInvalidPositionTest() throws Exception
    {
        String docName = "testDocument.ods";        

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);
        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(0);

        try
        {
            spreadSheet.getCellText(new CellPosition(-4, 4));
            fail("'getCellText' method must throw exception");
        }
        catch (CellNoFoundException ex)
        {
        }

        oSheetDoc.close();
        new File(docName).delete();
    }

    private void getTextFromNewDocumentCell(String docName, int spreadSheetIndex, CellPosition cell, String text) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        spreadSheet.setCellValue(cell, text);

        if (text == null)
        {
            text = ""; //Empty string is the getCellText method return when null value was inserted.
        }
        assertEquals(text, spreadSheet.getCellText(cell));

        oSheetDoc.close();
        new File(docName).delete();
    }

    @Test
    public void getValuesFromNewODSDocumentRangeTest() throws Exception
    {
        ArrayList<Double> values = new ArrayList<Double>();
        values.add(5.4);
        values.add(88812.3493);
        values.add(23.0);
        values.add(4523424.0);
        values.add(78.452);

        getValuesFromNewDocumentRange("testDocument.ods", 0, new CellPosition(0, 0), new CellPosition(0, 4), values);
    }

    @Test
    public void getValuesFromNewXLSDocumentRangeTest() throws Exception
    {
        ArrayList<Double> values = new ArrayList<Double>();
        values.add(5.4);
        values.add(88812.3493);
        values.add(23.0);
        values.add(4523424.0);
        values.add(78.452);

        getValuesFromNewDocumentRange("testDocument.xls", 0, new CellPosition(0, 0), new CellPosition(0, 4), values);
    }

    @Test
    public void getValuesFromNewDocumentRangeWithInvalidPositionTest() throws Exception
    {
        String docName = "testDocument.ods";        

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(0);

        try
        {
            spreadSheet.getRangeValues(new CellPosition(-1, 0), new CellPosition(0, 0));
            fail("'getRangeValues' method must fail with invalid cell position");
        }
        catch (CellNoFoundException ex)
        {
        }

        oSheetDoc.close();
        new File(docName).delete();
    }

    private void getValuesFromNewDocumentRange(String docName, int spreadSheetIndex, CellPosition startCell, CellPosition endCell, List<Double> values) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        int xLen = getAbsoluteValue(startCell.getPosX() - endCell.getPosX());
        int yLen = getAbsoluteValue(startCell.getPosY() - endCell.getPosY());

        int i = 0;
        for (CellPosition cell : getCellsFromRange(startCell, endCell))
        {
            spreadSheet.setCellValue(cell, values.get(i));
            i++;
        }

        assertTrue(listEquals(values, spreadSheet.getRangeValues(startCell, endCell)));

        oSheetDoc.close();
        new File(docName).delete();
    }

    private List<CellPosition> getCellsFromRange(CellPosition startCell, CellPosition endCell)
    {
        ArrayList cells = new ArrayList();
        int xLen = getAbsoluteValue(startCell.getPosX() - endCell.getPosX());
        int yLen = getAbsoluteValue(startCell.getPosY() - endCell.getPosY());

        for (int i = 0; i <= xLen; i++)
        {
            for (int j = 0; j <= yLen; j++)
            {
                cells.add(new CellPosition(i, j));
            }
        }
        return cells;
    }

    private int getAbsoluteValue(int x)
    {
        return x < 0 ? -x : x;
    }

    private boolean listEquals(List<Double> list1, List<Double> list2)
    {
        if (list1.size() != list2.size())
        {
            return false;
        }

        for (int i = 0; i < list1.size(); i++)
        {
            if (!list1.get(i).equals(list2.get(i)))
            {
                return false;
            }
        }

        return true;
    }
    
    private boolean stringListEquals(List<String> list1, List<String> list2)
    {
        if (list1.size() != list2.size())
        {
            return false;
        }

        for (int i = 0; i < list1.size(); i++)
        {
            if (list1.get(i) == null)
            {
                if (!list2.get(i).isEmpty())
                {
                    return false;
                }
            }
            else if (!list1.get(i).equals(list2.get(i)))
            {
                return false;
            }
        }

        return true;
    }

    @Test
    public void getTextsFromNewODSDocumentRangeTest() throws Exception
    {
        ArrayList<String> values = new ArrayList<String>();
        values.add("Test 01/8811/11");
        values.add("OpenSheet Library");
        values.add("");
        values.add(null);
        values.add("OpenSource");

        getTextsFromNewDocumentRange("testDocument.ods", 0, new CellPosition(0, 0), new CellPosition(0, 4), values);
    }

    @Test
    public void getTextsFromNewXLSDocumentRangeTest() throws Exception
    {
        ArrayList<String> values = new ArrayList<String>();
        values.add("Method getTextsFromNewXLSDocumentRangeTest");
        values.add("Junit text");
        values.add("   ");
        values.add(null);
        values.add("OpenSource, LGPL");

        getTextsFromNewDocumentRange("testDocument.ods", 0, new CellPosition(0, 0), new CellPosition(0, 4), values);
    }

    private void getTextsFromNewDocumentRange(String docName, int spreadSheetIndex, CellPosition startCell, CellPosition endCell, List<String> values) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        int xLen = getAbsoluteValue(startCell.getPosX() - endCell.getPosX());
        int yLen = getAbsoluteValue(startCell.getPosY() - endCell.getPosY());

        int i = 0;
        for (CellPosition cell : getCellsFromRange(startCell, endCell))
        {
            spreadSheet.setCellValue(cell, values.get(i));
            i++;
        }

        assertTrue(stringListEquals(values, spreadSheet.getRangeTexts(startCell, endCell)));

        oSheetDoc.close();
        new File(docName).delete();
    }

    @Test
    public void getTextsFromNewDocumentRangeWithInvalidPositionTest() throws Exception
    {
        String docName = "testDocument.ods";        

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(0);

        try
        {
            spreadSheet.getRangeTexts(new CellPosition(-1, 0), new CellPosition(0, 0));
            fail("'getRangeValues' method must fail with invalid cell position");
        }
        catch (CellNoFoundException ex)
        {
        }

        oSheetDoc.close();
        new File(docName).delete();
    }

    @Test
    public void getCellContentTest() throws Exception
    {
        String docName = "testDocument.ods";        

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(1);

        ArrayList<CellPosition> cells = new ArrayList<CellPosition>();

        cells.add(new CellPosition(0, 5));
        cells.add(new CellPosition(34, 2));
        cells.add(new CellPosition(4, 3));
        cells.add(new CellPosition(0, 0));

        ArrayList values = new ArrayList();

        values.add("cell contains this text");
        values.add(45.6);
        values.add(341234.34123);
        values.add("another text");

        for (int i = 0; i < cells.size(); i++)
        {
            if (values.get(i) instanceof String)
            {
                spreadSheet.setCellValue(cells.get(i), (String) values.get(i));
            }
            else
            {
                spreadSheet.setCellValue(cells.get(i), (Double) values.get(i));
            }
        }

        for (int i = 0; i < cells.size(); i++)
        {
            Object value = spreadSheet.getCellContent(cells.get(i));
            assertEquals(value, values.get(i));
        }

        oSheetDoc.close();
        new File(docName).delete();
    }

    @Test
    public void getCellContentWithInvalidPositionTest() throws Exception
    {
        String docName = "testDocument.ods";        

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(0);

        try
        {
            spreadSheet.getCellContent(new CellPosition(-1, 0));
            fail("'getCellContent' method must fail with invalid cell position");
        }
        catch (CellNoFoundException ex)
        {
        }

        oSheetDoc.close();
        new File(docName).delete();
    }

    @Test
    public void getRangeContentFromODSDocumentTest() throws Exception
    {
        String docName = "testDocument.ods";
        int spreadSheetIndex = 1;       

        ArrayList<CellPosition> cells = new ArrayList<CellPosition>();

        cells.add(new CellPosition(1, 5));
        cells.add(new CellPosition(1, 6));
        cells.add(new CellPosition(1, 7));
        cells.add(new CellPosition(1, 8));

        ArrayList values = new ArrayList();

        values.add("cell contains this text");
        values.add(45.6);
        values.add(341234.34123);
        values.add("another text");

        getRangeContent(docName, spreadSheetIndex, cells, values);
    }

     @Test
    public void getRangeContentFromXLSDocumentTest() throws Exception
    {
        String docName = "testDocument.xls";
        int spreadSheetIndex = 0;        

        ArrayList<CellPosition> cells = new ArrayList<CellPosition>();

        cells.add(new CellPosition(2, 2));
        cells.add(new CellPosition(2, 3));
        cells.add(new CellPosition(3, 2));
        cells.add(new CellPosition(3, 3));

        ArrayList values = new ArrayList();
        
        values.add(45.6);
        values.add("  ");
        values.add(769.9897);
        values.add("Text, text, text");

        getRangeContent(docName, spreadSheetIndex, cells, values);
    }

    private void getRangeContent(String docName, int spreadSheetIndex, ArrayList<CellPosition> cells, ArrayList values) throws Exception
    {
        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(spreadSheetIndex);

        for (int i = 0; i < cells.size(); i++)
        {
            if (values.get(i) instanceof String)
            {
                spreadSheet.setCellValue(cells.get(i), (String) values.get(i));
            }
            else
            {
                spreadSheet.setCellValue(cells.get(i), (Double) values.get(i));
            }
        }

        List<Object> contents = spreadSheet.getRangeContent(cells.get(0), cells.get(cells.size() - 1));

        assertTrue(values.size() == contents.size());

        for (int i = 0; i < values.size(); i++)
        {
            assertEquals(values.get(i), contents.get(i));
        }

        oSheetDoc.close();
        new File(docName).delete();
    }

    @Test
    public void getRangeContentWithInvalidPositionTest() throws Exception
    {
        String docName = "testDocument.ods";        

        new File(docName).delete();

        IOpenSheetDocument oSheetDoc = oSheetManager.createDocument("." + File.separatorChar + docName);

        ISpreadSheet spreadSheet = oSheetDoc.getSpreadSheet(0);

        try
        {
            spreadSheet.getRangeContent(new CellPosition(-1, 0), new CellPosition(4,5));
            fail("'getRangeContent' method must fail with invalid cell position");
        }
        catch (CellNoFoundException ex)
        {
        }

        oSheetDoc.close();
        new File(docName).delete();
    }
}
