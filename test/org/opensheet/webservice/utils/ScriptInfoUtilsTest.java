/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice.utils;

import org.opensheet.utils.TestUtils;
import java.util.Map;
import java.io.File;
import java.util.HashMap;
import java.util.Properties;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opensheet.webservice.ScriptInfo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author cruiz
 */
public class ScriptInfoUtilsTest {

    public ScriptInfoUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getScriptAndPropertyFilesTest()
    {
        File dirMock = mock(File.class);        
        when(dirMock.isDirectory()).thenReturn(Boolean.TRUE);

        File script1File = mock(File.class);
        when(script1File.getName()).thenReturn("script1.groovy");
        when(script1File.isDirectory()).thenReturn(Boolean.FALSE);
        File script1PropFile = mock(File.class);
        when(script1PropFile.getName()).thenReturn("script1.properties");
        when(script1PropFile.isDirectory()).thenReturn(Boolean.FALSE);

        File otherFile = mock(File.class);
        when(otherFile.getName()).thenReturn("script.groovy.xml");
        when(otherFile.isDirectory()).thenReturn(Boolean.FALSE);

        File otherDir = mock(File.class);
        when(otherDir.getName()).thenReturn("script2.groovy");
        when(otherDir.isDirectory()).thenReturn(Boolean.TRUE);

        File otherDirProp = mock(File.class);
        when(otherDirProp.getName()).thenReturn("script2.properties");
        when(otherDirProp.isDirectory()).thenReturn(Boolean.FALSE);

        File script3 = mock(File.class);
        when(script3.getName()).thenReturn("script3.groovy");
        when(script3.isDirectory()).thenReturn(Boolean.FALSE);

        File script4Prop = mock(File.class);
        when(script4Prop.getName()).thenReturn("script4.properties");
        when(script4Prop.isDirectory()).thenReturn(Boolean.FALSE);

        File files[] = {script1File, script1PropFile, otherFile, otherDir, otherDirProp, script3, script4Prop};

        when(dirMock.listFiles()).thenReturn(files);
        
        Map<File, File> scripts = ScriptInfoUtils.getScriptsAndPropertiesFiles(dirMock);

        assertEquals(1, scripts.keySet().size());
        assertTrue(scripts.containsKey(script1File));
        assertTrue(scripts.containsValue(script1PropFile));        
    }

    @Test
    public void getScriptAndPropertyFilesWithEmptyDirTest()
    {
        File dirMock = mock(File.class);
        when(dirMock.isDirectory()).thenReturn(Boolean.TRUE);
        when(dirMock.listFiles()).thenReturn(null);

        Map<File, File> scripts = ScriptInfoUtils.getScriptsAndPropertiesFiles(dirMock);
        assertTrue(scripts.isEmpty());
    }

    @Test
    public void getScriptInfoTest()
    {
        Properties properties = new Properties();

        String desc = "script test";
        String tempDir = "tempDirectory";

        properties.setProperty(ScriptInfoUtils.SCRIPT_DESCRIPTION_PROP, desc);
        properties.setProperty(ScriptInfoUtils.SCRIPT_OUTPUT_VARIABLES_PROP, "vars1;other;file_document");
        properties.setProperty(ScriptInfoUtils.SCRIPT_TEMPORAL_DIR_VARIABLE_PROP, tempDir);

        File scriptFile = new File("scriptTest.groovy");


        ScriptInfo scriptInfo = ScriptInfoUtils.getScriptInfo(scriptFile, properties);

        String[] outputVars = {"vars1","other","file_document"};
        checkScriptInfoValue(scriptInfo, desc, scriptFile, outputVars, tempDir);
    }

    @Test
    public void getScriptInfoWithEmptyPropertiesTest()
    {
        Properties properties = new Properties();
        
        File scriptFile = new File("scriptTest.groovy");

        ScriptInfo scriptInfo = ScriptInfoUtils.getScriptInfo(scriptFile, properties);
        checkScriptInfoValue(scriptInfo, null, scriptFile, null, null);
    }

    @Test
    public void getScriptsInfoTest() throws Exception
    {
        File script1PropertiesFile = new File("script1.properties");
        File script1 = new File("script1.groovy");        
        File script2PropertiesFile = new File("script2.properties");
        File script2 = new File("script2.groovy");

        String desc1 = "Script1 allows to convert numeric cells to another type cells";
        String tmpDirVar1 = "temporalDir";
        Map<String, Object> propScript1 = new HashMap<String, Object>();
        propScript1.put(ScriptInfoUtils.SCRIPT_DESCRIPTION_PROP, desc1);
        propScript1.put(ScriptInfoUtils.SCRIPT_OUTPUT_VARIABLES_PROP, "vars1;other;file_document");
        propScript1.put(ScriptInfoUtils.SCRIPT_TEMPORAL_DIR_VARIABLE_PROP, tmpDirVar1);
        String [] vars1 = {"vars1","other","file_document"};

        String desc2 = "Script1 allows to convert numeric cells to another type cells";
        String tmpDirVar2 = "cache_directory";
        Map<String, Object> propScript2 = new HashMap<String, Object>();
        propScript2.put(ScriptInfoUtils.SCRIPT_DESCRIPTION_PROP, desc2);
        propScript2.put(ScriptInfoUtils.SCRIPT_OUTPUT_VARIABLES_PROP, "vars12;other_34;ffile_document");
        propScript2.put(ScriptInfoUtils.SCRIPT_TEMPORAL_DIR_VARIABLE_PROP, tmpDirVar2);
        String [] vars2 = {"vars12","other_34","ffile_document"};

        script1PropertiesFile.delete();
        script2PropertiesFile.delete();

        TestUtils.writePropertiesFile(script1PropertiesFile.getPath(), propScript1);
        TestUtils.writePropertiesFile(script2PropertiesFile.getPath(), propScript2);

        Map<File, File> scriptsMap = new HashMap<File, File>();
        scriptsMap.put(script1, script1PropertiesFile);
        scriptsMap.put(script2, script2PropertiesFile);

        Map<String, ScriptInfo> scriptsInfo = ScriptInfoUtils.getScriptsInfo(scriptsMap);

        assertEquals(2,scriptsInfo.size());
        checkScriptInfoValue(scriptsInfo.get(script1.getName()),desc1, script1, vars1, tmpDirVar1);
        checkScriptInfoValue(scriptsInfo.get(script2.getName()),desc2, script2, vars2, tmpDirVar2);

        script1PropertiesFile.delete();
        script2PropertiesFile.delete();
    }

    private void checkScriptInfoValue(ScriptInfo scriptInfo,String description, File scriptFile, String[] outputVars, String tempDir){
        assertEquals(description, scriptInfo.getDescription());
        assertEquals(scriptFile.getPath(), scriptInfo.getPath());
        assertEquals(scriptFile.getName(), scriptInfo.getName());
        assertEquals(tempDir, scriptInfo.getTemporalDirectoryVariable());

        if (outputVars == null)
        {
            assertNull(scriptInfo.getOutputVariables());
        }
        else
        {
            assertEquals(outputVars.length, scriptInfo.getOutputVariables().length);
            for (int i = 0; i < outputVars.length; i++)
            {
                assertEquals(outputVars[i], scriptInfo.getOutputVariables()[i]);
            }
        }
    }

    
}