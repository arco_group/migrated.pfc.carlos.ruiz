/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice.utils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opensheet.utils.TestUtils;
import org.opensheet.webservice.Document;
import org.opensheet.webservice.ScriptVariable;
import static org.junit.Assert.*;

/**
 *
 * @author cruiz
 */
public class OperationUtilsTest {

    public OperationUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createTemporalDirTest() throws Exception
    {
        File cacheDir = new File ("./cache_opensheetTest");
        cacheDir.delete();

        assertTrue(cacheDir.mkdir());
        assertTrue(cacheDir.list().length == 0);

        File tempDir = OperationUtils.createTemporalDir(cacheDir);
        assertTrue(tempDir.exists());
        assertTrue(tempDir.isDirectory());
        assertTrue(tempDir.getParentFile().equals(cacheDir));

        File tempDir2 = OperationUtils.createTemporalDir(cacheDir);
        assertTrue(tempDir2.exists());
        assertTrue(tempDir2.isDirectory());
        assertTrue(tempDir2.getParentFile().equals(cacheDir));

        assertFalse(tempDir.getName().equals(tempDir2.getName()));

        tempDir.delete();
        tempDir2.delete();
        cacheDir.delete();
    }

    @Test
    public void createTemporalDirWithNoExistParentTest() throws Exception
    {
        File parentDir = new File("__test_no_exists");
        assertTrue(parentDir.exists() == false);

        try
        {
            File tmpDir = OperationUtils.createTemporalDir(parentDir);
            fail("The method must launch IOException");
        }
        catch(IOException ex)
        {            
        }
    }

    @Test
    public void saveDocumentTest() throws Exception
    {
        Document doc = new Document();
        String docName = "doc1.test";
        doc.setName(docName);
        String msg = "texto para fichero de prueba";
        doc.setContent(msg.getBytes());
        File dir = new File(".");
        assertTrue(dir.exists() && dir.isDirectory());

        File docFile = new File(dir, docName);
        docFile.delete();

        OperationUtils.saveDocument(doc, dir);
        assertTrue(docFile.exists());
        assertTrue(docFile.isFile());
        assertTrue(docFile.length() > 0);

        InputStream is = new FileInputStream(docFile);
        byte[] buffer = new byte[(int)docFile.length()];
        int offset = 0;
        int numRead = 0;
        while (offset < buffer.length && (numRead=is.read(buffer, offset, buffer.length-offset)) >= 0) {
            offset += numRead;
        }
        is.close();

        assertEquals(msg, new String(buffer));
        docFile.delete();
    }

    @Test
    public void loadDocumentTest() throws Exception
    {
        File dir = new File(".");
        File doc1File = new File("doc1.test");
        doc1File.delete();
        
        String msg = "\tWellcome to OpenSheet Web Service test\n\nBienvenido al test del Servicio Web de OpenSheet.";
        Document doc1 = new Document(doc1File.getName(), msg.getBytes());
        OperationUtils.saveDocument(doc1, dir);

        Document loadedDoc = OperationUtils.loadDocument(doc1File);
        assertEquals(doc1.getName(), loadedDoc.getName());
        assertEquals(msg, new String(loadedDoc.getContent()));
        doc1File.delete();
    }

    @Test
    public void addTemporalPathToDocumentKeywordWithCreateParamTest()
    {
        String docName1 = "test_117.ods";
        String docName2 = "document.Open.Sheet.xls";
        Map<String, String> propertiesData = new HashMap();
        propertiesData.put("prop1", "test 1 with String");
        propertiesData.put("prop2", "@number;2.5");
        propertiesData.put("manager", "@OpenSheetManager;");
        propertiesData.put("document", "@OpenSheetDocument;create," + docName1);
        propertiesData.put("document2", "@OpenSheetDocument;open," + docName2);
        propertiesData.put("var.ls.key", "@list;,saludos ,,¡hola bienvenido!,OpenSheet, Welcome to ... OpenSheet;");
        propertiesData.put("e2.doc", "@OpenSheetDocument;create," + docName1);

        File tempDir = new File("tempDirectoryTests_1");
        Map<String, String> parsedProperties = OperationUtils.addTemporalPathToDocumentKeyword(tempDir, propertiesData);

        assertEquals(propertiesData.size(), parsedProperties.size());
        assertEquals(propertiesData.keySet(), parsedProperties.keySet());
        assertEquals(propertiesData.get("prop1"), parsedProperties.get("prop1"));
        assertEquals(propertiesData.get("prop2"), parsedProperties.get("prop2"));
        assertEquals(propertiesData.get("manager"), parsedProperties.get("manager"));
        assertEquals(propertiesData.get("var.ls.key"), parsedProperties.get("var.ls.key"));
        assertEquals("@OpenSheetDocument;create,"+tempDir.getAbsolutePath()+File.separator+docName1, parsedProperties.get("document"));
        assertEquals("@OpenSheetDocument;open,"+tempDir.getAbsolutePath()+File.separator+docName2, parsedProperties.get("document2"));
        assertEquals("@OpenSheetDocument;create,"+tempDir.getAbsolutePath()+File.separator+docName1, parsedProperties.get("e2.doc"));
    }

    @Test
    public void deleteDirectoryTest() throws Exception
    {
        File firstDir = new File("test_delete_dir");        
        
        File contentDir = new File(new File(new File(firstDir,"dir2"), "dir3"),"with_files");

        assertTrue(contentDir.mkdirs());
        assertTrue(firstDir.exists());
        assertTrue(contentDir.exists());

        File file1 = new File(contentDir, "file1");
        File file2 = new File(contentDir, "op.file_3.txt");

        TestUtils.writeScriptToFile(file1.getAbsolutePath(), "examples");
        TestUtils.writeScriptToFile(file2.getAbsolutePath(), "texto de prueba para ser borrado");

        assertTrue(file1.exists());
        assertTrue(file2.exists());

        OperationUtils.deleteDirectory(firstDir);
        assertTrue(!firstDir.exists());
        
    }

    @Test
    public void getVariablesMapTest()
    {        
        ScriptVariable var1 = new ScriptVariable("document", "/home/user1/doc34");
        ScriptVariable var2 = new ScriptVariable("document1", "/home/user1/doc34");
        ScriptVariable var3 = new ScriptVariable("element", "xml file for internal use");
        List <ScriptVariable> variables = new ArrayList<ScriptVariable>();
        variables.add(var1);
        variables.add(var2);
        variables.add(var3);

        Map<String, String> variablesMap = OperationUtils.getVariablesMap(variables);

        assertEquals(variables.size(), variablesMap.size());

        for (ScriptVariable var : variables)
        {
            assertEquals(var.getValue(), variablesMap.get(var.getName()));
        }
    }

    @Test
    public void getVariablesMapWithEmptyArrayTest()
    {
        List <ScriptVariable> variables = new ArrayList<ScriptVariable>();
        Map<String, String> variablesMap = OperationUtils.getVariablesMap(variables);
        assertEquals(variables.size(), variablesMap.size());
    }

    @Test
    public void getVariablesMapWithNullArrayTest()
    {
        List <ScriptVariable> variables = null;
        Map<String, String> variablesMap = OperationUtils.getVariablesMap(variables);
        assertNull(variablesMap);        
    }
}