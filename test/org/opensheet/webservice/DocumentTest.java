/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cruiz
 */
public class DocumentTest {

    public DocumentTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void isEmptyDocumentTest()
    {
        Document doc = new Document();
        assertTrue(doc.isEmpty());
    }

    @Test
    public void isEmptyDocumentWithContentValueTest()
    {
        Document doc = new Document(null, "value".getBytes());
        assertFalse(doc.isEmpty());
    }

    @Test
    public void isEmptyDocumentWithContentValue2Test()
    {
        Document doc = new Document();
        doc.setContent("data".getBytes());
        assertFalse(doc.isEmpty());
    }

    @Test
    public void isEmptyDocumentWithNameValueTest()
    {
        Document doc = new Document("test", null);
        assertFalse(doc.isEmpty());
    }

    @Test
    public void isEmptyDocumentWithNameValue2Test()
    {
        Document doc = new Document();
        doc.setName("value");
        assertFalse(doc.isEmpty());
    }



}