/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2011 by Carlos Ruiz Ruiz
 *
 * OpenSheet - A open solution for automatic insertion/extraction data to/from
 * spreadsheets. (OpenSheet uses OpenOffice.org and UNO)
 *
 * This file is part of OpenSheet Solution.
 *
 * OpenSheet Solution is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenSheet Solution is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenSheet Solution.  If not, see
 * <http://www.gnu.org/copyleft/lgpl.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package org.opensheet.webservice;

import java.util.Map;
import java.util.List;
import groovy.lang.Binding;
import org.opensheet.webservice.exceptions.OpenSheetOperationInvalidParamsException;
import java.io.File;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opensheet.api.OpenSheetManager;
import org.opensheet.command.ScriptExecuter;
import org.opensheet.utils.TestUtils;
import org.opensheet.webservice.utils.OperationUtils;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author cruiz
 */
public class ExecuteScriptOperationTest {

    public ExecuteScriptOperationTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    @Test
    public void constructorWithNullManagerTest() throws Exception
    {
        try
        {
            ExecuteScriptOperation op = new ExecuteScriptOperation(null, new File("."), new ScriptInfo(), new Document(), new HashMap<String, String>());
            fail("Method must launch OpenSheetOperationInvalidParamsException");
        }
        catch(OpenSheetOperationInvalidParamsException ex)
        {            
        }        
    }

    @Test
    public void constructorWithNullWorkingDirTest() throws Exception
    {
        try
        {
            ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), null, new ScriptInfo(), new Document(), new HashMap<String, String>());
            fail("Method must launch OpenSheetOperationInvalidParamsException");
        }
        catch(OpenSheetOperationInvalidParamsException ex)
        {
        }
    }

    @Test
    public void constructorWithNoExistsWorkingDirTest() throws Exception
    {
        try
        {
            ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("dontExistTestDir__"), new ScriptInfo(), new Document(), new HashMap<String, String>());
            fail("Method must launch OpenSheetOperationInvalidParamsException");
        }
        catch(OpenSheetOperationInvalidParamsException ex)
        {
        }
    }

    @Test
    public void constructorWithWorkingDirNoDirectoryTest() throws Exception
    {
        File file = new File("prueba_file.test");
        file.delete();
        
        try
        {           
            TestUtils.writeScriptToFile(file.getAbsolutePath(), "hola!");
            ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), file, new ScriptInfo(), new Document(), new HashMap<String, String>());
            fail("Method must launch OpenSheetOperationInvalidParamsException");
        }
        catch(OpenSheetOperationInvalidParamsException ex)
        {
        }
        finally
        {
            file.delete();
        }
    }

    @Test
    public void constructorWithNullScriptInfoTest() throws Exception
    {
        try
        {
            ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."), null, new Document(), new HashMap<String, String>());
            fail("Method must launch OpenSheetOperationInvalidParamsException");
        }
        catch(OpenSheetOperationInvalidParamsException ex)
        {
        }
    }

    @Test
    public void constructorWithNullDocumentTest() throws Exception
    {
        try
        {
            ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."), new ScriptInfo(), null, new HashMap<String, String>());
            fail("Method must launch OpenSheetOperationInvalidParamsException");
        }
        catch(OpenSheetOperationInvalidParamsException ex)
        {
        }
    }

    @Test
    public void constructorWithNullVariablesTest() throws Exception
    {
        try
        {
            ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."), new ScriptInfo(), new Document(), null);
            fail("Method must launch OpenSheetOperationInvalidParamsException");
        }
        catch(OpenSheetOperationInvalidParamsException ex)
        {
        }
    }

    @Test
    public void constructorWithEmptyVariablesTest() throws Exception
    {
        ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."), new ScriptInfo(), new Document(), new HashMap<String, String>());        
    }

    @Test
    public void createTemporalDirectoryTest() throws Exception
    {
        OpenSheetManager managerMock = mock(OpenSheetManager.class);
        File cacheDirectory = new File("temp_forOperationTest");

        if (cacheDirectory.exists())
            OperationUtils.deleteDirectory(cacheDirectory);

        assertTrue(cacheDirectory.mkdir());
        assertEquals(0, cacheDirectory.listFiles().length);
        
        ExecuteScriptOperation op = new ExecuteScriptOperation(managerMock, cacheDirectory,new ScriptInfo(), new Document(), new HashMap<String, String>());
        op.createTemporalDirectory();

        assertEquals(1, cacheDirectory.listFiles().length);
        File tempDir = cacheDirectory.listFiles()[0];
        assertTrue(tempDir.isDirectory());
        assertEquals(0, tempDir.listFiles().length);
        OperationUtils.deleteDirectory(cacheDirectory);
    }

    @Test
    public void saveDocumentInTemporalDirectoryTest() throws Exception
    {
        OpenSheetManager managerMock = mock(OpenSheetManager.class);
        File tempDirectory = new File("temp_forOperationTest");

        if (tempDirectory.exists())
            OperationUtils.deleteDirectory(tempDirectory);

        assertTrue(tempDirectory.mkdir());
        assertEquals(0, tempDirectory.listFiles().length);

        Document doc1 = new Document("prueba1.xls", "new document for test".getBytes());
        ExecuteScriptOperation op = new ExecuteScriptOperation(managerMock, new File("."),new ScriptInfo(), doc1, new HashMap<String, String>());

        op.setTemporalDirectory(tempDirectory);
        op.saveDocumentInTemporalDirectory();        
        
        assertEquals(1, tempDirectory.listFiles().length);
        assertTrue(tempDirectory.listFiles()[0].isFile());
        assertEquals(doc1.getName(), tempDirectory.listFiles()[0].getName());
        OperationUtils.deleteDirectory(tempDirectory);
    }

    @Test
    public void parseVariablesMapTest() throws Exception
    {
        String docName1 = "spreadCont_1.ods";
        String docName2 = "Brian.ods";
        Map<String, String> propertiesData = new HashMap();
        propertiesData.put("var_23", "Number 23");
        propertiesData.put("p2", "@number;1233");
        propertiesData.put("manager", "@OpenSheetManager;");
        propertiesData.put("doc_test1", "@OpenSheetDocument;create," + docName1);
        propertiesData.put("doc_test2", "@OpenSheetDocument;open," + docName2);
        propertiesData.put("list_test", "@list;,saludos ,,¡hola bienvenido!,OpenSheet, Welcome to ... OpenSheet;");
        propertiesData.put("document", "@OpenSheetDocument;create," + docName1);

        ScriptInfo scriptInfo = new ScriptInfo();
        scriptInfo.setTemporalDirectoryVariable("temporal_dir");

        ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."),scriptInfo,
                new Document(), propertiesData);

        File tempDir = new File("test_parseVar");
        op.setTemporalDirectory(tempDir);

        assertEquals(propertiesData, op.getVariablesMap());
        op.parseVariablesMap();
        assertEquals(propertiesData.size() + 1, op.getVariablesMap().size());
        assertEquals(propertiesData.get("var_23"), op.getVariablesMap().get("var_23"));
        assertEquals(propertiesData.get("p2"), op.getVariablesMap().get("p2"));
        assertEquals(propertiesData.get("manager"), op.getVariablesMap().get("manager"));
        assertEquals(propertiesData.get("list_test"), op.getVariablesMap().get("list_test"));
        assertEquals("@OpenSheetDocument;create,"+tempDir.getAbsolutePath()+File.separator+docName1, op.getVariablesMap().get("doc_test1"));
        assertEquals("@OpenSheetDocument;open,"+tempDir.getAbsolutePath()+File.separator+docName2, op.getVariablesMap().get("doc_test2"));
        assertEquals("@OpenSheetDocument;create,"+tempDir.getAbsolutePath()+File.separator+docName1, op.getVariablesMap().get("document"));
        assertEquals(tempDir.getAbsolutePath(), op.getVariablesMap().get(scriptInfo.getTemporalDirectoryVariable()));                
        
    }

    @Test
    public void getDocumentsFromOutputsTest() throws Exception
    {
        ScriptInfo scriptInfo = new ScriptInfo();
        String[] outputVars = new String[]{"output1","logFile","result"};
        scriptInfo.setOutputVariables(outputVars);

        Binding binding = new Binding();
        File output1 = new File("generate_output1.txt");
        File output2 = new File("salida.test_.ods");
        File output3 = new File("values_ouput_.xls");

        output1.delete();
        output2.delete();
        output3.delete();

        binding.setVariable(outputVars[0], output1.getName());
        binding.setVariable(outputVars[1], output2.getName());
        binding.setVariable(outputVars[2], output3.getName());
        TestUtils.writeScriptToFile(output1.getAbsolutePath(), "output1 File");
        TestUtils.writeScriptToFile(output2.getAbsolutePath(), "OUTput2 FILE");
        TestUtils.writeScriptToFile(output3.getAbsolutePath(), "output3 File");

        ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."),scriptInfo,
                new Document(), new HashMap<String, String>());
        List<Document> documentsFromOutputs = op.getDocumentsFromOutputs(binding);

        assertEquals(3, documentsFromOutputs.size());
        for (Document doc : documentsFromOutputs)
        {
            assertTrue( doc.getName().equals(output1.getName()) || doc.getName().equals(output2.getName())
                    || doc.getName().equals(output3.getName()));
            String content = new String(doc.getContent());
            assertTrue(content.equals("output1 File") || content.equals("OUTput2 FILE") || content.equals("output3 File") );
        }

        output1.delete();
        output2.delete();
        output3.delete();
    }

    @Test
    public void getScriptFileTest() throws Exception
    {
        ScriptInfo scriptInfo = new ScriptInfo();
        scriptInfo.setPath("test_dir"+File.separator+"script1.groovy");
        ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."),scriptInfo,
                new Document(), new HashMap<String, String>());
        File scriptFile = op.getScriptFile();
        assertEquals(scriptInfo.getPath(), scriptFile.getPath());
    }

    @Test
    public void getScriptFile2Test() throws Exception
    {
        ScriptInfo scriptInfo = new ScriptInfo();
        scriptInfo.setPath("anotherfile");
        ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."),scriptInfo,
                new Document(), new HashMap<String, String>());
        File scriptFile = op.getScriptFile();
        assertEquals(scriptInfo.getPath(), scriptFile.getPath());
    }

    @Test
    public void deleteTemporalDirectoryTest() throws Exception
    {
        File tempDir = new File("temp_DIR_delete");                
        assertTrue(!tempDir.exists());

        File tempDir2 = new File(tempDir, "prueba");
        assertTrue(tempDir2.mkdirs());
        assertTrue(tempDir.exists());
        
        File data1 = new File(tempDir2, "example1.txt");
        TestUtils.writeScriptToFile(data1.getAbsolutePath(), "examples");

        assertTrue(data1.exists());
        assertTrue(data1.length() > 0);

        ExecuteScriptOperation op = new ExecuteScriptOperation(mock(OpenSheetManager.class), new File("."),new ScriptInfo(),
                new Document(), new HashMap<String, String>());
        op.setTemporalDirectory(tempDir);
        op.deleteTemporalDirectory();
        assertTrue(!tempDir.exists());
    }

    @Test
    public void executeScriptTest() throws Exception
    {
        // Document Object
        Document doc = new Document("prueba1.xls", "new document for test".getBytes());
        checkExecuteScriptWithDocument(doc);
    }

    @Test
    public void executeScriptWithEmptyDocumentTest() throws Exception
    {
        // Document Object
        Document doc = new Document();
        checkExecuteScriptWithDocument(doc);
    }
    
    public void checkExecuteScriptWithDocument(Document doc) throws Exception
    {                
        // Preparing ScriptInfo
        ScriptInfo scriptInfo = new ScriptInfo();
        scriptInfo.setName("scriptTest.groovy");
        scriptInfo.setPath("scripts/scriptTest.groovy");        
        String []outputVars = new String[]{"output"};        
        scriptInfo.setOutputVariables(outputVars);

        // Generating de output file
        File outputFile = new File ("test_executeScript_WS.txt");
        String outputContent = "Write text with TestUtils.writeScriptToFile .";
        outputFile.delete();
        TestUtils.writeScriptToFile(outputFile.getAbsolutePath(), outputContent);

        // Working Directory
        File workingDir = new File(".");                

        //Mocks
        OpenSheetManager managerMock = mock(OpenSheetManager.class);
        ScriptExecuter scriptExecuterMock = mock(ScriptExecuter.class);

        ExecuteScriptOperation operationSpy = spy(new ExecuteScriptOperation(managerMock, workingDir,
                scriptInfo, doc, new HashMap<String, String>())); //Spy ExecuteScriptOperation to check private methods calls        

        //Binding
        Binding binding = new Binding();
        binding.setVariable(outputVars[0], outputFile.getName());        
        when(operationSpy.createScriptExecuter()).thenReturn(scriptExecuterMock);
        when(scriptExecuterMock.getBinding()).thenReturn(binding);
        List <Document> docReturns = operationSpy.execute();
        
        //check calls into 'executeScript' method                
        verify(operationSpy, times(1)).createTemporalDirectory();

        if (doc.isEmpty())
        {
            verify(operationSpy, times(0)).saveDocumentInTemporalDirectory();
        }
        else
        {
            verify(operationSpy, times(1)).saveDocumentInTemporalDirectory();
        }
        verify(operationSpy, times(1)).getScriptFile();
        verify(operationSpy, times(1)).parseVariablesMap();
        verify(operationSpy, times(1)).createScriptExecuter();
        verify(operationSpy, times(1)).getDocumentsFromOutputs(binding);
        verify(operationSpy, times(1)).deleteTemporalDirectory();
        verify(scriptExecuterMock, times(1)).run(operationSpy.getScriptFile());

        assertEquals(1, docReturns.size());
        assertEquals(outputFile.getName(), docReturns.get(0).getName());
        assertEquals(outputContent, new String(docReturns.get(0).getContent()));

        outputFile.delete();
    }

    

}